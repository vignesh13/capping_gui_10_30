; Based on -- ISPPExample1.iss --
;
; This script shows various basic things you can achieve using Inno Setup Preprocessor (ISPP).
; To enable commented #define's, either remove the ';' or use ISCC with the /D switch.


[Setup]
AppName=SMAC Capping Control Center
AppVersion=0.18_TEMP
DefaultDirName={pf}\SMAC\SMAC Capping Control Center
DefaultGroupName=SMAC Capping Control Center
UninstallDisplayIcon={app}\SMAC Capping Control Center.exe
;LicenseFile=Files\License.txt
OutputDir=.\Output

[InstallDelete]
Type: files; Name: "{app}\Actuators\*.*"

[Files]
Source: "Files\SMAC Capping Control Center.exe"; DestDir: "{app}"; Flags: ignoreversion
Source: "..\Documenten\Help document.pdf"; DestDir: "{app}"; Flags: ignoreversion
Source: "Files\Actuators\*.*"; DestDir: "{app}\Actuators"; Flags: ignoreversion

; Source: "Readme.txt"; DestDir: "{app}"; \
;  Flags: isreadme

[Icons]
Name: "{commonprograms}\SMAC Capping Control Center"; Filename: "{app}\SMAC Capping Control Center.exe"
Name: "{commondesktop}\SMAC Capping Control Center"; Filename: "{app}\SMAC Capping Control Center.exe"

[Run]
Filename: "{app}\SMAC Capping Control Center.exe"; Description: "Launch SMAC Capping Control Center"; Flags: postinstall nowait skipifsilent

