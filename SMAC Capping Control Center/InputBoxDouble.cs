﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SMAC_Capping_Control_Center
{
    //
    // The InputBoxDouble allways uses a . as the decimal separator (not depending on regional settings)
    //

    public class InputBoxDouble
    {
        private double Min = 0;
        private double Max = 0;
        private double dValue = 0;
        private TextBox Textbox = null;
        private string separator = ".";       // assume that the . is the separator
        private System.Drawing.Color okForecolor = System.Drawing.Color.Black;
        private System.Drawing.Color okBackcolor = System.Drawing.Color.White;

        private string Name = "";

        public InputBoxDouble(TextBox textbox, double min, double max, double initialvalue, string name, bool set_initial_value = true)
        {
            if (set_initial_value)
            {
                Min = min;
                Max = max;

                Textbox = textbox;
                Textbox.Text = initialvalue.ToString().Replace(",", ".");
                double dummy = 0.5;
                if (dummy.ToString().Contains(','))
                {
                    separator = ",";
                }
                Name = name;
                dValue = initialvalue;
                okForecolor = Textbox.ForeColor;
                okBackcolor = Textbox.BackColor;
            }
            else
            {
                Name = name;
                Textbox = textbox;
                Textbox.Text = "";
                dValue = 0.0;
                okForecolor = Textbox.ForeColor;
                okBackcolor = Textbox.BackColor;
            }
        }

        public void SetLimits(double min, double max)
        {
            Min = min;
            Max = max;
        }

        public string Stringvalue
        {
            get
            {
                ReadValue();
                return Textbox.Text.Trim().Replace(",", ".");
            }
            set
            {
                Textbox.Text = value.Trim();
            }
        }

        public double Doublevalue
        {
            get
            {
                ReadValue();
                return dValue;
            }
            set
            {
                dValue = value;
                Textbox.Text = Utils.neutralDouble(dValue);
            }
        }

        public void ReadValue()
        {
            string message = "";
            if (double.TryParse(Textbox.Text.Replace(".", separator).Replace(",", separator), out dValue))
            {
            }
            else
            {
                ErrorColors();
                message = "Inalid entry " + Textbox.Text + " for " + Name + "\r" +
                          "The value you entered is not a valid number";
                message += "\rValue replaced by " + Utils.neutralDouble(Min);
                MessageBox.Show(message);
                Doublevalue = Min;
            }
            ClipToLimits();
            NormalColors();
        }

        private void ErrorColors()
        {
            Textbox.ForeColor = System.Drawing.Color.Red;
            Textbox.BackColor = System.Drawing.Color.LightYellow;
        }

        private void NormalColors()
        {
            Textbox.ForeColor = okForecolor;
            Textbox.BackColor = okBackcolor;
        }

        

        private void ClipToLimits()
        {
            string message = "";
            double newValue = 0;
            if (dValue < Min)
            {
                message = "Invalid entry " + Textbox.Text + " for " + Name + "\r" +
                          "The minimum value is " + Utils.neutralDouble(Min);
                newValue = Min;
            }
            if (dValue > Max)
            {
                message = "Inalid entry " + Textbox.Text + " for " + Name + "\r" +
                          "The maximum value is " + Utils.neutralDouble(Max);
                newValue = Max;
            }
            if (message != "")
            {
                ErrorColors();
                message += "\rValue will be replaced by " + Utils.neutralDouble(newValue);
                MessageBox.Show(message);
                Doublevalue = newValue;
                NormalColors();
            }
        }
    }
}
