﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SMAC_Capping_Control_Center
{
    //
    // The InputBoxInt allways uses a . as the decimal separator (not depending on regional settings)
    //

    public class InputBoxInt
    {
        private int Min = 0;
        private int  Max = 0;
        private int iValue = 0;
        private TextBox Textbox = null;
        private System.Drawing.Color okForecolor = System.Drawing.Color.Black;
        private System.Drawing.Color okBackcolor = System.Drawing.Color.White;

        private string Name = "";

        public InputBoxInt(TextBox textbox, int min, int max, int initialvalue, string name)
        {
            Min = min;
            Max = max;

            Textbox = textbox;
            Textbox.Text = initialvalue.ToString().Replace(",", ".");
            Name = name;
            iValue = initialvalue;
            okForecolor = Textbox.ForeColor;
            okBackcolor = Textbox.BackColor;
        }

        public void SetLimits(int min, int max)
        {
            Min = min;
            Max = max;
        }

        public string Stringvalue
        {
            get
            {
                ReadValue();
                return iValue.ToString();
            }
            set
            {
                Textbox.Text = value.Trim();
            }
        }

        public int Intvalue
        {
            get
            {
                ReadValue();
                return iValue;
            }
            set
            {
                iValue = value;
                Textbox.Text = value.ToString();
            }
        }

        public void ReadValue()
        {
            string message = "";
            if (int.TryParse(Textbox.Text, out iValue))
            {
            }
            else
            {
                ErrorColors();
                message = "Inalid entry " + Textbox.Text + " for " + Name + "\r" +
                          "The value you entered is not a valid number";
                message += "\rValue replaced by " + Min.ToString();
                MessageBox.Show(message);
                iValue = Min;
                Textbox.Text = Min.ToString();
            }
            ClipToLimits();
            NormalColors();
        }

        private void ErrorColors()
        {
            Textbox.ForeColor = System.Drawing.Color.Red;
            Textbox.BackColor = System.Drawing.Color.LightYellow;
        }

        private void NormalColors()
        {
            Textbox.ForeColor = okForecolor;
            Textbox.BackColor = okBackcolor;
        }

        

        private void ClipToLimits()
        {
            string message = "";
            int newValue = 0;
            if (iValue < Min)
            {
                message = "Invalid entry " + Textbox.Text + " for " + Name + "\r" +
                          "The minimum value is " + Min.ToString();
                newValue = Min;
            }
            if (iValue > Max)
            {
                message = "Inalid entry " + Textbox.Text + " for " + Name + "\r" +
                          "The maximum value is " + Max.ToString();
                newValue = Max;
            }
            if (message != "")
            {
                ErrorColors();
                message += "\rValue will be replaced by " + newValue.ToString();
                MessageBox.Show(message);
                iValue = newValue;
                NormalColors();
            }
        }
    }
}
