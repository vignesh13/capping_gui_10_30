﻿namespace SMAC_Capping_Control_Center
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.txtTerminal = new System.Windows.Forms.TextBox();
            this.txtPositionCloseToPart = new System.Windows.Forms.TextBox();
            this.lblPositionCloseToPart = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.cmbActuator = new System.Windows.Forms.ComboBox();
            this.lblActuator = new System.Windows.Forms.Label();
            this.lblPort = new System.Windows.Forms.Label();
            this.cmbPort = new System.Windows.Forms.ComboBox();
            this.btnLoadSettingsFromFile = new System.Windows.Forms.Button();
            this.chart1 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.label15 = new System.Windows.Forms.Label();
            this.btnActuatorInfo = new System.Windows.Forms.Button();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.Run = new System.Windows.Forms.TabPage();
            this.lblRotaryRevs_Run = new System.Windows.Forms.Label();
            this.lblLinearDistance_Run = new System.Windows.Forms.Label();
            this.lblLinDepthRun = new System.Windows.Forms.Label();
            this.lblRotaryDepthRun = new System.Windows.Forms.Label();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.lblThreadStartRun = new System.Windows.Forms.Label();
            this.lblHomePositionRun = new System.Windows.Forms.Label();
            this.lblPluginfoRun = new System.Windows.Forms.Label();
            this.lblThreadDepthMeasured = new System.Windows.Forms.Label();
            this.lblLandingHeight = new System.Windows.Forms.Label();
            this.pictureBoxRun = new System.Windows.Forms.PictureBox();
            this.Setup = new System.Windows.Forms.TabPage();
            this.lblRotaryRevs_Setup = new System.Windows.Forms.Label();
            this.lblLinearDistance_Setup = new System.Windows.Forms.Label();
            this.txtNumofThreads = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtRetractDistance = new System.Windows.Forms.TextBox();
            this.lblRetractDistance = new System.Windows.Forms.Label();
            this.btnTeachPickCap = new System.Windows.Forms.Button();
            this.txtPositionCloseToPickCap = new System.Windows.Forms.TextBox();
            this.lblPositionCloseToPickCap = new System.Windows.Forms.Label();
            this.cmbCappingType = new System.Windows.Forms.ComboBox();
            this.lblCappingType = new System.Windows.Forms.Label();
            this.lblLinDepthSetup = new System.Windows.Forms.Label();
            this.lblMinRotDistTravel = new System.Windows.Forms.Label();
            this.lblMaxRotDistTravel = new System.Windows.Forms.Label();
            this.txtMinRotDistTravel = new System.Windows.Forms.TextBox();
            this.txtMaxRotDistTravel = new System.Windows.Forms.TextBox();
            this.chkRotDistTravel = new System.Windows.Forms.CheckBox();
            this.lblMinLinTravel = new System.Windows.Forms.Label();
            this.lblMaxLinTravel = new System.Windows.Forms.Label();
            this.txtMinLinTravel = new System.Windows.Forms.TextBox();
            this.txtMaxLinTravel = new System.Windows.Forms.TextBox();
            this.chkLinDistTravel = new System.Windows.Forms.CheckBox();
            this.lblCloseToPart = new System.Windows.Forms.Label();
            this.lblHomePositionSetup = new System.Windows.Forms.Label();
            this.lblThreadStartSetup = new System.Windows.Forms.Label();
            this.lblRotDepthSetup = new System.Windows.Forms.Label();
            this.lblPluginfoSetup = new System.Windows.Forms.Label();
            this.lblThreadDepthMeasured2 = new System.Windows.Forms.Label();
            this.lblLandingHeight2 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.btnTeach = new System.Windows.Forms.Button();
            this.lblLandedHeightMax = new System.Windows.Forms.Label();
            this.lblLandedHeightMin = new System.Windows.Forms.Label();
            this.txtLandedHeightMax = new System.Windows.Forms.TextBox();
            this.txtLandedHeightMin = new System.Windows.Forms.TextBox();
            this.chkCheckLandedHeight = new System.Windows.Forms.CheckBox();
            this.chkRotateReverse = new System.Windows.Forms.CheckBox();
            this.cmbThreadDirection = new System.Windows.Forms.ComboBox();
            this.pictureBoxSetup = new System.Windows.Forms.PictureBox();
            this.lblPitch = new System.Windows.Forms.Label();
            this.lblThreadDirection = new System.Windows.Forms.Label();
            this.Tuning = new System.Windows.Forms.TabPage();
            this.label1 = new System.Windows.Forms.Label();
            this.txtSpeedtoPickCap = new System.Windows.Forms.TextBox();
            this.lblSpeedToPickCap = new System.Windows.Forms.Label();
            this.gbxTuningRotary = new System.Windows.Forms.GroupBox();
            this.txtTuningRotaryP = new System.Windows.Forms.TextBox();
            this.txtTuningRotaryI = new System.Windows.Forms.TextBox();
            this.txtTuningRotaryD = new System.Windows.Forms.TextBox();
            this.lblTuningRotaryP = new System.Windows.Forms.Label();
            this.lblTuningRotaryDUnits = new System.Windows.Forms.Label();
            this.lblTuningRotaryI = new System.Windows.Forms.Label();
            this.lblTuningRotaryD = new System.Windows.Forms.Label();
            this.lblTuningRotaryIUnits = new System.Windows.Forms.Label();
            this.lblTuningRotaryPUnits = new System.Windows.Forms.Label();
            this.gbxTuningLinear = new System.Windows.Forms.GroupBox();
            this.txtTuningLinearP = new System.Windows.Forms.TextBox();
            this.txtTuningLinearD = new System.Windows.Forms.TextBox();
            this.lblTuningLinearP = new System.Windows.Forms.Label();
            this.txtTuningLinearI = new System.Windows.Forms.TextBox();
            this.lblTuningLinearI = new System.Windows.Forms.Label();
            this.lblTuningLinearD = new System.Windows.Forms.Label();
            this.lblTuningLinearDUnits = new System.Windows.Forms.Label();
            this.lblTuningLinearPUnits = new System.Windows.Forms.Label();
            this.lblTuningLinearIUnits = new System.Windows.Forms.Label();
            this.gbxTeachResult = new System.Windows.Forms.GroupBox();
            this.btnUndo = new System.Windows.Forms.Button();
            this.txtTeachedHeight = new System.Windows.Forms.TextBox();
            this.txtTeachedSqLinearHome = new System.Windows.Forms.TextBox();
            this.txtTeachedSqLinear = new System.Windows.Forms.TextBox();
            this.lblTeachedHeight = new System.Windows.Forms.Label();
            this.lblTeachedSqLinearHome = new System.Windows.Forms.Label();
            this.lblTeachedSqLinear = new System.Windows.Forms.Label();
            this.lblTeachedHeightUnits = new System.Windows.Forms.Label();
            this.lblRotarySensitivity = new System.Windows.Forms.Label();
            this.lblSpeed = new System.Windows.Forms.Label();
            this.txtRotarySensitivity = new System.Windows.Forms.TextBox();
            this.txtSpeed = new System.Windows.Forms.TextBox();
            this.label40 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.txtMaxTorque = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.lblMaxTorque = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.btnRun = new System.Windows.Forms.Button();
            this.btnSaveProgramToFile = new System.Windows.Forms.Button();
            this.btnSingleCycle = new System.Windows.Forms.Button();
            this.btnSaveToController = new System.Windows.Forms.Button();
            this.btnSaveSettingsToFile = new System.Windows.Forms.Button();
            this.btnStop = new System.Windows.Forms.Button();
            this.btnContactInfo = new System.Windows.Forms.Button();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.btnHelpDocument = new System.Windows.Forms.Button();
            this.chkShowAll = new System.Windows.Forms.CheckBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.lblStatusTxt = new System.Windows.Forms.Label();
            this.lblStatus = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label3 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.Run.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxRun)).BeginInit();
            this.Setup.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxSetup)).BeginInit();
            this.Tuning.SuspendLayout();
            this.gbxTuningRotary.SuspendLayout();
            this.gbxTuningLinear.SuspendLayout();
            this.gbxTeachResult.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // txtTerminal
            // 
            this.txtTerminal.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTerminal.Location = new System.Drawing.Point(541, 514);
            this.txtTerminal.Margin = new System.Windows.Forms.Padding(4);
            this.txtTerminal.Multiline = true;
            this.txtTerminal.Name = "txtTerminal";
            this.txtTerminal.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtTerminal.Size = new System.Drawing.Size(556, 334);
            this.txtTerminal.TabIndex = 0;
            this.txtTerminal.WordWrap = false;
            this.txtTerminal.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtTerminal_KeyPress);
            // 
            // txtPositionCloseToPart
            // 
            this.txtPositionCloseToPart.Location = new System.Drawing.Point(964, 190);
            this.txtPositionCloseToPart.Margin = new System.Windows.Forms.Padding(4);
            this.txtPositionCloseToPart.Name = "txtPositionCloseToPart";
            this.txtPositionCloseToPart.Size = new System.Drawing.Size(72, 22);
            this.txtPositionCloseToPart.TabIndex = 504;
            this.txtPositionCloseToPart.Text = "50";
            this.txtPositionCloseToPart.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtPositionCloseToPart_KeyUp);
            this.txtPositionCloseToPart.Leave += new System.EventHandler(this.txtPositionCloseToPart_Leave);
            // 
            // lblPositionCloseToPart
            // 
            this.lblPositionCloseToPart.AutoSize = true;
            this.lblPositionCloseToPart.Location = new System.Drawing.Point(615, 190);
            this.lblPositionCloseToPart.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblPositionCloseToPart.Name = "lblPositionCloseToPart";
            this.lblPositionCloseToPart.Size = new System.Drawing.Size(210, 17);
            this.lblPositionCloseToPart.TabIndex = 2;
            this.lblPositionCloseToPart.Text = "Position close to Container [mm]";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(720, 492);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(113, 17);
            this.label8.TabIndex = 2;
            this.label8.Text = "Controller output";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(643, 338);
            this.label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(0, 17);
            this.label11.TabIndex = 2;
            // 
            // cmbActuator
            // 
            this.cmbActuator.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbActuator.FormattingEnabled = true;
            this.cmbActuator.Location = new System.Drawing.Point(733, 14);
            this.cmbActuator.Margin = new System.Windows.Forms.Padding(4);
            this.cmbActuator.Name = "cmbActuator";
            this.cmbActuator.Size = new System.Drawing.Size(303, 24);
            this.cmbActuator.TabIndex = 500;
            this.cmbActuator.SelectedIndexChanged += new System.EventHandler(this.cmbActuator_SelectedIndexChanged);
            // 
            // lblActuator
            // 
            this.lblActuator.AutoSize = true;
            this.lblActuator.Location = new System.Drawing.Point(615, 20);
            this.lblActuator.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblActuator.Name = "lblActuator";
            this.lblActuator.Size = new System.Drawing.Size(61, 17);
            this.lblActuator.TabIndex = 2;
            this.lblActuator.Text = "Actuator";
            // 
            // lblPort
            // 
            this.lblPort.AutoSize = true;
            this.lblPort.Location = new System.Drawing.Point(1133, 58);
            this.lblPort.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblPort.Name = "lblPort";
            this.lblPort.Size = new System.Drawing.Size(34, 17);
            this.lblPort.TabIndex = 2;
            this.lblPort.Text = "Port";
            // 
            // cmbPort
            // 
            this.cmbPort.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbPort.FormattingEnabled = true;
            this.cmbPort.Location = new System.Drawing.Point(1183, 54);
            this.cmbPort.Margin = new System.Windows.Forms.Padding(4);
            this.cmbPort.Name = "cmbPort";
            this.cmbPort.Size = new System.Drawing.Size(104, 24);
            this.cmbPort.Sorted = true;
            this.cmbPort.TabIndex = 109;
            this.cmbPort.SelectedIndexChanged += new System.EventHandler(this.cmbPort_SelectedIndexChanged);
            this.cmbPort.MouseDown += new System.Windows.Forms.MouseEventHandler(this.cmbPort_MouseDown);
            // 
            // btnLoadSettingsFromFile
            // 
            this.btnLoadSettingsFromFile.Location = new System.Drawing.Point(1132, 123);
            this.btnLoadSettingsFromFile.Margin = new System.Windows.Forms.Padding(4);
            this.btnLoadSettingsFromFile.Name = "btnLoadSettingsFromFile";
            this.btnLoadSettingsFromFile.Size = new System.Drawing.Size(157, 43);
            this.btnLoadSettingsFromFile.TabIndex = 110;
            this.btnLoadSettingsFromFile.Text = "Load settings from file";
            this.btnLoadSettingsFromFile.UseVisualStyleBackColor = true;
            this.btnLoadSettingsFromFile.Click += new System.EventHandler(this.btnLoadSettingsFromFile_Click);
            // 
            // chart1
            // 
            chartArea1.Name = "ChartArea1";
            this.chart1.ChartAreas.Add(chartArea1);
            this.chart1.Location = new System.Drawing.Point(13, 514);
            this.chart1.Margin = new System.Windows.Forms.Padding(4);
            this.chart1.Name = "chart1";
            series1.ChartArea = "ChartArea1";
            series1.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Point;
            series1.Name = "Series1";
            this.chart1.Series.Add(series1);
            this.chart1.Size = new System.Drawing.Size(503, 335);
            this.chart1.TabIndex = 112;
            this.chart1.Text = "chart1";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.label15.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label15.Location = new System.Drawing.Point(141, 492);
            this.label15.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(262, 19);
            this.label15.TabIndex = 113;
            this.label15.Text = "Rotary Position Error vs. Linear Position";
            // 
            // btnActuatorInfo
            // 
            this.btnActuatorInfo.Location = new System.Drawing.Point(1045, 14);
            this.btnActuatorInfo.Margin = new System.Windows.Forms.Padding(4);
            this.btnActuatorInfo.Name = "btnActuatorInfo";
            this.btnActuatorInfo.Size = new System.Drawing.Size(65, 28);
            this.btnActuatorInfo.TabIndex = 114;
            this.btnActuatorInfo.Text = "Info";
            this.btnActuatorInfo.UseVisualStyleBackColor = true;
            this.btnActuatorInfo.Click += new System.EventHandler(this.btnActuatorinfo_Click);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.Run);
            this.tabControl1.Controls.Add(this.Setup);
            this.tabControl1.Controls.Add(this.Tuning);
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Margin = new System.Windows.Forms.Padding(4);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1129, 489);
            this.tabControl1.TabIndex = 115;
            this.tabControl1.SelectedIndexChanged += new System.EventHandler(this.tabControl1_SelectedIndexChanged);
            // 
            // Run
            // 
            this.Run.Controls.Add(this.lblRotaryRevs_Run);
            this.Run.Controls.Add(this.lblLinearDistance_Run);
            this.Run.Controls.Add(this.lblLinDepthRun);
            this.Run.Controls.Add(this.lblRotaryDepthRun);
            this.Run.Controls.Add(this.listBox1);
            this.Run.Controls.Add(this.lblThreadStartRun);
            this.Run.Controls.Add(this.lblHomePositionRun);
            this.Run.Controls.Add(this.lblPluginfoRun);
            this.Run.Controls.Add(this.lblThreadDepthMeasured);
            this.Run.Controls.Add(this.lblLandingHeight);
            this.Run.Controls.Add(this.pictureBoxRun);
            this.Run.Location = new System.Drawing.Point(4, 25);
            this.Run.Margin = new System.Windows.Forms.Padding(4);
            this.Run.Name = "Run";
            this.Run.Padding = new System.Windows.Forms.Padding(4);
            this.Run.Size = new System.Drawing.Size(1121, 460);
            this.Run.TabIndex = 0;
            this.Run.Text = "Run";
            this.Run.UseVisualStyleBackColor = true;
            // 
            // lblRotaryRevs_Run
            // 
            this.lblRotaryRevs_Run.AutoSize = true;
            this.lblRotaryRevs_Run.BackColor = System.Drawing.Color.Transparent;
            this.lblRotaryRevs_Run.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold);
            this.lblRotaryRevs_Run.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.lblRotaryRevs_Run.Location = new System.Drawing.Point(618, 382);
            this.lblRotaryRevs_Run.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblRotaryRevs_Run.Name = "lblRotaryRevs_Run";
            this.lblRotaryRevs_Run.Size = new System.Drawing.Size(126, 20);
            this.lblRotaryRevs_Run.TabIndex = 572;
            this.lblRotaryRevs_Run.Text = "lblRotaryRevs";
            // 
            // lblLinearDistance_Run
            // 
            this.lblLinearDistance_Run.AutoSize = true;
            this.lblLinearDistance_Run.BackColor = System.Drawing.Color.Transparent;
            this.lblLinearDistance_Run.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold);
            this.lblLinearDistance_Run.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.lblLinearDistance_Run.Location = new System.Drawing.Point(149, 361);
            this.lblLinearDistance_Run.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblLinearDistance_Run.Name = "lblLinearDistance_Run";
            this.lblLinearDistance_Run.Size = new System.Drawing.Size(157, 20);
            this.lblLinearDistance_Run.TabIndex = 533;
            this.lblLinearDistance_Run.Text = "lblLinearDistance";
            this.lblLinearDistance_Run.Click += new System.EventHandler(this.lblLinearDistance_Run_Click);
            // 
            // lblLinDepthRun
            // 
            this.lblLinDepthRun.BackColor = System.Drawing.Color.Transparent;
            this.lblLinDepthRun.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLinDepthRun.Location = new System.Drawing.Point(195, 317);
            this.lblLinDepthRun.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblLinDepthRun.Name = "lblLinDepthRun";
            this.lblLinDepthRun.Size = new System.Drawing.Size(89, 54);
            this.lblLinDepthRun.TabIndex = 532;
            this.lblLinDepthRun.Text = "Linear distance";
            this.lblLinDepthRun.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblRotaryDepthRun
            // 
            this.lblRotaryDepthRun.BackColor = System.Drawing.Color.Transparent;
            this.lblRotaryDepthRun.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRotaryDepthRun.Location = new System.Drawing.Point(600, 334);
            this.lblRotaryDepthRun.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblRotaryDepthRun.Name = "lblRotaryDepthRun";
            this.lblRotaryDepthRun.Size = new System.Drawing.Size(186, 48);
            this.lblRotaryDepthRun.TabIndex = 531;
            this.lblRotaryDepthRun.Text = "Depth in Number of Revolutions";
            this.lblRotaryDepthRun.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // listBox1
            // 
            this.listBox1.FormattingEnabled = true;
            this.listBox1.ItemHeight = 16;
            this.listBox1.Location = new System.Drawing.Point(931, 412);
            this.listBox1.Margin = new System.Windows.Forms.Padding(4);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(68, 388);
            this.listBox1.TabIndex = 119;
            // 
            // lblThreadStartRun
            // 
            this.lblThreadStartRun.AutoSize = true;
            this.lblThreadStartRun.BackColor = System.Drawing.Color.Transparent;
            this.lblThreadStartRun.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblThreadStartRun.Location = new System.Drawing.Point(624, 301);
            this.lblThreadStartRun.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblThreadStartRun.Name = "lblThreadStartRun";
            this.lblThreadStartRun.Size = new System.Drawing.Size(120, 20);
            this.lblThreadStartRun.TabIndex = 531;
            this.lblThreadStartRun.Text = "Thread Start ";
            this.lblThreadStartRun.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblHomePositionRun
            // 
            this.lblHomePositionRun.AutoSize = true;
            this.lblHomePositionRun.BackColor = System.Drawing.Color.Transparent;
            this.lblHomePositionRun.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHomePositionRun.Location = new System.Drawing.Point(624, 190);
            this.lblHomePositionRun.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblHomePositionRun.Name = "lblHomePositionRun";
            this.lblHomePositionRun.Size = new System.Drawing.Size(130, 20);
            this.lblHomePositionRun.TabIndex = 531;
            this.lblHomePositionRun.Text = "Home position";
            // 
            // lblPluginfoRun
            // 
            this.lblPluginfoRun.BackColor = System.Drawing.Color.SteelBlue;
            this.lblPluginfoRun.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPluginfoRun.Location = new System.Drawing.Point(168, 190);
            this.lblPluginfoRun.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblPluginfoRun.Name = "lblPluginfoRun";
            this.lblPluginfoRun.Size = new System.Drawing.Size(155, 78);
            this.lblPluginfoRun.TabIndex = 528;
            this.lblPluginfoRun.Text = "Tooling to hold/grip cap";
            this.lblPluginfoRun.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblThreadDepthMeasured
            // 
            this.lblThreadDepthMeasured.AutoSize = true;
            this.lblThreadDepthMeasured.BackColor = System.Drawing.Color.Transparent;
            this.lblThreadDepthMeasured.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblThreadDepthMeasured.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.lblThreadDepthMeasured.Location = new System.Drawing.Point(672, 43);
            this.lblThreadDepthMeasured.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblThreadDepthMeasured.Name = "lblThreadDepthMeasured";
            this.lblThreadDepthMeasured.Size = new System.Drawing.Size(256, 25);
            this.lblThreadDepthMeasured.TabIndex = 522;
            this.lblThreadDepthMeasured.Text = "lblThreadDepthMeasured";
            // 
            // lblLandingHeight
            // 
            this.lblLandingHeight.AutoSize = true;
            this.lblLandingHeight.BackColor = System.Drawing.Color.Transparent;
            this.lblLandingHeight.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLandingHeight.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.lblLandingHeight.Location = new System.Drawing.Point(761, 18);
            this.lblLandingHeight.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblLandingHeight.Name = "lblLandingHeight";
            this.lblLandingHeight.Size = new System.Drawing.Size(173, 25);
            this.lblLandingHeight.TabIndex = 521;
            this.lblLandingHeight.Text = "lblLandingHeight";
            // 
            // pictureBoxRun
            // 
            this.pictureBoxRun.Image = ((System.Drawing.Image)(resources.GetObject("pictureBoxRun.Image")));
            this.pictureBoxRun.Location = new System.Drawing.Point(109, 18);
            this.pictureBoxRun.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBoxRun.Name = "pictureBoxRun";
            this.pictureBoxRun.Size = new System.Drawing.Size(844, 429);
            this.pictureBoxRun.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBoxRun.TabIndex = 520;
            this.pictureBoxRun.TabStop = false;
            // 
            // Setup
            // 
            this.Setup.Controls.Add(this.lblRotaryRevs_Setup);
            this.Setup.Controls.Add(this.lblLinearDistance_Setup);
            this.Setup.Controls.Add(this.txtNumofThreads);
            this.Setup.Controls.Add(this.label2);
            this.Setup.Controls.Add(this.txtRetractDistance);
            this.Setup.Controls.Add(this.lblRetractDistance);
            this.Setup.Controls.Add(this.btnTeachPickCap);
            this.Setup.Controls.Add(this.txtPositionCloseToPickCap);
            this.Setup.Controls.Add(this.lblPositionCloseToPickCap);
            this.Setup.Controls.Add(this.cmbCappingType);
            this.Setup.Controls.Add(this.lblCappingType);
            this.Setup.Controls.Add(this.lblLinDepthSetup);
            this.Setup.Controls.Add(this.lblMinRotDistTravel);
            this.Setup.Controls.Add(this.lblMaxRotDistTravel);
            this.Setup.Controls.Add(this.txtMinRotDistTravel);
            this.Setup.Controls.Add(this.txtMaxRotDistTravel);
            this.Setup.Controls.Add(this.chkRotDistTravel);
            this.Setup.Controls.Add(this.lblMinLinTravel);
            this.Setup.Controls.Add(this.lblMaxLinTravel);
            this.Setup.Controls.Add(this.txtMinLinTravel);
            this.Setup.Controls.Add(this.txtMaxLinTravel);
            this.Setup.Controls.Add(this.chkLinDistTravel);
            this.Setup.Controls.Add(this.lblCloseToPart);
            this.Setup.Controls.Add(this.lblHomePositionSetup);
            this.Setup.Controls.Add(this.lblThreadStartSetup);
            this.Setup.Controls.Add(this.lblRotDepthSetup);
            this.Setup.Controls.Add(this.lblPluginfoSetup);
            this.Setup.Controls.Add(this.lblThreadDepthMeasured2);
            this.Setup.Controls.Add(this.lblLandingHeight2);
            this.Setup.Controls.Add(this.label44);
            this.Setup.Controls.Add(this.label42);
            this.Setup.Controls.Add(this.btnTeach);
            this.Setup.Controls.Add(this.lblLandedHeightMax);
            this.Setup.Controls.Add(this.lblLandedHeightMin);
            this.Setup.Controls.Add(this.txtLandedHeightMax);
            this.Setup.Controls.Add(this.txtLandedHeightMin);
            this.Setup.Controls.Add(this.chkCheckLandedHeight);
            this.Setup.Controls.Add(this.chkRotateReverse);
            this.Setup.Controls.Add(this.cmbThreadDirection);
            this.Setup.Controls.Add(this.pictureBoxSetup);
            this.Setup.Controls.Add(this.txtPositionCloseToPart);
            this.Setup.Controls.Add(this.btnActuatorInfo);
            this.Setup.Controls.Add(this.lblPitch);
            this.Setup.Controls.Add(this.lblPositionCloseToPart);
            this.Setup.Controls.Add(this.cmbActuator);
            this.Setup.Controls.Add(this.lblThreadDirection);
            this.Setup.Controls.Add(this.lblActuator);
            this.Setup.Location = new System.Drawing.Point(4, 25);
            this.Setup.Margin = new System.Windows.Forms.Padding(4);
            this.Setup.Name = "Setup";
            this.Setup.Padding = new System.Windows.Forms.Padding(4);
            this.Setup.Size = new System.Drawing.Size(1121, 460);
            this.Setup.TabIndex = 1;
            this.Setup.Text = "Setup";
            this.Setup.UseVisualStyleBackColor = true;
            // 
            // lblRotaryRevs_Setup
            // 
            this.lblRotaryRevs_Setup.AutoSize = true;
            this.lblRotaryRevs_Setup.BackColor = System.Drawing.Color.Transparent;
            this.lblRotaryRevs_Setup.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold);
            this.lblRotaryRevs_Setup.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.lblRotaryRevs_Setup.Location = new System.Drawing.Point(454, 383);
            this.lblRotaryRevs_Setup.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblRotaryRevs_Setup.Name = "lblRotaryRevs_Setup";
            this.lblRotaryRevs_Setup.Size = new System.Drawing.Size(126, 20);
            this.lblRotaryRevs_Setup.TabIndex = 571;
            this.lblRotaryRevs_Setup.Text = "lblRotaryRevs";
            // 
            // lblLinearDistance_Setup
            // 
            this.lblLinearDistance_Setup.AutoSize = true;
            this.lblLinearDistance_Setup.BackColor = System.Drawing.Color.Transparent;
            this.lblLinearDistance_Setup.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold);
            this.lblLinearDistance_Setup.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.lblLinearDistance_Setup.Location = new System.Drawing.Point(12, 352);
            this.lblLinearDistance_Setup.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblLinearDistance_Setup.Name = "lblLinearDistance_Setup";
            this.lblLinearDistance_Setup.Size = new System.Drawing.Size(157, 20);
            this.lblLinearDistance_Setup.TabIndex = 570;
            this.lblLinearDistance_Setup.Text = "lblLinearDistance";
            // 
            // txtNumofThreads
            // 
            this.txtNumofThreads.Location = new System.Drawing.Point(964, 101);
            this.txtNumofThreads.Margin = new System.Windows.Forms.Padding(4);
            this.txtNumofThreads.Name = "txtNumofThreads";
            this.txtNumofThreads.Size = new System.Drawing.Size(72, 22);
            this.txtNumofThreads.TabIndex = 568;
            this.txtNumofThreads.Text = "1";
            this.txtNumofThreads.TextChanged += new System.EventHandler(this.txtNumofThreads_TextChanged);
            this.txtNumofThreads.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtNumofThreads_KeyUp);
            this.txtNumofThreads.Leave += new System.EventHandler(this.txtNumofThreads_Leave);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(615, 107);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(131, 17);
            this.label2.TabIndex = 567;
            this.label2.Text = "Number of Threads";
            // 
            // txtRetractDistance
            // 
            this.txtRetractDistance.Location = new System.Drawing.Point(964, 160);
            this.txtRetractDistance.Margin = new System.Windows.Forms.Padding(4);
            this.txtRetractDistance.Name = "txtRetractDistance";
            this.txtRetractDistance.Size = new System.Drawing.Size(72, 22);
            this.txtRetractDistance.TabIndex = 566;
            this.txtRetractDistance.Text = "10";
            this.txtRetractDistance.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtRetractDistance_KeyUp);
            this.txtRetractDistance.Leave += new System.EventHandler(this.txtRetractDistance_Leave);
            // 
            // lblRetractDistance
            // 
            this.lblRetractDistance.AutoSize = true;
            this.lblRetractDistance.Location = new System.Drawing.Point(615, 160);
            this.lblRetractDistance.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblRetractDistance.Name = "lblRetractDistance";
            this.lblRetractDistance.Size = new System.Drawing.Size(238, 17);
            this.lblRetractDistance.TabIndex = 565;
            this.lblRetractDistance.Text = "Retract Distance  from Pick Cap[mm]";
            // 
            // btnTeachPickCap
            // 
            this.btnTeachPickCap.Location = new System.Drawing.Point(1045, 131);
            this.btnTeachPickCap.Margin = new System.Windows.Forms.Padding(4);
            this.btnTeachPickCap.Name = "btnTeachPickCap";
            this.btnTeachPickCap.Size = new System.Drawing.Size(65, 28);
            this.btnTeachPickCap.TabIndex = 561;
            this.btnTeachPickCap.Text = "Teach";
            this.btnTeachPickCap.UseVisualStyleBackColor = true;
            this.btnTeachPickCap.Click += new System.EventHandler(this.btnTeachPickCap_Click);
            // 
            // txtPositionCloseToPickCap
            // 
            this.txtPositionCloseToPickCap.Location = new System.Drawing.Point(964, 131);
            this.txtPositionCloseToPickCap.Margin = new System.Windows.Forms.Padding(4);
            this.txtPositionCloseToPickCap.Name = "txtPositionCloseToPickCap";
            this.txtPositionCloseToPickCap.Size = new System.Drawing.Size(72, 22);
            this.txtPositionCloseToPickCap.TabIndex = 562;
            this.txtPositionCloseToPickCap.Text = "0";
            this.txtPositionCloseToPickCap.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtPositionCloseToPickCap_KeyUp);
            this.txtPositionCloseToPickCap.Leave += new System.EventHandler(this.txtPositionCloseToPickCap_Leave);
            // 
            // lblPositionCloseToPickCap
            // 
            this.lblPositionCloseToPickCap.AutoSize = true;
            this.lblPositionCloseToPickCap.Location = new System.Drawing.Point(615, 131);
            this.lblPositionCloseToPickCap.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblPositionCloseToPickCap.Name = "lblPositionCloseToPickCap";
            this.lblPositionCloseToPickCap.Size = new System.Drawing.Size(125, 17);
            this.lblPositionCloseToPickCap.TabIndex = 560;
            this.lblPositionCloseToPickCap.Text = "Park Position [mm]";
            // 
            // cmbCappingType
            // 
            this.cmbCappingType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbCappingType.FormattingEnabled = true;
            this.cmbCappingType.Items.AddRange(new object[] {
            "Cap Already In Place On Container",
            "Picking Cap, Then Installing"});
            this.cmbCappingType.Location = new System.Drawing.Point(733, 68);
            this.cmbCappingType.Margin = new System.Windows.Forms.Padding(4);
            this.cmbCappingType.Name = "cmbCappingType";
            this.cmbCappingType.Size = new System.Drawing.Size(303, 24);
            this.cmbCappingType.TabIndex = 555;
            this.cmbCappingType.SelectedIndexChanged += new System.EventHandler(this.cmbCappingType_SelectedIndexChanged);
            // 
            // lblCappingType
            // 
            this.lblCappingType.AutoSize = true;
            this.lblCappingType.Location = new System.Drawing.Point(615, 68);
            this.lblCappingType.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblCappingType.Name = "lblCappingType";
            this.lblCappingType.Size = new System.Drawing.Size(96, 17);
            this.lblCappingType.TabIndex = 554;
            this.lblCappingType.Text = "Capping Type";
            // 
            // lblLinDepthSetup
            // 
            this.lblLinDepthSetup.BackColor = System.Drawing.Color.Transparent;
            this.lblLinDepthSetup.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLinDepthSetup.Location = new System.Drawing.Point(9, 328);
            this.lblLinDepthSetup.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblLinDepthSetup.Name = "lblLinDepthSetup";
            this.lblLinDepthSetup.Size = new System.Drawing.Size(160, 53);
            this.lblLinDepthSetup.TabIndex = 553;
            this.lblLinDepthSetup.Text = "Linear Distance";
            this.lblLinDepthSetup.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lblMinRotDistTravel
            // 
            this.lblMinRotDistTravel.AutoSize = true;
            this.lblMinRotDistTravel.Location = new System.Drawing.Point(757, 378);
            this.lblMinRotDistTravel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblMinRotDistTravel.Name = "lblMinRotDistTravel";
            this.lblMinRotDistTravel.Size = new System.Drawing.Size(102, 17);
            this.lblMinRotDistTravel.TabIndex = 549;
            this.lblMinRotDistTravel.Text = "Min Limit [revs]";
            // 
            // lblMaxRotDistTravel
            // 
            this.lblMaxRotDistTravel.AutoSize = true;
            this.lblMaxRotDistTravel.Location = new System.Drawing.Point(757, 398);
            this.lblMaxRotDistTravel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblMaxRotDistTravel.Name = "lblMaxRotDistTravel";
            this.lblMaxRotDistTravel.Size = new System.Drawing.Size(105, 17);
            this.lblMaxRotDistTravel.TabIndex = 550;
            this.lblMaxRotDistTravel.Text = "Max Limit [revs]";
            // 
            // txtMinRotDistTravel
            // 
            this.txtMinRotDistTravel.Location = new System.Drawing.Point(964, 378);
            this.txtMinRotDistTravel.Margin = new System.Windows.Forms.Padding(4);
            this.txtMinRotDistTravel.Name = "txtMinRotDistTravel";
            this.txtMinRotDistTravel.Size = new System.Drawing.Size(72, 22);
            this.txtMinRotDistTravel.TabIndex = 552;
            this.txtMinRotDistTravel.Text = "55";
            this.txtMinRotDistTravel.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtMinRotDistTravel_KeyUp);
            this.txtMinRotDistTravel.Leave += new System.EventHandler(this.txtMinRotDistTravel_Leave);
            // 
            // txtMaxRotDistTravel
            // 
            this.txtMaxRotDistTravel.Location = new System.Drawing.Point(964, 405);
            this.txtMaxRotDistTravel.Margin = new System.Windows.Forms.Padding(4);
            this.txtMaxRotDistTravel.Name = "txtMaxRotDistTravel";
            this.txtMaxRotDistTravel.Size = new System.Drawing.Size(72, 22);
            this.txtMaxRotDistTravel.TabIndex = 551;
            this.txtMaxRotDistTravel.Text = "51";
            this.txtMaxRotDistTravel.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtMaxRotDistTravel_KeyUp);
            this.txtMaxRotDistTravel.Leave += new System.EventHandler(this.txtMaxRotDistTravel_Leave);
            // 
            // chkRotDistTravel
            // 
            this.chkRotDistTravel.AutoSize = true;
            this.chkRotDistTravel.Location = new System.Drawing.Point(619, 357);
            this.chkRotDistTravel.Margin = new System.Windows.Forms.Padding(4);
            this.chkRotDistTravel.Name = "chkRotDistTravel";
            this.chkRotDistTravel.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.chkRotDistTravel.Size = new System.Drawing.Size(237, 21);
            this.chkRotDistTravel.TabIndex = 548;
            this.chkRotDistTravel.Text = "Check Rotary Distance Travelled";
            this.chkRotDistTravel.UseVisualStyleBackColor = true;
            this.chkRotDistTravel.CheckedChanged += new System.EventHandler(this.chkRotDistTravel_CheckedChanged);
            // 
            // lblMinLinTravel
            // 
            this.lblMinLinTravel.AutoSize = true;
            this.lblMinLinTravel.Location = new System.Drawing.Point(757, 317);
            this.lblMinLinTravel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblMinLinTravel.Name = "lblMinLinTravel";
            this.lblMinLinTravel.Size = new System.Drawing.Size(97, 17);
            this.lblMinLinTravel.TabIndex = 543;
            this.lblMinLinTravel.Text = "Min Limit [mm]";
            // 
            // lblMaxLinTravel
            // 
            this.lblMaxLinTravel.AutoSize = true;
            this.lblMaxLinTravel.Location = new System.Drawing.Point(757, 339);
            this.lblMaxLinTravel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblMaxLinTravel.Name = "lblMaxLinTravel";
            this.lblMaxLinTravel.Size = new System.Drawing.Size(100, 17);
            this.lblMaxLinTravel.TabIndex = 544;
            this.lblMaxLinTravel.Text = "Max Limit [mm]";
            // 
            // txtMinLinTravel
            // 
            this.txtMinLinTravel.Location = new System.Drawing.Point(964, 310);
            this.txtMinLinTravel.Margin = new System.Windows.Forms.Padding(4);
            this.txtMinLinTravel.Name = "txtMinLinTravel";
            this.txtMinLinTravel.Size = new System.Drawing.Size(72, 22);
            this.txtMinLinTravel.TabIndex = 547;
            this.txtMinLinTravel.Text = "55";
            this.txtMinLinTravel.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtMinLinTravel_KeyUp);
            this.txtMinLinTravel.Leave += new System.EventHandler(this.txtMinLinTravel_Leave);
            // 
            // txtMaxLinTravel
            // 
            this.txtMaxLinTravel.Location = new System.Drawing.Point(964, 334);
            this.txtMaxLinTravel.Margin = new System.Windows.Forms.Padding(4);
            this.txtMaxLinTravel.Name = "txtMaxLinTravel";
            this.txtMaxLinTravel.Size = new System.Drawing.Size(72, 22);
            this.txtMaxLinTravel.TabIndex = 546;
            this.txtMaxLinTravel.Text = "51";
            this.txtMaxLinTravel.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtMaxLinTravel_KeyUp);
            this.txtMaxLinTravel.Leave += new System.EventHandler(this.txtMaxLinTravel_Leave);
            // 
            // chkLinDistTravel
            // 
            this.chkLinDistTravel.AutoSize = true;
            this.chkLinDistTravel.Location = new System.Drawing.Point(619, 297);
            this.chkLinDistTravel.Margin = new System.Windows.Forms.Padding(4);
            this.chkLinDistTravel.Name = "chkLinDistTravel";
            this.chkLinDistTravel.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.chkLinDistTravel.Size = new System.Drawing.Size(235, 21);
            this.chkLinDistTravel.TabIndex = 542;
            this.chkLinDistTravel.Text = "Check Linear Distance Travelled";
            this.chkLinDistTravel.UseVisualStyleBackColor = true;
            this.chkLinDistTravel.CheckedChanged += new System.EventHandler(this.chkLinDistTravel_CheckedChanged);
            // 
            // lblCloseToPart
            // 
            this.lblCloseToPart.BackColor = System.Drawing.Color.Transparent;
            this.lblCloseToPart.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCloseToPart.Location = new System.Drawing.Point(16, 205);
            this.lblCloseToPart.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblCloseToPart.Name = "lblCloseToPart";
            this.lblCloseToPart.Size = new System.Drawing.Size(153, 69);
            this.lblCloseToPart.TabIndex = 538;
            this.lblCloseToPart.Text = "Position close to container";
            this.lblCloseToPart.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lblHomePositionSetup
            // 
            this.lblHomePositionSetup.AutoSize = true;
            this.lblHomePositionSetup.BackColor = System.Drawing.Color.Transparent;
            this.lblHomePositionSetup.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHomePositionSetup.Location = new System.Drawing.Point(465, 187);
            this.lblHomePositionSetup.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblHomePositionSetup.Name = "lblHomePositionSetup";
            this.lblHomePositionSetup.Size = new System.Drawing.Size(130, 20);
            this.lblHomePositionSetup.TabIndex = 538;
            this.lblHomePositionSetup.Text = "Home position";
            // 
            // lblThreadStartSetup
            // 
            this.lblThreadStartSetup.AutoSize = true;
            this.lblThreadStartSetup.BackColor = System.Drawing.Color.Transparent;
            this.lblThreadStartSetup.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblThreadStartSetup.Location = new System.Drawing.Point(465, 295);
            this.lblThreadStartSetup.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblThreadStartSetup.Name = "lblThreadStartSetup";
            this.lblThreadStartSetup.Size = new System.Drawing.Size(114, 20);
            this.lblThreadStartSetup.TabIndex = 537;
            this.lblThreadStartSetup.Text = "Thread Start";
            // 
            // lblRotDepthSetup
            // 
            this.lblRotDepthSetup.BackColor = System.Drawing.Color.Transparent;
            this.lblRotDepthSetup.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRotDepthSetup.Location = new System.Drawing.Point(452, 316);
            this.lblRotDepthSetup.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblRotDepthSetup.Name = "lblRotDepthSetup";
            this.lblRotDepthSetup.Size = new System.Drawing.Size(123, 62);
            this.lblRotDepthSetup.TabIndex = 536;
            this.lblRotDepthSetup.Text = "Depth in Number of Revolutions";
            this.lblRotDepthSetup.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // lblPluginfoSetup
            // 
            this.lblPluginfoSetup.BackColor = System.Drawing.Color.SteelBlue;
            this.lblPluginfoSetup.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPluginfoSetup.Location = new System.Drawing.Point(74, 135);
            this.lblPluginfoSetup.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblPluginfoSetup.Name = "lblPluginfoSetup";
            this.lblPluginfoSetup.Size = new System.Drawing.Size(140, 56);
            this.lblPluginfoSetup.TabIndex = 532;
            this.lblPluginfoSetup.Text = "Tooling to hold/grip cap";
            this.lblPluginfoSetup.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblPluginfoSetup.Click += new System.EventHandler(this.lblPluginfoSetup_Click);
            // 
            // lblThreadDepthMeasured2
            // 
            this.lblThreadDepthMeasured2.AutoSize = true;
            this.lblThreadDepthMeasured2.BackColor = System.Drawing.Color.Transparent;
            this.lblThreadDepthMeasured2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblThreadDepthMeasured2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.lblThreadDepthMeasured2.Location = new System.Drawing.Point(420, 147);
            this.lblThreadDepthMeasured2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblThreadDepthMeasured2.Name = "lblThreadDepthMeasured2";
            this.lblThreadDepthMeasured2.Size = new System.Drawing.Size(256, 25);
            this.lblThreadDepthMeasured2.TabIndex = 525;
            this.lblThreadDepthMeasured2.Text = "lblThreadDepthMeasured";
            // 
            // lblLandingHeight2
            // 
            this.lblLandingHeight2.AutoSize = true;
            this.lblLandingHeight2.BackColor = System.Drawing.Color.Transparent;
            this.lblLandingHeight2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLandingHeight2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.lblLandingHeight2.Location = new System.Drawing.Point(464, 212);
            this.lblLandingHeight2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblLandingHeight2.Name = "lblLandingHeight2";
            this.lblLandingHeight2.Size = new System.Drawing.Size(185, 25);
            this.lblLandingHeight2.TabIndex = 524;
            this.lblLandingHeight2.Text = "lblLandingHeight2";
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Location = new System.Drawing.Point(721, 517);
            this.label44.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(88, 17);
            this.label44.TabIndex = 519;
            this.label44.Text = "Teached SQ";
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Location = new System.Drawing.Point(721, 505);
            this.label42.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(88, 17);
            this.label42.TabIndex = 519;
            this.label42.Text = "Teached SQ";
            // 
            // btnTeach
            // 
            this.btnTeach.Location = new System.Drawing.Point(1045, 190);
            this.btnTeach.Margin = new System.Windows.Forms.Padding(4);
            this.btnTeach.Name = "btnTeach";
            this.btnTeach.Size = new System.Drawing.Size(65, 28);
            this.btnTeach.TabIndex = 120;
            this.btnTeach.Text = "Teach";
            this.btnTeach.UseVisualStyleBackColor = true;
            this.btnTeach.Click += new System.EventHandler(this.btnTeach_Click);
            // 
            // lblLandedHeightMax
            // 
            this.lblLandedHeightMax.AutoSize = true;
            this.lblLandedHeightMax.Location = new System.Drawing.Point(757, 254);
            this.lblLandedHeightMax.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblLandedHeightMax.Name = "lblLandedHeightMax";
            this.lblLandedHeightMax.Size = new System.Drawing.Size(162, 17);
            this.lblLandedHeightMax.TabIndex = 119;
            this.lblLandedHeightMax.Text = "Landed height max [mm]";
            // 
            // lblLandedHeightMin
            // 
            this.lblLandedHeightMin.AutoSize = true;
            this.lblLandedHeightMin.Location = new System.Drawing.Point(757, 232);
            this.lblLandedHeightMin.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblLandedHeightMin.Name = "lblLandedHeightMin";
            this.lblLandedHeightMin.Size = new System.Drawing.Size(159, 17);
            this.lblLandedHeightMin.TabIndex = 119;
            this.lblLandedHeightMin.Text = "Landed height min [mm]";
            // 
            // txtLandedHeightMax
            // 
            this.txtLandedHeightMax.Location = new System.Drawing.Point(964, 252);
            this.txtLandedHeightMax.Margin = new System.Windows.Forms.Padding(4);
            this.txtLandedHeightMax.Name = "txtLandedHeightMax";
            this.txtLandedHeightMax.Size = new System.Drawing.Size(72, 22);
            this.txtLandedHeightMax.TabIndex = 506;
            this.txtLandedHeightMax.Text = "55";
            this.txtLandedHeightMax.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtPartLocationMax_KeyUp);
            this.txtLandedHeightMax.Leave += new System.EventHandler(this.txtPartLocationMax_Leave);
            // 
            // txtLandedHeightMin
            // 
            this.txtLandedHeightMin.Location = new System.Drawing.Point(964, 227);
            this.txtLandedHeightMin.Margin = new System.Windows.Forms.Padding(4);
            this.txtLandedHeightMin.Name = "txtLandedHeightMin";
            this.txtLandedHeightMin.Size = new System.Drawing.Size(72, 22);
            this.txtLandedHeightMin.TabIndex = 505;
            this.txtLandedHeightMin.Text = "51";
            this.txtLandedHeightMin.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtPartLocationMin_KeyUp);
            this.txtLandedHeightMin.Leave += new System.EventHandler(this.txtPartLocationMin_Leave);
            // 
            // chkCheckLandedHeight
            // 
            this.chkCheckLandedHeight.AutoSize = true;
            this.chkCheckLandedHeight.Location = new System.Drawing.Point(619, 212);
            this.chkCheckLandedHeight.Margin = new System.Windows.Forms.Padding(4);
            this.chkCheckLandedHeight.Name = "chkCheckLandedHeight";
            this.chkCheckLandedHeight.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.chkCheckLandedHeight.Size = new System.Drawing.Size(166, 21);
            this.chkCheckLandedHeight.TabIndex = 117;
            this.chkCheckLandedHeight.Text = "Check Landed Height";
            this.chkCheckLandedHeight.UseVisualStyleBackColor = true;
            this.chkCheckLandedHeight.CheckedChanged += new System.EventHandler(this.chkCheckLandedHeight_CheckedChanged);
            // 
            // chkRotateReverse
            // 
            this.chkRotateReverse.AutoSize = true;
            this.chkRotateReverse.Location = new System.Drawing.Point(619, 274);
            this.chkRotateReverse.Margin = new System.Windows.Forms.Padding(4);
            this.chkRotateReverse.Name = "chkRotateReverse";
            this.chkRotateReverse.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.chkRotateReverse.Size = new System.Drawing.Size(312, 21);
            this.chkRotateReverse.TabIndex = 117;
            this.chkRotateReverse.Text = "Rotate in reverse to locate the lead in thread";
            this.chkRotateReverse.UseVisualStyleBackColor = true;
            this.chkRotateReverse.CheckedChanged += new System.EventHandler(this.chkRotateReverse_CheckedChanged);
            // 
            // cmbThreadDirection
            // 
            this.cmbThreadDirection.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbThreadDirection.FormattingEnabled = true;
            this.cmbThreadDirection.Location = new System.Drawing.Point(733, 41);
            this.cmbThreadDirection.Margin = new System.Windows.Forms.Padding(4);
            this.cmbThreadDirection.Name = "cmbThreadDirection";
            this.cmbThreadDirection.Size = new System.Drawing.Size(303, 24);
            this.cmbThreadDirection.TabIndex = 502;
            // 
            // pictureBoxSetup
            // 
            this.pictureBoxSetup.Image = ((System.Drawing.Image)(resources.GetObject("pictureBoxSetup.Image")));
            this.pictureBoxSetup.Location = new System.Drawing.Point(8, 4);
            this.pictureBoxSetup.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBoxSetup.Name = "pictureBoxSetup";
            this.pictureBoxSetup.Size = new System.Drawing.Size(600, 446);
            this.pictureBoxSetup.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBoxSetup.TabIndex = 115;
            this.pictureBoxSetup.TabStop = false;
            // 
            // lblPitch
            // 
            this.lblPitch.Location = new System.Drawing.Point(0, 0);
            this.lblPitch.Name = "lblPitch";
            this.lblPitch.Size = new System.Drawing.Size(100, 23);
            this.lblPitch.TabIndex = 541;
            // 
            // lblThreadDirection
            // 
            this.lblThreadDirection.AutoSize = true;
            this.lblThreadDirection.Location = new System.Drawing.Point(615, 44);
            this.lblThreadDirection.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblThreadDirection.Name = "lblThreadDirection";
            this.lblThreadDirection.Size = new System.Drawing.Size(112, 17);
            this.lblThreadDirection.TabIndex = 2;
            this.lblThreadDirection.Text = "Thread direction";
            // 
            // Tuning
            // 
            this.Tuning.Controls.Add(this.label1);
            this.Tuning.Controls.Add(this.txtSpeedtoPickCap);
            this.Tuning.Controls.Add(this.lblSpeedToPickCap);
            this.Tuning.Controls.Add(this.gbxTuningRotary);
            this.Tuning.Controls.Add(this.gbxTuningLinear);
            this.Tuning.Controls.Add(this.gbxTeachResult);
            this.Tuning.Controls.Add(this.lblRotarySensitivity);
            this.Tuning.Controls.Add(this.lblSpeed);
            this.Tuning.Controls.Add(this.txtRotarySensitivity);
            this.Tuning.Controls.Add(this.txtSpeed);
            this.Tuning.Controls.Add(this.label40);
            this.Tuning.Controls.Add(this.label20);
            this.Tuning.Controls.Add(this.label11);
            this.Tuning.Controls.Add(this.txtMaxTorque);
            this.Tuning.Controls.Add(this.label25);
            this.Tuning.Controls.Add(this.lblMaxTorque);
            this.Tuning.Location = new System.Drawing.Point(4, 25);
            this.Tuning.Margin = new System.Windows.Forms.Padding(4);
            this.Tuning.Name = "Tuning";
            this.Tuning.Size = new System.Drawing.Size(1121, 460);
            this.Tuning.TabIndex = 2;
            this.Tuning.Text = "Tuning";
            this.Tuning.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(608, 169);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(20, 17);
            this.label1.TabIndex = 567;
            this.label1.Text = "%";
            // 
            // txtSpeedtoPickCap
            // 
            this.txtSpeedtoPickCap.Location = new System.Drawing.Point(527, 164);
            this.txtSpeedtoPickCap.Margin = new System.Windows.Forms.Padding(4);
            this.txtSpeedtoPickCap.Name = "txtSpeedtoPickCap";
            this.txtSpeedtoPickCap.Size = new System.Drawing.Size(72, 22);
            this.txtSpeedtoPickCap.TabIndex = 566;
            this.txtSpeedtoPickCap.Text = "10";
            // 
            // lblSpeedToPickCap
            // 
            this.lblSpeedToPickCap.AutoSize = true;
            this.lblSpeedToPickCap.Location = new System.Drawing.Point(360, 164);
            this.lblSpeedToPickCap.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblSpeedToPickCap.Name = "lblSpeedToPickCap";
            this.lblSpeedToPickCap.Size = new System.Drawing.Size(124, 17);
            this.lblSpeedToPickCap.TabIndex = 565;
            this.lblSpeedToPickCap.Text = "Speed to Pick Cap";
            // 
            // gbxTuningRotary
            // 
            this.gbxTuningRotary.Controls.Add(this.txtTuningRotaryP);
            this.gbxTuningRotary.Controls.Add(this.txtTuningRotaryI);
            this.gbxTuningRotary.Controls.Add(this.txtTuningRotaryD);
            this.gbxTuningRotary.Controls.Add(this.lblTuningRotaryP);
            this.gbxTuningRotary.Controls.Add(this.lblTuningRotaryDUnits);
            this.gbxTuningRotary.Controls.Add(this.lblTuningRotaryI);
            this.gbxTuningRotary.Controls.Add(this.lblTuningRotaryD);
            this.gbxTuningRotary.Controls.Add(this.lblTuningRotaryIUnits);
            this.gbxTuningRotary.Controls.Add(this.lblTuningRotaryPUnits);
            this.gbxTuningRotary.Location = new System.Drawing.Point(35, 242);
            this.gbxTuningRotary.Margin = new System.Windows.Forms.Padding(4);
            this.gbxTuningRotary.Name = "gbxTuningRotary";
            this.gbxTuningRotary.Padding = new System.Windows.Forms.Padding(4);
            this.gbxTuningRotary.Size = new System.Drawing.Size(173, 167);
            this.gbxTuningRotary.TabIndex = 531;
            this.gbxTuningRotary.TabStop = false;
            this.gbxTuningRotary.Text = " Tuning rotary ";
            // 
            // txtTuningRotaryP
            // 
            this.txtTuningRotaryP.Location = new System.Drawing.Point(53, 46);
            this.txtTuningRotaryP.Margin = new System.Windows.Forms.Padding(4);
            this.txtTuningRotaryP.Name = "txtTuningRotaryP";
            this.txtTuningRotaryP.Size = new System.Drawing.Size(72, 22);
            this.txtTuningRotaryP.TabIndex = 514;
            this.txtTuningRotaryP.Text = "100";
            this.txtTuningRotaryP.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtTuningRotaryP_KeyUp);
            this.txtTuningRotaryP.Leave += new System.EventHandler(this.txtTuningRotaryP_Leave);
            // 
            // txtTuningRotaryI
            // 
            this.txtTuningRotaryI.Location = new System.Drawing.Point(53, 75);
            this.txtTuningRotaryI.Margin = new System.Windows.Forms.Padding(4);
            this.txtTuningRotaryI.Name = "txtTuningRotaryI";
            this.txtTuningRotaryI.Size = new System.Drawing.Size(72, 22);
            this.txtTuningRotaryI.TabIndex = 515;
            this.txtTuningRotaryI.Text = "100";
            this.txtTuningRotaryI.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtTuningRotaryI_KeyUp);
            this.txtTuningRotaryI.Leave += new System.EventHandler(this.txtTuningRotaryI_Leave);
            // 
            // txtTuningRotaryD
            // 
            this.txtTuningRotaryD.Location = new System.Drawing.Point(53, 105);
            this.txtTuningRotaryD.Margin = new System.Windows.Forms.Padding(4);
            this.txtTuningRotaryD.Name = "txtTuningRotaryD";
            this.txtTuningRotaryD.Size = new System.Drawing.Size(72, 22);
            this.txtTuningRotaryD.TabIndex = 516;
            this.txtTuningRotaryD.Text = "100";
            this.txtTuningRotaryD.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtTuningRotaryD_KeyUp);
            this.txtTuningRotaryD.Leave += new System.EventHandler(this.txtTuningRotaryD_Leave);
            // 
            // lblTuningRotaryP
            // 
            this.lblTuningRotaryP.AutoSize = true;
            this.lblTuningRotaryP.Location = new System.Drawing.Point(29, 48);
            this.lblTuningRotaryP.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblTuningRotaryP.Name = "lblTuningRotaryP";
            this.lblTuningRotaryP.Size = new System.Drawing.Size(17, 17);
            this.lblTuningRotaryP.TabIndex = 2;
            this.lblTuningRotaryP.Text = "P";
            // 
            // lblTuningRotaryDUnits
            // 
            this.lblTuningRotaryDUnits.AutoSize = true;
            this.lblTuningRotaryDUnits.Location = new System.Drawing.Point(132, 107);
            this.lblTuningRotaryDUnits.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblTuningRotaryDUnits.Name = "lblTuningRotaryDUnits";
            this.lblTuningRotaryDUnits.Size = new System.Drawing.Size(20, 17);
            this.lblTuningRotaryDUnits.TabIndex = 2;
            this.lblTuningRotaryDUnits.Text = "%";
            // 
            // lblTuningRotaryI
            // 
            this.lblTuningRotaryI.AutoSize = true;
            this.lblTuningRotaryI.Location = new System.Drawing.Point(29, 78);
            this.lblTuningRotaryI.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblTuningRotaryI.Name = "lblTuningRotaryI";
            this.lblTuningRotaryI.Size = new System.Drawing.Size(11, 17);
            this.lblTuningRotaryI.TabIndex = 2;
            this.lblTuningRotaryI.Text = "I";
            // 
            // lblTuningRotaryD
            // 
            this.lblTuningRotaryD.AutoSize = true;
            this.lblTuningRotaryD.Location = new System.Drawing.Point(29, 107);
            this.lblTuningRotaryD.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblTuningRotaryD.Name = "lblTuningRotaryD";
            this.lblTuningRotaryD.Size = new System.Drawing.Size(18, 17);
            this.lblTuningRotaryD.TabIndex = 2;
            this.lblTuningRotaryD.Text = "D";
            // 
            // lblTuningRotaryIUnits
            // 
            this.lblTuningRotaryIUnits.AutoSize = true;
            this.lblTuningRotaryIUnits.Location = new System.Drawing.Point(132, 78);
            this.lblTuningRotaryIUnits.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblTuningRotaryIUnits.Name = "lblTuningRotaryIUnits";
            this.lblTuningRotaryIUnits.Size = new System.Drawing.Size(20, 17);
            this.lblTuningRotaryIUnits.TabIndex = 2;
            this.lblTuningRotaryIUnits.Text = "%";
            // 
            // lblTuningRotaryPUnits
            // 
            this.lblTuningRotaryPUnits.AutoSize = true;
            this.lblTuningRotaryPUnits.Location = new System.Drawing.Point(132, 48);
            this.lblTuningRotaryPUnits.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblTuningRotaryPUnits.Name = "lblTuningRotaryPUnits";
            this.lblTuningRotaryPUnits.Size = new System.Drawing.Size(20, 17);
            this.lblTuningRotaryPUnits.TabIndex = 2;
            this.lblTuningRotaryPUnits.Text = "%";
            // 
            // gbxTuningLinear
            // 
            this.gbxTuningLinear.Controls.Add(this.txtTuningLinearP);
            this.gbxTuningLinear.Controls.Add(this.txtTuningLinearD);
            this.gbxTuningLinear.Controls.Add(this.lblTuningLinearP);
            this.gbxTuningLinear.Controls.Add(this.txtTuningLinearI);
            this.gbxTuningLinear.Controls.Add(this.lblTuningLinearI);
            this.gbxTuningLinear.Controls.Add(this.lblTuningLinearD);
            this.gbxTuningLinear.Controls.Add(this.lblTuningLinearDUnits);
            this.gbxTuningLinear.Controls.Add(this.lblTuningLinearPUnits);
            this.gbxTuningLinear.Controls.Add(this.lblTuningLinearIUnits);
            this.gbxTuningLinear.Location = new System.Drawing.Point(35, 47);
            this.gbxTuningLinear.Margin = new System.Windows.Forms.Padding(4);
            this.gbxTuningLinear.Name = "gbxTuningLinear";
            this.gbxTuningLinear.Padding = new System.Windows.Forms.Padding(4);
            this.gbxTuningLinear.Size = new System.Drawing.Size(173, 167);
            this.gbxTuningLinear.TabIndex = 530;
            this.gbxTuningLinear.TabStop = false;
            this.gbxTuningLinear.Text = " Tuning linear ";
            // 
            // txtTuningLinearP
            // 
            this.txtTuningLinearP.Location = new System.Drawing.Point(48, 38);
            this.txtTuningLinearP.Margin = new System.Windows.Forms.Padding(4);
            this.txtTuningLinearP.Name = "txtTuningLinearP";
            this.txtTuningLinearP.Size = new System.Drawing.Size(72, 22);
            this.txtTuningLinearP.TabIndex = 511;
            this.txtTuningLinearP.Text = "100";
            this.txtTuningLinearP.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtTuningLinearP_KeyUp);
            this.txtTuningLinearP.Leave += new System.EventHandler(this.txtTuningLinearP_Leave);
            // 
            // txtTuningLinearD
            // 
            this.txtTuningLinearD.Location = new System.Drawing.Point(48, 97);
            this.txtTuningLinearD.Margin = new System.Windows.Forms.Padding(4);
            this.txtTuningLinearD.Name = "txtTuningLinearD";
            this.txtTuningLinearD.Size = new System.Drawing.Size(72, 22);
            this.txtTuningLinearD.TabIndex = 513;
            this.txtTuningLinearD.Text = "100";
            this.txtTuningLinearD.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtTuningLinearD_KeyUp);
            this.txtTuningLinearD.Leave += new System.EventHandler(this.txtTuningLinearD_Leave);
            // 
            // lblTuningLinearP
            // 
            this.lblTuningLinearP.AutoSize = true;
            this.lblTuningLinearP.Location = new System.Drawing.Point(24, 41);
            this.lblTuningLinearP.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblTuningLinearP.Name = "lblTuningLinearP";
            this.lblTuningLinearP.Size = new System.Drawing.Size(17, 17);
            this.lblTuningLinearP.TabIndex = 2;
            this.lblTuningLinearP.Text = "P";
            // 
            // txtTuningLinearI
            // 
            this.txtTuningLinearI.Location = new System.Drawing.Point(48, 68);
            this.txtTuningLinearI.Margin = new System.Windows.Forms.Padding(4);
            this.txtTuningLinearI.Name = "txtTuningLinearI";
            this.txtTuningLinearI.Size = new System.Drawing.Size(72, 22);
            this.txtTuningLinearI.TabIndex = 512;
            this.txtTuningLinearI.Text = "100";
            this.txtTuningLinearI.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtTuningLinearI_KeyUp);
            this.txtTuningLinearI.Leave += new System.EventHandler(this.txtTuningLinearI_Leave);
            // 
            // lblTuningLinearI
            // 
            this.lblTuningLinearI.AutoSize = true;
            this.lblTuningLinearI.Location = new System.Drawing.Point(24, 70);
            this.lblTuningLinearI.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblTuningLinearI.Name = "lblTuningLinearI";
            this.lblTuningLinearI.Size = new System.Drawing.Size(11, 17);
            this.lblTuningLinearI.TabIndex = 2;
            this.lblTuningLinearI.Text = "I";
            // 
            // lblTuningLinearD
            // 
            this.lblTuningLinearD.AutoSize = true;
            this.lblTuningLinearD.Location = new System.Drawing.Point(24, 100);
            this.lblTuningLinearD.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblTuningLinearD.Name = "lblTuningLinearD";
            this.lblTuningLinearD.Size = new System.Drawing.Size(18, 17);
            this.lblTuningLinearD.TabIndex = 2;
            this.lblTuningLinearD.Text = "D";
            // 
            // lblTuningLinearDUnits
            // 
            this.lblTuningLinearDUnits.AutoSize = true;
            this.lblTuningLinearDUnits.Location = new System.Drawing.Point(127, 100);
            this.lblTuningLinearDUnits.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblTuningLinearDUnits.Name = "lblTuningLinearDUnits";
            this.lblTuningLinearDUnits.Size = new System.Drawing.Size(20, 17);
            this.lblTuningLinearDUnits.TabIndex = 2;
            this.lblTuningLinearDUnits.Text = "%";
            // 
            // lblTuningLinearPUnits
            // 
            this.lblTuningLinearPUnits.AutoSize = true;
            this.lblTuningLinearPUnits.Location = new System.Drawing.Point(127, 41);
            this.lblTuningLinearPUnits.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblTuningLinearPUnits.Name = "lblTuningLinearPUnits";
            this.lblTuningLinearPUnits.Size = new System.Drawing.Size(20, 17);
            this.lblTuningLinearPUnits.TabIndex = 2;
            this.lblTuningLinearPUnits.Text = "%";
            // 
            // lblTuningLinearIUnits
            // 
            this.lblTuningLinearIUnits.AutoSize = true;
            this.lblTuningLinearIUnits.Location = new System.Drawing.Point(127, 70);
            this.lblTuningLinearIUnits.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblTuningLinearIUnits.Name = "lblTuningLinearIUnits";
            this.lblTuningLinearIUnits.Size = new System.Drawing.Size(20, 17);
            this.lblTuningLinearIUnits.TabIndex = 2;
            this.lblTuningLinearIUnits.Text = "%";
            // 
            // gbxTeachResult
            // 
            this.gbxTeachResult.Controls.Add(this.btnUndo);
            this.gbxTeachResult.Controls.Add(this.txtTeachedHeight);
            this.gbxTeachResult.Controls.Add(this.txtTeachedSqLinearHome);
            this.gbxTeachResult.Controls.Add(this.txtTeachedSqLinear);
            this.gbxTeachResult.Controls.Add(this.lblTeachedHeight);
            this.gbxTeachResult.Controls.Add(this.lblTeachedSqLinearHome);
            this.gbxTeachResult.Controls.Add(this.lblTeachedSqLinear);
            this.gbxTeachResult.Controls.Add(this.lblTeachedHeightUnits);
            this.gbxTeachResult.Location = new System.Drawing.Point(335, 254);
            this.gbxTeachResult.Margin = new System.Windows.Forms.Padding(4);
            this.gbxTeachResult.Name = "gbxTeachResult";
            this.gbxTeachResult.Padding = new System.Windows.Forms.Padding(4);
            this.gbxTeachResult.Size = new System.Drawing.Size(348, 156);
            this.gbxTeachResult.TabIndex = 529;
            this.gbxTeachResult.TabStop = false;
            this.gbxTeachResult.Text = " Teach result";
            // 
            // btnUndo
            // 
            this.btnUndo.Location = new System.Drawing.Point(224, 59);
            this.btnUndo.Margin = new System.Windows.Forms.Padding(4);
            this.btnUndo.Name = "btnUndo";
            this.btnUndo.Size = new System.Drawing.Size(65, 28);
            this.btnUndo.TabIndex = 521;
            this.btnUndo.Text = "Undo";
            this.btnUndo.UseVisualStyleBackColor = true;
            this.btnUndo.Visible = false;
            this.btnUndo.Click += new System.EventHandler(this.btnUndo_Click);
            // 
            // txtTeachedHeight
            // 
            this.txtTeachedHeight.Location = new System.Drawing.Point(128, 102);
            this.txtTeachedHeight.Margin = new System.Windows.Forms.Padding(4);
            this.txtTeachedHeight.Name = "txtTeachedHeight";
            this.txtTeachedHeight.ReadOnly = true;
            this.txtTeachedHeight.Size = new System.Drawing.Size(67, 22);
            this.txtTeachedHeight.TabIndex = 520;
            // 
            // txtTeachedSqLinearHome
            // 
            this.txtTeachedSqLinearHome.Location = new System.Drawing.Point(128, 20);
            this.txtTeachedSqLinearHome.Margin = new System.Windows.Forms.Padding(4);
            this.txtTeachedSqLinearHome.Name = "txtTeachedSqLinearHome";
            this.txtTeachedSqLinearHome.ReadOnly = true;
            this.txtTeachedSqLinearHome.Size = new System.Drawing.Size(67, 22);
            this.txtTeachedSqLinearHome.TabIndex = 520;
            // 
            // txtTeachedSqLinear
            // 
            this.txtTeachedSqLinear.Location = new System.Drawing.Point(128, 60);
            this.txtTeachedSqLinear.Margin = new System.Windows.Forms.Padding(4);
            this.txtTeachedSqLinear.Name = "txtTeachedSqLinear";
            this.txtTeachedSqLinear.ReadOnly = true;
            this.txtTeachedSqLinear.Size = new System.Drawing.Size(67, 22);
            this.txtTeachedSqLinear.TabIndex = 520;
            // 
            // lblTeachedHeight
            // 
            this.lblTeachedHeight.AutoSize = true;
            this.lblTeachedHeight.Location = new System.Drawing.Point(15, 106);
            this.lblTeachedHeight.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblTeachedHeight.Name = "lblTeachedHeight";
            this.lblTeachedHeight.Size = new System.Drawing.Size(49, 17);
            this.lblTeachedHeight.TabIndex = 519;
            this.lblTeachedHeight.Text = "Height";
            // 
            // lblTeachedSqLinearHome
            // 
            this.lblTeachedSqLinearHome.AutoSize = true;
            this.lblTeachedSqLinearHome.Location = new System.Drawing.Point(15, 25);
            this.lblTeachedSqLinearHome.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblTeachedSqLinearHome.Name = "lblTeachedSqLinearHome";
            this.lblTeachedSqLinearHome.Size = new System.Drawing.Size(89, 17);
            this.lblTeachedSqLinearHome.TabIndex = 519;
            this.lblTeachedSqLinearHome.Text = "SQ retracted";
            // 
            // lblTeachedSqLinear
            // 
            this.lblTeachedSqLinear.AutoSize = true;
            this.lblTeachedSqLinear.Location = new System.Drawing.Point(15, 65);
            this.lblTeachedSqLinear.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblTeachedSqLinear.Name = "lblTeachedSqLinear";
            this.lblTeachedSqLinear.Size = new System.Drawing.Size(110, 17);
            this.lblTeachedSqLinear.TabIndex = 519;
            this.lblTeachedSqLinear.Text = "SQ close to part";
            // 
            // lblTeachedHeightUnits
            // 
            this.lblTeachedHeightUnits.AutoSize = true;
            this.lblTeachedHeightUnits.Location = new System.Drawing.Point(209, 108);
            this.lblTeachedHeightUnits.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblTeachedHeightUnits.Name = "lblTeachedHeightUnits";
            this.lblTeachedHeightUnits.Size = new System.Drawing.Size(34, 17);
            this.lblTeachedHeightUnits.TabIndex = 2;
            this.lblTeachedHeightUnits.Text = "mm.";
            // 
            // lblRotarySensitivity
            // 
            this.lblRotarySensitivity.AutoSize = true;
            this.lblRotarySensitivity.Location = new System.Drawing.Point(360, 99);
            this.lblRotarySensitivity.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblRotarySensitivity.Name = "lblRotarySensitivity";
            this.lblRotarySensitivity.Size = new System.Drawing.Size(115, 17);
            this.lblRotarySensitivity.TabIndex = 527;
            this.lblRotarySensitivity.Text = "Rotary sensitivity";
            // 
            // lblSpeed
            // 
            this.lblSpeed.AutoSize = true;
            this.lblSpeed.Location = new System.Drawing.Point(360, 64);
            this.lblSpeed.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblSpeed.Name = "lblSpeed";
            this.lblSpeed.Size = new System.Drawing.Size(49, 17);
            this.lblSpeed.TabIndex = 512;
            this.lblSpeed.Text = "Speed";
            // 
            // txtRotarySensitivity
            // 
            this.txtRotarySensitivity.Location = new System.Drawing.Point(527, 93);
            this.txtRotarySensitivity.Margin = new System.Windows.Forms.Padding(4);
            this.txtRotarySensitivity.Name = "txtRotarySensitivity";
            this.txtRotarySensitivity.Size = new System.Drawing.Size(72, 22);
            this.txtRotarySensitivity.TabIndex = 518;
            this.txtRotarySensitivity.Text = "100";
            this.txtRotarySensitivity.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtRotarySensitivity_KeyUp);
            this.txtRotarySensitivity.Leave += new System.EventHandler(this.txtRotarySensitivity_Leave);
            // 
            // txtSpeed
            // 
            this.txtSpeed.Location = new System.Drawing.Point(527, 59);
            this.txtSpeed.Margin = new System.Windows.Forms.Padding(4);
            this.txtSpeed.Name = "txtSpeed";
            this.txtSpeed.Size = new System.Drawing.Size(72, 22);
            this.txtSpeed.TabIndex = 517;
            this.txtSpeed.Text = "50";
            this.txtSpeed.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtSpeed_KeyUp);
            this.txtSpeed.Leave += new System.EventHandler(this.txtSpeed_Leave);
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(605, 63);
            this.label40.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(20, 17);
            this.label40.TabIndex = 2;
            this.label40.Text = "%";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(607, 98);
            this.label20.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(50, 17);
            this.label20.TabIndex = 2;
            this.label20.Text = "counts";
            // 
            // txtMaxTorque
            // 
            this.txtMaxTorque.Location = new System.Drawing.Point(527, 127);
            this.txtMaxTorque.Margin = new System.Windows.Forms.Padding(4);
            this.txtMaxTorque.Name = "txtMaxTorque";
            this.txtMaxTorque.Size = new System.Drawing.Size(72, 22);
            this.txtMaxTorque.TabIndex = 510;
            this.txtMaxTorque.Text = "100";
            this.txtMaxTorque.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtMaxTorque_KeyUp);
            this.txtMaxTorque.Leave += new System.EventHandler(this.txtMaxTorque_Leave);
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(608, 128);
            this.label25.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(20, 17);
            this.label25.TabIndex = 2;
            this.label25.Text = "%";
            // 
            // lblMaxTorque
            // 
            this.lblMaxTorque.AutoSize = true;
            this.lblMaxTorque.Location = new System.Drawing.Point(360, 132);
            this.lblMaxTorque.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblMaxTorque.Name = "lblMaxTorque";
            this.lblMaxTorque.Size = new System.Drawing.Size(123, 17);
            this.lblMaxTorque.TabIndex = 2;
            this.lblMaxTorque.Text = "Permisable torque";
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(1253, 437);
            this.button2.Margin = new System.Windows.Forms.Padding(4);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(7, 6);
            this.button2.TabIndex = 518;
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // btnRun
            // 
            this.btnRun.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRun.Location = new System.Drawing.Point(1132, 697);
            this.btnRun.Margin = new System.Windows.Forms.Padding(4);
            this.btnRun.Name = "btnRun";
            this.btnRun.Size = new System.Drawing.Size(163, 43);
            this.btnRun.TabIndex = 120;
            this.btnRun.Text = "INITIALIZE";
            this.btnRun.UseVisualStyleBackColor = true;
            this.btnRun.Click += new System.EventHandler(this.btnRun_Click);
            // 
            // btnSaveProgramToFile
            // 
            this.btnSaveProgramToFile.Location = new System.Drawing.Point(1132, 223);
            this.btnSaveProgramToFile.Margin = new System.Windows.Forms.Padding(4);
            this.btnSaveProgramToFile.Name = "btnSaveProgramToFile";
            this.btnSaveProgramToFile.Size = new System.Drawing.Size(157, 43);
            this.btnSaveProgramToFile.TabIndex = 120;
            this.btnSaveProgramToFile.Text = "Save program to file";
            this.btnSaveProgramToFile.UseVisualStyleBackColor = true;
            this.btnSaveProgramToFile.Click += new System.EventHandler(this.btnSaveProgramToFile_Click);
            // 
            // btnSingleCycle
            // 
            this.btnSingleCycle.Location = new System.Drawing.Point(1132, 325);
            this.btnSingleCycle.Margin = new System.Windows.Forms.Padding(4);
            this.btnSingleCycle.Name = "btnSingleCycle";
            this.btnSingleCycle.Size = new System.Drawing.Size(157, 43);
            this.btnSingleCycle.TabIndex = 120;
            this.btnSingleCycle.Text = "Single cycle";
            this.btnSingleCycle.UseVisualStyleBackColor = true;
            this.btnSingleCycle.Click += new System.EventHandler(this.btnSingleCycle_Click);
            // 
            // btnSaveToController
            // 
            this.btnSaveToController.Location = new System.Drawing.Point(1132, 275);
            this.btnSaveToController.Margin = new System.Windows.Forms.Padding(4);
            this.btnSaveToController.Name = "btnSaveToController";
            this.btnSaveToController.Size = new System.Drawing.Size(157, 43);
            this.btnSaveToController.TabIndex = 120;
            this.btnSaveToController.Text = "Save program to controller";
            this.btnSaveToController.UseVisualStyleBackColor = true;
            this.btnSaveToController.Click += new System.EventHandler(this.btnSaveInController_Click);
            // 
            // btnSaveSettingsToFile
            // 
            this.btnSaveSettingsToFile.Location = new System.Drawing.Point(1132, 172);
            this.btnSaveSettingsToFile.Margin = new System.Windows.Forms.Padding(4);
            this.btnSaveSettingsToFile.Name = "btnSaveSettingsToFile";
            this.btnSaveSettingsToFile.Size = new System.Drawing.Size(157, 43);
            this.btnSaveSettingsToFile.TabIndex = 110;
            this.btnSaveSettingsToFile.Text = "Save settings to file";
            this.btnSaveSettingsToFile.UseVisualStyleBackColor = true;
            this.btnSaveSettingsToFile.Click += new System.EventHandler(this.btnSaveSettingsToFile_Click);
            // 
            // btnStop
            // 
            this.btnStop.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnStop.Location = new System.Drawing.Point(1135, 761);
            this.btnStop.Margin = new System.Windows.Forms.Padding(4);
            this.btnStop.Name = "btnStop";
            this.btnStop.Size = new System.Drawing.Size(160, 74);
            this.btnStop.TabIndex = 116;
            this.btnStop.Text = "STOP";
            this.btnStop.UseVisualStyleBackColor = true;
            this.btnStop.Click += new System.EventHandler(this.btnStop_Click);
            // 
            // btnContactInfo
            // 
            this.btnContactInfo.Location = new System.Drawing.Point(1135, 567);
            this.btnContactInfo.Margin = new System.Windows.Forms.Padding(4);
            this.btnContactInfo.Name = "btnContactInfo";
            this.btnContactInfo.Size = new System.Drawing.Size(160, 33);
            this.btnContactInfo.TabIndex = 117;
            this.btnContactInfo.Text = "Contact information";
            this.btnContactInfo.UseVisualStyleBackColor = true;
            this.btnContactInfo.Click += new System.EventHandler(this.btnContactInfo_Click);
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(1121, 487);
            this.pictureBox2.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(181, 73);
            this.pictureBox2.TabIndex = 118;
            this.pictureBox2.TabStop = false;
            // 
            // btnHelpDocument
            // 
            this.btnHelpDocument.Location = new System.Drawing.Point(1135, 619);
            this.btnHelpDocument.Margin = new System.Windows.Forms.Padding(4);
            this.btnHelpDocument.Name = "btnHelpDocument";
            this.btnHelpDocument.Size = new System.Drawing.Size(160, 33);
            this.btnHelpDocument.TabIndex = 117;
            this.btnHelpDocument.Text = "Help document";
            this.btnHelpDocument.UseVisualStyleBackColor = true;
            this.btnHelpDocument.Click += new System.EventHandler(this.button4_Click);
            // 
            // chkShowAll
            // 
            this.chkShowAll.AutoSize = true;
            this.chkShowAll.Location = new System.Drawing.Point(1011, 490);
            this.chkShowAll.Margin = new System.Windows.Forms.Padding(4);
            this.chkShowAll.Name = "chkShowAll";
            this.chkShowAll.Size = new System.Drawing.Size(82, 21);
            this.chkShowAll.TabIndex = 120;
            this.chkShowAll.Text = "Show all";
            this.chkShowAll.UseVisualStyleBackColor = true;
            this.chkShowAll.CheckedChanged += new System.EventHandler(this.chkShowAll_CheckedChanged);
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(939, 647);
            this.textBox2.Margin = new System.Windows.Forms.Padding(4);
            this.textBox2.Multiline = true;
            this.textBox2.Name = "textBox2";
            this.textBox2.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.textBox2.Size = new System.Drawing.Size(203, 69);
            this.textBox2.TabIndex = 121;
            // 
            // lblStatusTxt
            // 
            this.lblStatusTxt.AutoSize = true;
            this.lblStatusTxt.Location = new System.Drawing.Point(1133, 94);
            this.lblStatusTxt.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblStatusTxt.Name = "lblStatusTxt";
            this.lblStatusTxt.Size = new System.Drawing.Size(48, 17);
            this.lblStatusTxt.TabIndex = 519;
            this.lblStatusTxt.Text = "Status";
            // 
            // lblStatus
            // 
            this.lblStatus.Location = new System.Drawing.Point(1185, 92);
            this.lblStatus.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(104, 18);
            this.lblStatus.TabIndex = 519;
            this.lblStatus.Text = "Status";
            this.lblStatus.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::SMAC_Capping_Control_Center.Properties.Resources.Capping;
            this.pictureBox1.Location = new System.Drawing.Point(1212, 381);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(96, 100);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 520;
            this.pictureBox1.TabStop = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(1132, 437);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(69, 25);
            this.label3.TabIndex = 521;
            this.label3.Text = "V0.18_TEMP";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1312, 864);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.lblStatus);
            this.Controls.Add(this.lblStatusTxt);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.chkShowAll);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.chart1);
            this.Controls.Add(this.btnHelpDocument);
            this.Controls.Add(this.btnContactInfo);
            this.Controls.Add(this.btnStop);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.btnRun);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.btnSaveProgramToFile);
            this.Controls.Add(this.txtTerminal);
            this.Controls.Add(this.btnSingleCycle);
            this.Controls.Add(this.btnSaveToController);
            this.Controls.Add(this.btnLoadSettingsFromFile);
            this.Controls.Add(this.btnSaveSettingsToFile);
            this.Controls.Add(this.lblPort);
            this.Controls.Add(this.cmbPort);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "SMAC Capping Control Center";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.Load += new System.EventHandler(this.MainForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.Run.ResumeLayout(false);
            this.Run.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxRun)).EndInit();
            this.Setup.ResumeLayout(false);
            this.Setup.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxSetup)).EndInit();
            this.Tuning.ResumeLayout(false);
            this.Tuning.PerformLayout();
            this.gbxTuningRotary.ResumeLayout(false);
            this.gbxTuningRotary.PerformLayout();
            this.gbxTuningLinear.ResumeLayout(false);
            this.gbxTuningLinear.PerformLayout();
            this.gbxTeachResult.ResumeLayout(false);
            this.gbxTeachResult.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblPositionCloseToPart;
        public System.Windows.Forms.TextBox txtPositionCloseToPart;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label lblThreadDepth;
        public System.Windows.Forms.TextBox txtThreadDepth;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label lblActuator;
        private System.Windows.Forms.Label lblPort;
        public System.Windows.Forms.TextBox txtTerminal;
        private System.Windows.Forms.Button btnLoadSettingsFromFile;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart1;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Button btnActuatorInfo;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage Run;
        private System.Windows.Forms.TabPage Setup;
        private System.Windows.Forms.PictureBox pictureBoxSetup;
        private System.Windows.Forms.Label lblThreadDirection;
        public System.Windows.Forms.TextBox txtPitch;
        private System.Windows.Forms.Label lblPitch;
        private System.Windows.Forms.Label lblLandedHeightMin;
        public System.Windows.Forms.TextBox txtLandedHeightMax;
        public System.Windows.Forms.TextBox txtLandedHeightMin;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label lblMaxTorque;
        public System.Windows.Forms.TextBox txtMaxTorque;
        private System.Windows.Forms.Button btnSaveToController;
        private System.Windows.Forms.Button btnStop;
        private System.Windows.Forms.Button btnContactInfo;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Button btnHelpDocument;
        private System.Windows.Forms.Button btnRun;
        private System.Windows.Forms.Button btnSaveProgramToFile;
        private System.Windows.Forms.Button btnSingleCycle;
        private System.Windows.Forms.Button btnTeach;
        private System.Windows.Forms.Button btnSaveSettingsToFile;
        private System.Windows.Forms.Label lblSpeed;
        public System.Windows.Forms.TextBox txtTuningRotaryD;
        public System.Windows.Forms.TextBox txtTuningRotaryI;
        public System.Windows.Forms.TextBox txtTuningLinearD;
        public System.Windows.Forms.TextBox txtTuningRotaryP;
        public System.Windows.Forms.TextBox txtTuningLinearI;
        public System.Windows.Forms.TextBox txtTuningLinearP;
        public System.Windows.Forms.TextBox txtSpeed;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label lblTuningRotaryDUnits;
        private System.Windows.Forms.Label lblTuningRotaryIUnits;
        private System.Windows.Forms.Label lblTuningLinearDUnits;
        private System.Windows.Forms.Label lblTuningRotaryPUnits;
        private System.Windows.Forms.Label lblTuningLinearIUnits;
        private System.Windows.Forms.Label lblTuningRotaryD;
        private System.Windows.Forms.Label lblTuningLinearPUnits;
        private System.Windows.Forms.Label lblTuningRotaryI;
        private System.Windows.Forms.Label lblTuningLinearD;
        private System.Windows.Forms.Label lblTuningRotaryP;
        private System.Windows.Forms.Label lblTuningLinearI;
        private System.Windows.Forms.Label lblTuningLinearP;
        public System.Windows.Forms.ComboBox cmbActuator;
        public System.Windows.Forms.ComboBox cmbThreadDirection;
        public System.Windows.Forms.CheckBox chkRotateReverse;
        public System.Windows.Forms.CheckBox chkCheckLandedHeight;
        public System.Windows.Forms.ComboBox cmbPort;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.CheckBox chkShowAll;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.PictureBox pictureBoxRun;
        private System.Windows.Forms.Label lblLandingHeight;
        private System.Windows.Forms.Label lblThreadDepthMeasured;
        private System.Windows.Forms.Button btnUndo;
        private System.Windows.Forms.Label lblTeachedSqLinear;
        public System.Windows.Forms.ComboBox cmbPitchUnits;
        public System.Windows.Forms.TextBox txtTeachedSqLinear;
        private System.Windows.Forms.Label lblThreadDepthMeasured2;
        private System.Windows.Forms.Label lblLandingHeight2;
        public System.Windows.Forms.TextBox txtTeachedHeight;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label lblTeachedHeight;
        private System.Windows.Forms.Label lblTeachedHeightUnits;
        private System.Windows.Forms.Label lblRotarySensitivity;
        public System.Windows.Forms.TextBox txtRotarySensitivity;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.GroupBox gbxTeachResult;
        public System.Windows.Forms.TextBox txtTeachedSqLinearHome;
        private System.Windows.Forms.Label lblTeachedSqLinearHome;
        private System.Windows.Forms.Label lblLandedHeightMax;
        private System.Windows.Forms.TabPage Tuning;
        private System.Windows.Forms.GroupBox gbxTuningRotary;
        private System.Windows.Forms.GroupBox gbxTuningLinear;
        private System.Windows.Forms.Label lblStatusTxt;
        public System.Windows.Forms.Label lblStatus;
        private System.Windows.Forms.Label lblPluginfoRun;
        private System.Windows.Forms.Label lblRotaryDepthRun;
        private System.Windows.Forms.Label lblThreadStartRun;
        private System.Windows.Forms.Label lblHomePositionRun;
        private System.Windows.Forms.Label lblPluginfoSetup;
        private System.Windows.Forms.Label lblHomePositionSetup;
        private System.Windows.Forms.Label lblThreadStartSetup;
        private System.Windows.Forms.Label lblRotDepthSetup;
        private System.Windows.Forms.Label lblCloseToPart;
        private System.Windows.Forms.Label lblMinLinTravel;
        private System.Windows.Forms.Label lblMaxLinTravel;
        public System.Windows.Forms.TextBox txtMinLinTravel;
        public System.Windows.Forms.TextBox txtMaxLinTravel;
        public System.Windows.Forms.CheckBox chkLinDistTravel;
        private System.Windows.Forms.Label lblMinRotDistTravel;
        private System.Windows.Forms.Label lblMaxRotDistTravel;
        public System.Windows.Forms.TextBox txtMinRotDistTravel;
        public System.Windows.Forms.TextBox txtMaxRotDistTravel;
        public System.Windows.Forms.CheckBox chkRotDistTravel;
        private System.Windows.Forms.Label lblLinDepthRun;
        private System.Windows.Forms.Label lblLinDepthSetup;
        public System.Windows.Forms.ComboBox cmbCappingType;
        private System.Windows.Forms.Label lblCappingType;
        private System.Windows.Forms.Button btnTeachPickCap;
        public System.Windows.Forms.TextBox txtPositionCloseToPickCap;
        private System.Windows.Forms.Label lblPositionCloseToPickCap;
        public System.Windows.Forms.TextBox txtRetractDistance;
        private System.Windows.Forms.Label lblRetractDistance;
        public System.Windows.Forms.TextBox txtNumofThreads;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        public System.Windows.Forms.TextBox txtSpeedtoPickCap;
        private System.Windows.Forms.Label lblSpeedToPickCap;
        private System.Windows.Forms.Label lblLinearDistance_Run;
        private System.Windows.Forms.Label lblLinearDistance_Setup;
        private System.Windows.Forms.Label lblRotaryRevs_Run;
        private System.Windows.Forms.Label lblRotaryRevs_Setup;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label3;
    }
}

