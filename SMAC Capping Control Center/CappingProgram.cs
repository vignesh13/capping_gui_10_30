﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.Windows.Forms;

namespace SMAC_Capping_Control_Center
{
    class CappingProgram
    {
        private List<string> theProgram = new List<string>();
        public Variables variables = null;

        private List<string> translateFrom = new List<string>();
        private List<string> translateInto = new List<string>();

        private string[] masterProgram = new string[]
        {
            ";",
            ";   I/O ALLOCATION",
            "; ==================================",
            "; OUTPUT 0: READY/RETRACTED/HOME",
            "; OUTPUT 1: PASS",
            "; OUTPUT 2: FAIL",
            "; OUTPUT 3: ERROR/ JAM",
            "; Expansion I/O Module (MIOE 8-8) for additional I/O handshaking",
            "; OUTPUT 8: SOFT LANDED ON CAP(For Both Capping Types)",
            "; COMMENT_OUTPUT_9",
            "; OUTPUT 10: CAP SOFT LANDED ON CONTAINER(For Both Capping Types)(?Release vacuum/ air-friction)",
            "; OUTPUT 11: CAP IS INSTALLED/ RELEASE VACUUM/GRIPPER/CHUCK",
            "; OUTPUT 12: CAP NOT VERIFIED",
            ";",
            "; INPUT 0: START CYCLE",
            "; INPUT 1: MOVE TO CLEAR POS.",
            "; INPUT 2: INSTALL CAP",
            "; INPUT 3: RESET (In Case of Jam Condition)",
            "; Expansion I/O Module(MIOE 8-8) for additional I/O handshaking",
            "; INPUT 8: RETRACT TO HOME(Gripeer/Vacuum is released)",
            ";",
            ";",
            "; Register usage:",
            "; Possible registers(park/home offset position, 2nd park position-retract after pickup)",
            ";",
            "; 10: Position Close to Pick Cap / Park Position ",
            "; 11: Retracted Verification Limit For Park Position (Park Pos. +2mm)",
            "; 12:",
            "; 15: Landed Height with cap on container",
            "; 16: (RESEREVED FOR DECAPPING/ FUTURE USE)",
            "; 17: (RESEREVED FOR DECAPPING/ FUTURE USE)",
            "; 18: (RESEREVED FOR DECAPPING/ FUTURE USE)",
            "; 19: Initial soft landed height with CAP ON CONTAINER",
            "; 20: Min position of linear movement during reverse thread find (locating thread lead in- working register during find)",
            "; 21: Initial Linear datum / landed point. This is the INITIAL position of the softland",
            "; 22: (RESEREVED FOR DECAPPING/ FUTURE USE)",
            "; 23: (RESEREVED FOR DECAPPING/ FUTURE USE)",
            "; 24: (RESEREVED FOR DECAPPING/ FUTURE USE)",
            "; 25: Linear Height positon when first thread found",
            "; 26: (RESEREVED FOR DECAPPING/ FUTURE USE)",
            "; 27: (RESEREVED FOR DECAPPING/ FUTURE USE)",
            "; 28: Fail output (counter of failures during the process)",
            "; 29: (RESEREVED FOR DECAPPING/ FUTURE USE)",
            "; 30: General purpose working register",
            "; 31: Linear position of required depth",
            "; 32: Time to do next rotate out of thread check",
            "; 33: Rotary position of last out of thread check",
            "; 34: Timer used for reporting following errors",
            ";",
            "; 35: Final linear in position in encoder counts for depth ",
            "; 36: Linear Depth (!LD) in distance (calulated, final - datum pos.) ",
            "; 40: Rotary Depth (!RD) achieved",
            ";",
            "0MF",
            "EN",            
            "; **CLEAR SCREEN, CONFIGURE I/O CHANNELS******",
            "MD0,BR57600,MG\"!I0\"",
            "MD1,CH0,CH1,CH2,CH3,CH8,CH9,CH10,CH11,CF0,CF1,CF2,CF3,CF8,CF9,CF10,CF11,UM1,SS2",
            ";",
            "; **LOAD PID'S FOR AXIS 1 (Linear) & AXIS 2 (Rotary)",
            "MD2,0MF,PH0,1PM,1SG[P_LINEAR],SI[I_LINEAR],SD[D_LINEAR],IL[IL_LINEAR],FR[FR_LINEAR],RI[RI_LINEAR],OO[OO_LINEAR]",
            "MD3,2PM,SG[P_ROTARY],SI[I_ROTARY],SD[D_ROTARY],IL[IL_ROTARY],FR[FR_ROTARY],RI[RI_ROTARY],OO[OO_ROTARY]",
            ";",
            "; ****OPTIONAL Setting up Interrupt (Input 3)********",
            "; **  This Disables the Linear and Rotary axis when input 3 is activated.",
            "MD4,AL9,LV3,EV3",
            ";",
            "; Set Servo phasing for rotary to 0 for right handed thread and to 3 for left handed",
            ";",
            "MD5,2PH[PHASING_ROTARY]",
            ";",
            "; Do homing for the linear and then continue",
            ";",
            "MD6,MC10,MJ15",
            ";",
            "; Endless loop that can only be exited by te interrupt routine",
            "; The interrupt routine is entered by pressing the reset button",
            ";",
            "MD8,NO,RP",
            ";",
            "; The interrupt routine",
            ";",
            "MD9,0MF,DV3,WF3,UM1,EV3,MJ0",
            ";",
            "; Homing routine for linear axis",
            ";",
            "MD10,1SV[VF_HOMING_LINEAR],SA[AF_HOMING_LINEAR],SQ[QF_HOMING_LINEAR],VM,MN,DI1,GO,WA250",
            "MD11,RW538,IB[HARDSTOP_THRESHOLD_LINEAR],DI0,JR2,RP,1FI,MJ12",
            "MD12,RL448,IC10,MJ13,NO,RW538,IG[INDEX_LIMIT_LINEAR],MJ14,NO,RP",
            "MD13,AB,WA5,PM,GH,MG\"LINEAR HOME/RETRACTED\",RC",
            "MD14,MG\"14-NO INDEX,\",0MF,CN3,MG\" CLEAR CAPPING AREA, PUSH RESET INPUT \",MJ8",
            ";",
            "; No homing routine for rotary axis necessary",
            ";",
            "MD15,2DH,SV[V_HOMING_ROTARY],SA[A_HOMING_ROTARY],SQ[Q_HOMING_ROTARY],MF,MJ25",
            ";",
            "; Entry point for Single cycle command from user interface",
            ";",
            "MD24,2MF,MG\"!I1\",MG\"Single cycle\",EV3,MJ28",
            ";",
            "; Start of main loop",
            "; - Wait for start input (input0) to be OFF",
            "; - Wait for start capping input (input2) to be OFF",
            "; - Wait for start input (input0) to be ON",
            "; - Then clear all used registers and start the overall timer",
            ";",
            "MD25,1PM,MA[PARK_POSITION],GO,WS10,2MF,EV3,1SQ[Q_HOME_LINEAR],CN0",
            "MD26,1SQ12000,IN0,MJ27,NO,RP",
            "MD27,MG\"!I1\",MG\"IP 0\",MJ28",
            "MD28,UM1,CF0,CF1,CF2,CF3,CF8,CF9,CF10,CF11,CF12,CF13,CF14,CF15,MJ29",
            "MD29,AL0,AR15,AR20,AR21,AR22,AR23,AR24,AR25,AR26,AR27,AR28,AR29,AR30,AR31,WL1830,MJ30",
            ";",
            ";",
            ";",
            "; COMMENT_CAP_ON_CONTAINER",
            "; COMMENT_ROTATE_IN_REVERSE",
            "; Move linear close to Container",
            ";",
            "MD30,1PM,MN,1SQ[Q_MOVE_CLOSE_TO_CAP_ON_CONTAINER_LINEAR],SV[V_MOVE_CLOSE_TO_CAP_ON_CONTAINER_LINEAR],SA[A_MOVE_CLOSE_TO_CAP_ON_CONTAINER_LINEAR],MA[POS_CLOSE_TO_CAP_ON_CONTAINER_LINEAR],GO,WS50,MJ40",
            ";",
            "; (Macros 31-39 reserved for furture use)",
            "; Softland on container/cap",
            ";",
            "MD40,1VM,MN,SQ[Q1_SOFTLAND_LINEAR_CAP_ON_CONTAINER],SV[V_SOFTLAND_LINEAR_CAP_ON_CONTAINER],SA[A_SOFTLAND_LINEAR_CAP_ON_CONTAINER],DI0,GO,WA100",
            "MD41,RW538,IG[PERR_SOFTLAND_LINEAR_CAP_ON_CONTAINER],MJ42,NO,RP",
            ";",
            "; Change to force mode, capture and report the datum (Format is Hnnnn)",
            "; Check if the datum is within limits",
            ";",
            "MD42,1PM,SQ[Q_CALC_PF_AFT_TEACH_ADD_LINEAR],QM0,SQ[Q_CALC_PF_AFT_TEACH_ADD_LINEAR],WA150,CN8,RL494,AR15,MG\"!H\":15,AR21",
            "MD43,RL494,IB[CAP_ON_CONTAINER_POSITION_MIN_LINEAR],MJ45,NO,IG[CAP_ON_CONTAINER_POSITION_MAX_LINEAR],MJ46,NO,MG\"HEIGHT OK\",MJ48",
            ";",
            ";",
            "MD45,1PM,MN,GH,WS20,CN2,MG\"45-CAP/CONTAINER TOO HIGH\",MJ6",
            "MD46,1PM,MN,GH,WS20,CN2,MG\"46-CAP/CONTAINER TOO LOW/NO PART PRESENT\",MJ6",
            ";",
            ";",
            "MD48,MG\"48-WAITING FOR INPUT 2, START CAPPING\",WN2,AL0,WL1830,MJ50",
            "; Rotate in reverse to find first thread",
            "; During reverse rotation Reg20 holds the minimum value (peak detect) of the linear.",
            "; The start of thread is found when the linear is 1mm above the detected minimum",
            "; This process must be completed within 2.0 revolution, if not then \"no thread found\" error.",
            "; During search the following error of the rotary may not exceed 5000 encoder counts",
            ";",
            "MD50,RA21,AR20,2VM,2MN,2SV[V_REVERSE_FIND_ROTARY],SA[A_REVERSE_FIND_ROTARY],SQ[Q_REVERSE_FIND_ROTARY],DI1,GO,WA150,2DH",
            "MD51,MC169,IG[20.0_MAX_ROTARY_ERROR],MJ54,NO,RL638,IB-[2.0_REV_ROTARY],MJ53,NO,RL494,IB@20,AR20,RP,AS@20,IG[1mm_THREAD_DROP_LINEAR],MJ52,NO,RP",
            "MD52,2PM,2DH,RL494,AR25,MG\"!HS\":25,MG\"Threads Aligned\",MJ60 ;Message !HS added for Linear Height Start position",
            "MD53,MG\"53-No Threads, Misaligned\",MJ106",
            "MD54,MG\"54-Rotary Error,Cross Thread,Clear Jam\",MJ106",
            ";",
            ";",
            "; Define Home for Rotary",
            "; Start rotation forward",
            "; Check the position error to ensure threads aligned/cap has torqued out",
            "; check if the linear has moved forward by 2mm or more",
            "; check if rotary has gone over 2 revolutions, if so threads have not started/no thread/no cap",
            ";",
            "MD60,2DH,1PM,SQ[Q_CALC_PF_AFT_TEACH_ADD_LINEAR],QM0,SQ[Q_CALC_PF_AFT_TEACH_ADD_LINEAR],2VM,MN,DI0,MC150,GO,WA20,MC151,WA[T_WA_DELAY_TO_MONITOR_MD60],RL1830,AR34",
            "MD61,MC169,IG[MAX_ROTARY_ERROR],MJ70,NO,RL494,AS@25,IG[MOVE_FORWARD_2mm], MG\"62-THREAD ENGAGED\",MJ70,NO, RL638,IG[2.0_REV_ROTARY],NO MJ63,RP",
            "; MD62 not used",
            ";",
            "MD63,2MF,1PM,AB,MC180,MG\"63-NO THREADS/MISALIGNED\",MC100,MC102,WF0,WF2,MJ26",
            "; MD64 not used",
            ";",
            "; The thread has been picked up.",
            "; Now rotate further with low linear force until required depth is reached.",
            "; The torque is reached along with the following error limit (rotary sensativity setting in GUI)",
            "; the rotary position exceeds 1.5x/2.5x # of Rev. (field to be added to have customer input the # of Rev's for a cap)",
            ";",
            "MD70,MC151,MC169,MC171,RW682,IG[MAX_ROTARY_ERROR],MJ73,NO,RL638,IG[NUM_OF_THREADS_CHECK_VAL_ENC_COUNTS],NO,MJ72,RP",
            ";",
            "; If the rotation exceeded 1.5x the given spec. (GUI setting) ****",
            "; Turn on output 2, Jump to macro 75 ******",
            "MD72,MC74,MG\"72-FAILED, ROTATED TO FAR\",MG\" ,CHECK FOR MISSING THREADS OR SLIPPAGE\",MC180,MJ75",
            ";",
            "; Torque & position error has been met, continue on for verification of process ",
            "MD73,MC74,MJ75",
            ";",
            ";",
            ";",
            ";Store the linear position to Reg 35, rotary positon to Reg 40 ",
            "; Turn off the rotary axis, turn on output 11 ( relase gripper)",
            "MD74,RL494,AR35,RL638,AR40,2MF,1AB,1PM,CN11,RC",
            ";",
            "; Calculate linear distance, Report the linear and rotary distance.",
            "MD75,RA35,AS@25,AR36,MG\"!LD \":36,RA40,MG\"!RD \":40,MJ80",
            ";",
            "; Integrated Automation Macro, waits for input 8 to retract to safe position ",
            "MD80,SQ[SQ_CLOSE_TO_PART],2MF,MG\"L80-WAITING FOR RETRACT INPUT 8\",WN8,MC100,MJ82",
            ";",
            ";",
            "MD82,MJ85",
            ";",
            ";Final verification of capping process for linear axis",
            "MD85,RA36,IB[MIN_LINEAR_DISTANCE],NO,MJ86,IG[MAX_LINEAR_DISTANCE],NO,MJ87,MJ90",
            "MD86,MC180,MG\"86-DEPTH DID NOT REACH MIN. LIMIT, FAILED !\",MC102,MJ25",
            "MD87,MC180,MG\"87-DEPTH EXCEEDED MAX. LIMIT, FAILED !\",MC102,MJ25",
            ";",
            ";Final verification of capping process for rotary axis",
            "MD90,RA40,IB[MIN_ROTARY_DISTANCE],NO,MJ91,IG[MAX_ROTARY_DISTANCE],NO,MJ92,MJ95",
            "MD91,MC180,MG\"91-ROTATION DID NOT REACH MIN. LIMIT, FAILED !\",MC102,WF0,WF2,MJ25",
            "MD92,MC180,MG\"92-ROTATION EXCEEDED MAX. LIMIT, FAILED !\",MC102,WF0,WF2,MJ25",
            ";",
            "; Verify that no process failed(REG28) and that actuator is retracted, then turn on output 1(pass), macro jump to MJ25",
            "MD95,RA28,IG0,NO,MJ96,MG\"95-CAP PASSED\",MC102,WF0,WF2,MJ25",
            "MD96,MG\"96-CAP FAILED\",CN2,MC102,WF0,WF2,MJ25",
            ";",
            ";",
            "; Retract to park position ",
            ";",
            ";",
            ";",
            "MD100,CN11,1PM,MC156,1MA[PARK_POSITION],GO,WS100,RC",
            ";MD101",
            ";",
            "; Verify actuator is retract to safe position. If it is, Turn on output 0, Return from call, if not Macro Jump 103",
            "MD102,MC100,RL494,IG[SAFE_PARK_POS],NO,MJ103,CN0,MG\" RETRACTED\",RL1830,MG\"Cycle time is \":0:N,MG\" msec.\",RC",
            "; Actuator is not retracted.",
            "MD103,1SQ10000,2MF,CF0,CN3,MG\"FAULT, NOT RETRACTED !\",WN3,WF3,MJ0",
            ";",
            ";",
            ";",
            "MD105,MC180,MJ95",
            "MD106,MC180,MJ95",
            ";",
            ";",
            ";",
            "; ****Soft Start for the Rotary *****",
            "MD150,2SV[V_SOFT1_ROTARY],SA[A_SOFT1_ROTARY],SQ[Q_SOFT1_ROTARY],RC",
            "MD151,2SV[V_SOFT2_ROTARY],SA[A_SOFT2_ROTARY],SQ[Q_SOFT2_ROTARY],RC",
            "MD152,2SV[V_SOFT3_ROTARY],SA[A_SOFT3_ROTARY],SQ[Q_SOFT3_ROTARY],RC",
            "MD153,2SV[V_SOFT4_ROTARY],SA[A_SOFT4_ROTARY],SQ[Q_SOFT4_ROTARY],RC",
            ";",
            ";",
            "; ********** Linear Speeds/ Forces *********",
            "; **** Speed settings for the Linear ***** (adjusted by GUI for rapid moves)",
            "MD156,1SV[V_MOVE_BACK_HOME_LINEAR],SA[A_MOVE_BACK_HOME_LINEAR],SQ[Q_MOVE_BACK_HOME_LINEAR],RC",
            "",
            "; Macro 169: Get absolute value of rotary folloing error in accum",
            ";",
            "MD169,RW682,MJ170",
            ";",
            "; Calculate absolute value of ACC",
            "; Negative values are made positive by complementing and adding 1.",
            "; This is a faster operation than multiplying by -1",
            "; ",
            "MD170,IB0,AC,AA1,RC",
            ";",
            "; Report linear position and rotary error (separated by a comma)",
            "; Reports only once in 20 msec (timer is kept in reg 34)",
            ";",
            "MD171,RL1830,AS@34,IB0,RC,NO,RL1830,AA10,AR34,RL494,AS@25,MG\"!\":0:N,MG\",\":N,RW682,TR0,RC",
            ";",
            "; Set fail output and write 1 to reg 28",
            ";",
            "MD180,CN2,AL1,AR28,RC",
            ";",
            "; Macro 220 is executed when the rotary is blocked during a reverse move",
            "; Manual intervention of the operator is necessary.",
            "; Only way out is to press the reset button",
            ";",
            ";MD220,0AB,MF,CN3",
            ";MD221,MG\"221-Rotary blocked during reverse move\"",
            ";MD222,MG\"Remove block and press reset button\",MJ8",
            ";",
            "; Routine to teach linear force at landing position:",
            ";",
            "; - Do a homing, wait 3 seconds and report linear force (format !Rnnnn)",
            "; - Softland on the part (without initial fast move)",
            "; - Do a position move to 2.0 pitch above softland position",
            "; - Report softland position (format !Lnnnn)",
            "; - After a wait of 1 second report THRO at this position (format !Qnnnn)",
            "; - Go back home",
            ";",
            "MD240,MC10,WA3000,RW530,MG\"!R\":0",
            "MD241,1PM,SQ30000,VM,MN,SQ[Q_PF_BEF_TEACH_ADD_LINEAR],SV[V_SOFTLAND_LINEAR],SA[A_SOFTLAND_LINEAR],DI0,GO,WA100",
            "MD242,RW538,IG[PERR_SOFTLAND_LINEAR_CAP_ON_CONTAINER],MJ243,NO,RP",
            "MD243,RL494,MG\"!L\":0,AS[2.0_PITCH_LINEAR],AR30,1PM,MA@30,GO,WS1000,2ST,WS1000",
            "MD244,RW530,MG\"!Q\":0,1GH,WS100,MJ25"
//            ";",
//            ";",
//            "; Routine used for debuggung only",
//            ";",
//            "MD250,MG\"Rotate Backward\",2AB,2VM,MN,DI1,MC150,GO,WA250,MC152"            
        };

        public int CountRaw()
        {
            return masterProgram.Count();
        }

        public string GetRawString(int index)
        {
            return masterProgram[index];
        }

        public int Count()      // Only valid after a call to Create
        {
            return theProgram.Count;
        }

        public string GetLine(int index) // Only valid after a call to Create
        {
            Trace.Assert(index < theProgram.Count, "Invalid index " + index.ToString());
            return theProgram[index];
        }

        public void Show()
        {
            foreach (string s in masterProgram)
            {
                MessageBox.Show(s);
            }
        }

        // Get global speed of the application:
        // Apply the settin to all accelereations, all speeds and all times
        public void SetSpeed(double factor)
        {
            for (int i = 0; i < variables.Count(); i++)
            {
                Variable var = variables.Get(i);
                if ((var.Name.IndexOf("A_") == 0) || (var.Name.IndexOf("V_") == 0))
                {
                    variables.Multiply(var.Name, factor);
                }
            }
        }

        public void SetFastMoveParameters( )
        {
            variables.SetValue("V_FAST_MOVE", variables.GetValue("VF_HOMING_LINEAR") * 2);
            variables.SetValue("A_FAST_MOVE", variables.GetValue("AF_HOMING_LINEAR") * 2);
            variables.SetValue("Q_FAST_MOVE", limitLinearSQ(variables.GetValue("Q_HOME_LINEAR")+5000));
        }

        public void SetRotaryResolution(int encoderCounts)
        {
//            MessageBox.Show("SetRotaryResolution(" + encoderCounts.ToString() + ")");
            variables.SetValue("0.25_REV_ROTARY", (int)(encoderCounts * 0.25));
            variables.SetValue("0.75_REV_ROTARY", (int)(encoderCounts * 0.75));
            variables.SetValue("1.0_REV_ROTARY", encoderCounts);
            variables.SetValue("1.5_REV_ROTARY", (int)(encoderCounts * 1.5));
            variables.SetValue("2.0_REV_ROTARY", encoderCounts * 2);
        }

        public void SetParkPosition(int encoderCounts)
        {
            variables.SetValue("PARK_POSITION", encoderCounts); 
        }

       
        public void SetNumofThreadsCheckValue( double num_of_threads, bool rotate_reverse_selected)
        {
            if (rotate_reverse_selected)
            {
                variables.SetValue("NUM_OF_THREADS_CHECK_VAL_ENC_COUNTS", (int)(variables.GetValue("1.0_REV_ROTARY") * 1.5* num_of_threads));
            }
            else
            {
                variables.SetValue("NUM_OF_THREADS_CHECK_VAL_ENC_COUNTS", (int)(variables.GetValue("1.0_REV_ROTARY") * 2.5 * num_of_threads));
            }


            if( num_of_threads <3)
            {
                Translate("MD60,", "MD60,2DH,1PM,SQ[Q_CALC_PF_AFT_TEACH_ADD_LINEAR],QM0,SQ[Q_CALC_PF_AFT_TEACH_ADD_LINEAR],2VM,MN,DI0,MC150,GO,WA20,MC151,WA[T_WA_DELAY_TO_MONITOR_MD60],RL1830,AR34,MJ70");
      
                Translate("MD61,", ";");
                Translate("MD63,", ";"); 
            }
            if (num_of_threads >= 2)
            {
                Translate("MD171,", "MD171, RL1830, AS@34, IB0, RC, NO, RL1830, AA20, AR34, RL494, AS@25, MG\"!\":0:N,MG\",\":N,RW682,TR0,RC");
            }
        }

        public void SetLinearConstants(int encoderCounts)
        {
            variables.SetValue("1mm_THREAD_DROP_LINEAR",encoderCounts);
            variables.SetValue("MOVE_FORWARD_2mm", 2*encoderCounts);
        }

        public void SetRotarySensitivity(int encoderCounts)
        {
//            MessageBox.Show("SetRotarySensitivity(" + encoderCounts.ToString() + ")");
            variables.SetValue("MAX_ROTARY_ERROR", encoderCounts);
            variables.SetValue("20.0_MAX_ROTARY_ERROR", 20*encoderCounts);
            variables.SetValue("MAX_REV_ROTARY", encoderCounts);

        }
        public void SetDwellTime(int encoderCounts)
        {
            variables.SetValue("T_WA_DELAY_TO_MONITOR_MD60", variables.GetValue("T_WA_DELAY_TO_MONITOR_MD60"));  
        }

        public void ResetTranslation()
        {
            translateFrom.Clear();
            translateInto.Clear();
        }

        public void Translate(string from, string into)
        {
            translateFrom.Add(from);
            translateInto.Add(into);
        }

        public void DemoModeEnabled( )
        {
            string demo_string1 = "; Retract to home / park position \r\n" +
                                  "; ******Use this version for Demo Mode Only ****\r\n" +
                                  "; capping Demo Macro, Pause prior to retracting / unthreading\r\n" +
                                  "; Pause for 2 sec., ais 1 in force mode retracting lightly, start rotating in rev.\r\n" +
                                  "; Read the linear pos.to monitor that the cap is off the threads, read the rot.pos to see that cap is off\r\n" +
                                  "; Once cap is off, turn off Axis2, then move to retracted pos./ home.Set the force low / safe.\r\n" +
                                  "; ";
            string MD48 = "MD48,AL0,WL1830,MJ50";
            string MD80 = "MD80,MC100,MJ82";
            string MD100 = "MD100,WA1000,1QM0,1SQ[Q_RETRACT_LINEAR],2VM,MN,MC150,DI1,GO,WA50,MC152,WA50,MC153,WA200";
            string MD101 = "MD101,RL494,IB@15,NO,JR6,RL638,IB-[0.75_REV_ROTARY],NO,JR2,RP,2MF,1PM,MC156,GH,WS200,1SQ10000,RC";
            string MD102 = "MD102,RL494,IB[SAFE_PARK_POS],NO,MJ103,0MF,CF0,CN3,MG\"NOT RETRACTED FAULT! \",WF0,WF2,WN3,WF3,MJ0";
            string MD103 = "MD103,CN0,MG\"103-LINEAR HOME/RETRACTED\",RC";

            Translate("MD48,",MD48);
            Translate("; Integrated Automation Macro,", "; ****** Use this version for Demo Mode Only ****");
            Translate("MD80,",MD80);
            Translate("; Retract to park position", demo_string1);
            //Translate("MD100,", MD100 + "\r\n" + MD101);
            Translate("MD100,", MD100);
            Translate(";MD101", MD101);
            Translate("MD102,", MD102);
            Translate("MD103,", MD103);

        }

        public void EnableRotateReverseFind(bool enable)
        {
            if (!enable)
            {
                Translate("MD50,", "MD50,MJ60; Rotate in reverse to find first thread is disabled");
                Translate("MD51,", ";");
                Translate("MD52,", ";");
                Translate("MD53,", ";");
                Translate("MD54,", ";");
            }
        }

        public void EnableHeightCheck(bool enable)
        {
            if (!enable)
            {
                Translate("MD43,", "MD43,MJ48; Part height check is disabled");
                Translate("MD45,", ";");
                Translate("MD46,", ";");
            }
        }

        public void EnableLinearDistanceTravelCheck(bool enable)
        {
            if(!enable)
            {
                //Translate("MD81","MD81,MJ85");
                //Translate("MD82", ";");
                Translate("MD83", ";");
                Translate("MD84", ";");
                Translate("MD85", "MD85,MJ90");
                Translate("MD86", ";");
                Translate("MD87", ";");
                Translate("MD88", ";");
            }
        }

        public void EnableRotaryDistanceTravelCheck(bool enable)
        {
            if(!enable)
            {
                
                Translate(";Final verification of capping process for rotary axis", ";");
                Translate("MD90", "MD90,MJ95");
                Translate("MD91", ";");
                Translate("MD92", ";");
            }
        }

        public void EnablePushPull(bool enabled, bool top)
        {
            if (!enabled)
            {
                    Translate(";COMMENT1", ";");
                    Translate(";COMMENT2", "; Push/pull test is disabled:");
                    Translate(";COMMENT3", "; Rotate out of the thread");
                    Translate(";COMMENT4", ";");
                    Translate(";COMMENT5", "");
                    Translate(";COMMENT6", "");
                    Translate(";COMMENT7", "");
                    Translate(";COMMENT8", "");

                    Translate("MD95,", "MD95,MJ110");
                    Translate("MD96,", "");
                    Translate("MD97,", "");
                    Translate("MD98,", "");
                    Translate("MD99,", "");
                    Translate("MD100,", "");
            }
            else
            {
                if (top)
                {
                    Translate(";COMMENT1", ";");
                    Translate(";COMMENT2", "; For the push/pull test:");
                    Translate(";COMMENT3", "; First rotate back to 3 pitches from landing point");
                    Translate(";COMMENT4", "; During move check rotary following error and check if linear keeps decreasing");
                    Translate(";COMMENT5", ";");
                    Translate(";COMMENT6", ";");
                    Translate(";COMMENT7", ";Rotary stopped 3 pitches from the landing point: Do the push/pull test now");
                    Translate(";COMMENT8", ";");
                
                }
                else    // It must be bottom then
                {
                    Translate(";COMMENT1", ";");
                    Translate(";COMMENT2", "; Push/pull test at bottom of thread");
                    Translate(";COMMENT3", "; Stop the rotary and do the push / pull test after stop");
                    Translate(";COMMENT4", ";");
                    Translate(";COMMENT5", "");
                    Translate(";COMMENT6", ";");
                    Translate(";COMMENT7", "; Rotary stopped at the bottom: Do the push/pull test now");
                    Translate(";COMMENT8", ";");
                    Translate("MD95", "MD95,2PM,2ST,WS100,MJ97");
                    Translate("MD96,", "");
                }
            }
        }

        public void SetCapInPlace( )
        {
            if(Globals.mainForm.cmbCappingType.SelectedIndex == 0)
            {
                Translate("; COMMENT_CAP_ON_CONTAINER", "; CAP IS ALREADY IN PLACE ON CONTAINER, TOOLING IT’S A: CHUCK/ WEDGE ON /VACUUM/ or GRIPPER TYPE TOOLING");
                Translate("; COMMENT_OUTPUT_9", "; ");
            }
            else if(Globals.mainForm.cmbCappingType.SelectedIndex == 1)
            {
                Translate("; COMMENT_CAP_ON_CONTAINER", ";");
                Translate("; COMMENT_OUTPUT_9", "; OUTPUT 9: AT CLEAR TO PROCEED POSITION");
            }
            if(Globals.mainForm.chkRotateReverse.Checked)
            {
                Translate("; COMMENT_ROTATE_IN_REVERSE", "; With Rotate in Reverse to locate first thread");
            }
            else
            {
                Translate("; COMMENT_ROTATE_IN_REVERSE", ";");
            }
        }

        public void SetRotaryDirection(string direction)
        {
            //
            // The master program is created for standard right hande thread.
            // In case of left handed thread all the output and encoder phasing
            // of the rotary axis must be reversed.
            //
            switch (direction)
            {
                case Globals.STR_THREAD_DIRECTION_RIGHT_HANDED:
                    variables.SetValue("PHASING_ROTARY", 0);        // Default phasing
                    break;
                case Globals.STR_THREAD_DIRECTION_LEFT_HANDED:
                    variables.SetValue("PHASING_ROTARY", 3);        // Both output and encoder reversed
                    break;

            }
        }

        //public void SetThreadType(string threadtype)
        //{
        //    //
        //    // The master program is created for standard right hande thread.
        //    // In case of left handed thread all the output and encoder phasing
        //    // of the rotary axis must be reversed.
        //    //
        //    //switch (threadtype)
        //    //{
        //    //    case Globals.STR_THREAD_TYPE_INTERNAL:
        //    //        Translate("MD100", "MD100,MG\"Push-pull test out of limits: OVERSIZED Thread\",MJ105");
        //    //        break;
        //    //    case Globals.STR_THREAD_TYPE_EXTERNAL:
        //    //        Translate("MD100", "MD100,MG\"Push-pull test out of limits: UNDERIZED Thread\",MJ105");
        //    //        break;

        //    //}
        //}

        private void LimitTorque(string variableName, int maxvalue)
        {
            if (variables.GetValue(variableName) > maxvalue)
            {
                variables.SetValue(variableName, maxvalue);
            }
        }

        public void SetPermisableTorque(int maxSQ)
        {
                LimitTorque("Q_HOMING_ROTARY", maxSQ);
                LimitTorque("Q_REVERSE_FIND_ROTARY", maxSQ);
                LimitTorque("Q_BRAKE_FOR_PUSH_PULL_TEST_ROTARY", maxSQ);
                LimitTorque("Q_SOFT1_ROTARY", maxSQ);
                LimitTorque("Q_SOFT2_ROTARY", maxSQ);
                LimitTorque("Q_SOFT3_ROTARY", maxSQ);
                LimitTorque("Q_SOFT4_ROTARY", maxSQ);
        }

        private int limitLinearSQ(int sq)
        {
            if (sq > 32000)
            {
                sq = 32000;
            }
            return sq;
        }

        public void setLinearForce(string teachedSQ)
        {
            if (teachedSQ != "")
            {
                int sq = int.Parse(teachedSQ);
                
                variables.SetValue("Q_CALC_PF_AFT_TEACH_ADD_LINEAR", limitLinearSQ(sq + variables.GetValue("Q_PF_AFT_TEACH_ADD_LINEAR")));
                variables.SetValue("SQ_CLOSE_TO_PART", limitLinearSQ(sq));  
                variables.SetValue("SAFE_REST_SQ", limitLinearSQ(sq + 5000)); 
                variables.SetValue("Q_FLOAT_LINEAR", limitLinearSQ(sq + 1000));
                variables.SetValue("Q_RETRACT_LINEAR", limitLinearSQ(sq - 2000));
                if(limitLinearSQ(sq + 5000) > 15000)
                {
                    Translate("MD80,", "MD80,SQ[SQ_CLOSE_TO_PART],MG\"L80-WAITING FOR RETRACT INPUT 8\",WN8,2MF,MC100,MJ82"); 
                }
                else
                {
                    Translate("MD80,", "MD80,SQ[SAFE_REST_SQ],MG\"L80-WAITING FOR RETRACT INPUT 8\",WN8,2MF,MC100,MJ82");
                }

            }

        }

        public void setLinearForceHome(string teachedSQ)
        {
            if (teachedSQ != "")
            {
                int sq = int.Parse(teachedSQ) + 4000;
                variables.SetValue("Q_HOME_LINEAR", limitLinearSQ(sq + 4000));
            }
        }

        public void SetLinearPID(double P, double I, double D)
        {
            variables.Multiply("P_LINEAR", P / 100.0);
            variables.Multiply("I_LINEAR", I / 100.0);
            variables.Multiply("D_LINEAR", D / 100.0);
        }

        public void SetRotaryPID(double P, double I, double D)
        {
            variables.Multiply("P_ROTARY", P / 100.0);
            variables.Multiply("I_ROTARY", I / 100.0);
            variables.Multiply("D_ROTARY", D / 100.0);
        }

        public void SetPositionCloseToContainer(int encoderCounts)
        {
            variables.SetValue("POS_CLOSE_TO_CAP_ON_CONTAINER_LINEAR", encoderCounts);
        }

        public void SetLandedHeightMin(int encoderCounts)
        {
            variables.SetValue("CAP_ON_CONTAINER_POSITION_MIN_LINEAR", encoderCounts);
        }

        public void SetLandedHeightMax(int encoderCounts)
        {
            variables.SetValue("CAP_ON_CONTAINER_POSITION_MAX_LINEAR", encoderCounts);
        }

        public void SetPitch(int encoderCounts)
        {
            
            variables.SetValue("0.5_PITCH_LINEAR", (5 * encoderCounts + 5) / 10);
            variables.SetValue("0.8_PITCH_LINEAR", (8 * encoderCounts + 5) / 10);
            variables.SetValue("1.0_PITCH_LINEAR", encoderCounts);
            variables.SetValue("1.5_PITCH_LINEAR", (3 * encoderCounts + 1) / 2);
            variables.SetValue("2.0_PITCH_LINEAR", 2 * encoderCounts);
            variables.SetValue("3.0_PITCH_LINEAR", 3 * encoderCounts);
        }

        public void SetSafeMinimumRetractDistance(int encoderCounts)
        {
            variables.SetValue("SAFE_MIN_RETRACT_POS", encoderCounts);
        }

        public void SetSafeParkPosition(int encoderCounts)
        {
            variables.SetValue("SAFE_PARK_POS", encoderCounts);
        }

        public void SetMinLinearDistanceTravelled(int encoderCounts)
        {
            variables.SetValue("MIN_LINEAR_DISTANCE", encoderCounts);
        }

        public void SetMaxLinearDistanceTravelled(int encoderCounts)
        {
            variables.SetValue("MAX_LINEAR_DISTANCE", encoderCounts);
        }

        public void SetMinRotaryDistanceTravelled(int encoderCounts)
        {
            variables.SetValue("MIN_ROTARY_DISTACE", encoderCounts);
        }

        public void SetMaxRotaryDistanceTravelled(int encoderCounts)
        {
            variables.SetValue("MAX_ROTARY_DISTANCE", encoderCounts);
        }
        //public void SetThreadDepth(int encoderCounts)
        //{
        //    variables.SetValue("1.0_DEPTH_LINEAR", encoderCounts);
        //}

        public void SetPushPullLimit(int encoderCounts)
        {
            variables.SetValue("PUSH_PULL_LIMIT", encoderCounts);
        }


        public bool Create()
        {
            theProgram.Clear();
            foreach (string s in masterProgram)
            {
                string compiled = s.Trim();
                if (compiled.Length > 0)    // Do not copy empty lines
                {
                    if (compiled[0] != ';')     // Do not substitute variables in comment lines
                    {
                        for (int i = 0; i < variables.Count(); i++)
                        {
                            Variable v = variables.Get(i);
                            string strTemp = "[" + v.Name + "]";
                            while (compiled.IndexOf(strTemp) > 0)
                            {
                                compiled = compiled.Replace(strTemp, v.Value.ToString());
                            }
                        }

                        int index1 = compiled.IndexOf('['); // The first delimiter
                        int index2 = compiled.IndexOf(']', index1 + 1);

                        if (index1 != index2)
                        {
                            Trace.Assert(index1 == index2, "Unresolved variable in string " + compiled);
                        }

                    }

                    for (int i = 0; i < translateFrom.Count; i++)
                    {
                        if (compiled.IndexOf(translateFrom[i]) == 0)
                        {
                            compiled = translateInto[i];
                            break;
                        }
                    }

                    if (compiled[0] != ';')     // Do not substitute variables in comment lines
                    {
                        for (int i = 0; i < variables.Count(); i++)
                        {
                            Variable v = variables.Get(i);
                            string strTemp = "[" + v.Name + "]";
                            while (compiled.IndexOf(strTemp) > 0)
                            {
                                compiled = compiled.Replace(strTemp, v.Value.ToString());
                            }
                        }

                        int index1 = compiled.IndexOf('['); // The first delimiter
                        int index2 = compiled.IndexOf(']', index1 + 1);

                        if (index1 != index2)
                        {
                            Trace.Assert(index1 == index2, "Unresolved variable in string " + compiled);
                        }

                    }

                    if (compiled.Trim() != "")
                    {
                        theProgram.Add(compiled);
                    }
                }
            }
            return true;
        }
    }
    public static class Constants
    {
        public static string[] Defaults()
        {
            return new string[]
            {
                "P_LINEAR, 12",
                "I_LINEAR, 50",
                "D_LINEAR, 600",
                "IL_LINEAR, 8000",
                "FR_LINEAR, 1",
                "RI_LINEAR, 0",
                "OO_LINEAR, 0",
                "FR_ROTARY, 0",
                "RI_ROTARY, 0",
                "OO_ROTARY, 0",
                "P_ROTARY, 80",
                "I_ROTARY, 120",
                "D_ROTARY, 800",
                "IL_ROTARY, 10000",
                "VF_HOMING_LINEAR, 1500000",
                "AF_HOMING_LINEAR, 20000",
                "QF_HOMING_LINEAR, 20000",
                "Q_HOME_LINEAR, 20000",
                "HARDSTOP_THRESHOLD_LINEAR, -2500",
                "INDEX_LIMIT_LINEAR, 2500",
                "V_HOMING_ROTARY, 600000",
                "A_HOMING_ROTARY, 4000",
                "Q_HOMING_ROTARY, 15000",
                "Q_MOVE_CLOSE_TO_CAP_ON_CONTAINER_LINEAR, 25000",
                "V_MOVE_CLOSE_TO_CAP_ON_CONTAINER_LINEAR, 1250000",
                "A_MOVE_CLOSE_TO_CAP_ON_CONTAINER_LINEAR, 20000",
                "Q1_SOFTLAND_LINEAR_CAP_ON_CONTAINER, 25000",
                "V_SOFTLAND_LINEAR_CAP_ON_CONTAINER, 500000",
                "A_SOFTLAND_LINEAR_CAP_ON_CONTAINER, 20000",
                "PERR_SOFTLAND_LINEAR_CAP_ON_CONTAINER, 2500",
                "Q_PF_BEF_TEACH_ADD_LINEAR, 25000",
                "Q_PF_AFT_TEACH_ADD_LINEAR, 5000",
                "Q_CALC_PF_AFT_TEACH_ADD_LINEAR, 0",
                "V_REVERSE_FIND_ROTARY, 1000000",
                "A_REVERSE_FIND_ROTARY, 50000",
                "Q_REVERSE_FIND_ROTARY, 15000",
                "A_BRAKE_FOR_PUSH_PULL_TEST_ROTARY, 3000",
                "Q_BRAKE_FOR_PUSH_PULL_TEST_ROTARY, 4000",
                "T_WA_DELAY_TO_MONITOR_MD60, 100",
                "T_WA_DELAY_TO_MONITOR_MD70, 200",
                "Q_FOR_PUSH_PULL_TEST_LINEAR, 32000",
                "V_ROTATE_BACK_LINEAR, 200000",
                "Q_ROTATE_BACK_LINEAR, 10000",
                "V_MOVE_BACK_HOME_LINEAR, 2000000",
                "A_MOVE_BACK_HOME_LINEAR, 30000",
                "Q_MOVE_BACK_HOME_LINEAR, 15000",
                "Q_BREAKOUT_ROTARY, 32000",
                "T_BREAKOUT_ROTARY, 400",
                "V_SOFTLAND_LINEAR, 0",
                "A_SOFTLAND_LINEAR, 0",
                "Q_FLOAT_LINEAR, 20000",
                "V_SOFT1_ROTARY, 50000",
                "A_SOFT1_ROTARY, 2000",
                "Q_SOFT1_ROTARY, 4000",
                "V_SOFT2_ROTARY, 1300000",
                "A_SOFT2_ROTARY, 60000",
                "Q_SOFT2_ROTARY, 22000",
                "V_SOFT3_ROTARY, 1000000",
                "A_SOFT3_ROTARY, 60000",
                "Q_SOFT3_ROTARY, 22000",
                "V_SOFT4_ROTARY, 2000000",
                "A_SOFT4_ROTARY, 60000",
                "Q_SOFT4_ROTARY, 22000",
                "Q_SETUP_LINEAR, 25000",
                "V_SETUP_LINEAR, 500000",
                "A_SETUP_LINEAR, 20000",
                //
                // The following values are filled in by the program
                //
                "PHASING_ROTARY, 0",
                "POS_CLOSE_TO_CAP_ON_CONTAINER_LINEAR, 45000",
                "CAP_ON_CONTAINER_POSITION_MIN_LINEAR, 46000",
                "CAP_ON_CONTAINER_POSITION_MAX_LINEAR, 50000",
                "PUSH_PULL_LIMIT, 100",
                "0.25_REV_ROTARY, 7000",
                "0.75_REV_ROTARY, 21000",
                "MAX_ROTARY_ERROR, 7000",
                "MAX_REV_ROTARY, 7000",
                "20.0_MAX_ROTARY_ERROR, 140000",
                "1.0_REV_ROTARY, 28000",
                "1.5_REV_ROTARY, 42000",
                "2.0_REV_ROTARY, 56000",
                "1mm_THREAD_DROP_LINEAR, 200",
                "MOVE_FORWARD_2mm, 400",
                "0.5_PITCH_LINEAR, 500",
                "0.8_PITCH_LINEAR, 800",
                "1.0_PITCH_LINEAR, 1000",
                "1.5_PITCH_LINEAR, 1500",
                "2.0_PITCH_LINEAR, 2000",
                "3.0_PITCH_LINEAR, 3000",
                "1.0_DEPTH_LINEAR, 7000",
                "SAFE_MIN_RETRACT_POS, 1000",
                "MIN_LINEAR_DISTANCE, 1000",
                "MAX_LINEAR_DISTANCE, 1000",
                "MIN_ROTARY_DISTANCE, 1000",
                "MAX_ROTARY_DISTANCE, 1000",
                "Q_RETRACT_LINEAR, 18000",
                "V_FAST_MOVE, 2000000",
                "A_FAST_MOVE, 2000000",
                "Q_FAST_MOVE, 200000",
                "NUM_OF_THREADS_CHECK_VAL_ENC_COUNTS, 15000",
                "SAFE_PARK_POS, 1000",
                "DEMO, 0",
                "PARK_POSITION, 0",
                "SAFE_REST_SQ, 0",
                "SQ_CLOSE_TO_PART, 0",
            };
        }
    }

}
