﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SMAC_Capping_Control_Center
{
    //
    //
    // The ValueChecker class is used to check if one of the variables has been changed
    //
    // First call Load() - This will read the value of all variables on the mainform
    // Call Changed() to check if one of the values has been changed since the last call to Load.
    //
    class ValueChecker
    {
        private string text1 = "";
        private string text3 = "";
        private string text5 = "";
        private string text6 = "";
        private string text7 = "";

        private bool bool1 = false;
        private bool bool2 = false;
        private bool bool3 = false;
        private bool bool4 = false;
        private bool bool5 = false;


        private double double1 = 0;
        private double double2 = 0;
        private double double3 = 0;
        
        private double double6 = 0;
        private double double7 = 0;
        private double double8 = 0;
        private double double9 = 0;
        private double double10 = 0;
        private double double11 = 0;
        private double double12 = 0;
        private double double13 = 0;
        private double double14 = 0;

        private int int1 = 0;

        public void Load()
        {
            text1 = Globals.mainForm.cmbActuator.SelectedItem.ToString();
            text3 = Globals.mainForm.cmbThreadDirection.SelectedItem.ToString();
            text5 = Globals.mainForm.txtTeachedSqLinear.Text;
            text6 = Globals.mainForm.txtTeachedHeight.Text;
            text7 = Globals.mainForm.txtTeachedSqLinearHome.Text;

            bool1 = Globals.mainForm.chkCheckLandedHeight.Checked;
            bool2 = Globals.mainForm.chkRotateReverse.Checked;
            



            double1 = Globals.mainForm.ibdPositonCloseToContainer.Doublevalue;
            double2 = Globals.mainForm.ibdLandedHeightMin.Doublevalue;
            double3 = Globals.mainForm.ibdLandedHeightMax.Doublevalue;
            
            double7 = Globals.mainForm.ibdMaxTorque.Doublevalue;

            double8 = Globals.mainForm.ibdTuningLinearP.Doublevalue;
            double9 = Globals.mainForm.ibdTuningLinearI.Doublevalue;
            double10 = Globals.mainForm.ibdTuningLinearD.Doublevalue;

            double11 = Globals.mainForm.ibdTuningRotaryP.Doublevalue;
            double12 = Globals.mainForm.ibdTuningRotaryI.Doublevalue;
            double13 = Globals.mainForm.ibdTuningRotaryD.Doublevalue;

            double14 = Globals.mainForm.ibdSpeed.Doublevalue;

            int1 = Globals.mainForm.ibiRotarySensitivity.Intvalue;

        }

        public bool Changed()
        {
            return
                (text1 != Globals.mainForm.cmbActuator.SelectedItem.ToString()) ||
                //(text2 != Globals.mainForm.cmbPitchUnits.SelectedItem.ToString()) ||
                (text3 != Globals.mainForm.cmbThreadDirection.SelectedItem.ToString()) ||
                //(text4 != Globals.mainForm.cmbThreadType.SelectedItem.ToString()) ||
                (text5 != Globals.mainForm.txtTeachedSqLinear.Text) ||
                (text6 != Globals.mainForm.txtTeachedHeight.Text) ||
                (text7 != Globals.mainForm.txtTeachedSqLinearHome.Text) ||

                (bool1 != Globals.mainForm.chkCheckLandedHeight.Checked) ||
                (bool2 != Globals.mainForm.chkRotateReverse.Checked) ||

                (double1 != Globals.mainForm.ibdPositonCloseToContainer.Doublevalue) ||
                (double2 != Globals.mainForm.ibdLandedHeightMin.Doublevalue) ||
                (double3 != Globals.mainForm.ibdLandedHeightMax.Doublevalue) ||
                
                (double7 != Globals.mainForm.ibdMaxTorque.Doublevalue) ||

                (double8 != Globals.mainForm.ibdTuningLinearP.Doublevalue) ||
                (double9 != Globals.mainForm.ibdTuningLinearI.Doublevalue) ||
                (double10 != Globals.mainForm.ibdTuningLinearD.Doublevalue) ||

                (double11 != Globals.mainForm.ibdTuningRotaryP.Doublevalue) ||
                (double12 != Globals.mainForm.ibdTuningRotaryI.Doublevalue) ||
                (double13 != Globals.mainForm.ibdTuningRotaryD.Doublevalue) ||

                (double14 != Globals.mainForm.ibdSpeed.Doublevalue) ||
                (int1 != Globals.mainForm.ibiRotarySensitivity.Intvalue);
        }
    }
}
