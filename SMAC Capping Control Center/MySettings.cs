﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;


namespace SMAC_Capping_Control_Center
{
    class MySettings
    {
        private const string SECTION_SETTINGS = "Settings";
        private const string KEY_ACTUATOR = "Actuator";
        private const string KEY_ORIENTATION = "Orientation";
        private const string KEY_THREAD_DIRECTION = "Thread direction";


        private const string KEY_CAP_TYPE = "Cap type";
        private const string KEY_RETRACT_DIST = "Retract Distance";
        private const string KEY_NUMBER_OF_THREAD = "Number of Thread";
        private const string KEY_PARK_POSITION = "Park Position";
        private const string KEY_LIN_DIST_MIN = "Linear Distance Travel Minimum";
        private const string KEY_LIN_DIST_MAX = "Linear Distance Travel Maximum";
        private const string KEY_ROT_DIST_MIN = "Rotary Distance Travel Minimum";
        private const string KEY_ROT_DIST_MAX = "Rotary Distance Travel Maximum";
        private const string KEY_SPEED_TO_PICK_CAP = "Speed to Pick Cap";


        private const string KEY_POSITION_CLOSE_TO_PART = "Position close to part";
        private const string KEY_CHECK_LANDED_HEIGHT = "Check landed height";
        private const string KEY_LANDED_HEIGHT_MIN = "Landed height min";
        private const string KEY_LANDED_HEIGHT_MAX = "Landed height max";
        private const string KEY_ROTATE_IN_REVERSE = "Rotate in reverse";
        private const string KEY_THREAD_DEPTH = "Thread depth";
        private const string KEY_CHECK_THREAD_CLEARANCE = "Check thread clearance";
        private const string KEY_CHECK_THREAD_CLEARANCE_AT_TOP = "Check thread clearance at top";
        


        private const string KEY_CHECK_THREAD_CLEARANCE_AT_BOTTOM = "Check thread clearance at bottom";
        private const string KEY_THREAD_CLEARANCE = "Thread clearance";
        private const string KEY_PERMISABLE_TORQUE = "Permisable torque";
        private const string KEY_TUNING_LINEAR_P = "Tuning linear P";
        private const string KEY_TUNING_LINEAR_I = "Tuning linear I";
        private const string KEY_TUNING_LINEAR_D = "Tuning linear D";
        private const string KEY_TUNING_ROTARY_P = "Tuning rotary P";
        private const string KEY_TUNING_ROTARY_I = "Tuning rotary I";
        private const string KEY_TUNING_ROTARY_D = "Tuning rotary D";
        private const string KEY_SPEED = "Speed";
        private const string KEY_SENSITIVITY = "Sensitivity";
       
        private const string KEY_TEACHED_SQ_LINEAR = "Teached SQ Linear";
        private const string KEY_TEACHED_SQ_LINEAR_HOME = "Teached SQ Linear Home";
        private const string KEY_TEACHED_HEIGHT = "Teached height";
       


        public MySettings()       // The constructor
        {
        }

        public void Read(string path)
        {
            IniFile inifile = new IniFile(path);
            try
            {
                Globals.mainForm.cmbActuator.SelectedItem = inifile.ReadValue(SECTION_SETTINGS, KEY_ACTUATOR);
                Globals.mainForm.cmbThreadDirection.SelectedItem = inifile.ReadValue(SECTION_SETTINGS, KEY_THREAD_DIRECTION);


                Globals.mainForm.ibdNumofThreads.Doublevalue = inifile.ReadDoubleValue(SECTION_SETTINGS, KEY_NUMBER_OF_THREAD);
                Globals.mainForm.ibdRetractDistance.Doublevalue = inifile.ReadDoubleValue(SECTION_SETTINGS, KEY_RETRACT_DIST);  
                Globals.mainForm.cmbCappingType.SelectedItem = inifile.ReadValue(SECTION_SETTINGS, KEY_CAP_TYPE);      
                Globals.mainForm.ibdPositionClosetoPickCap.Doublevalue = inifile.ReadDoubleValue(SECTION_SETTINGS, KEY_PARK_POSITION);
                Globals.mainForm.ibdSpeedToPickCap.Doublevalue = inifile.ReadDoubleValue(SECTION_SETTINGS, KEY_SPEED_TO_PICK_CAP);
                Globals.mainForm.ibdLinearDistanceMin.Doublevalue = inifile.ReadDoubleValue(SECTION_SETTINGS, KEY_LIN_DIST_MIN);
                Globals.mainForm.ibdLinearDistanceMax.Doublevalue = inifile.ReadDoubleValue(SECTION_SETTINGS, KEY_LIN_DIST_MAX);
                Globals.mainForm.ibdRotaryDistanceMin.Doublevalue = inifile.ReadDoubleValue(SECTION_SETTINGS, KEY_ROT_DIST_MIN);
                Globals.mainForm.ibdRotaryDistanceMax.Doublevalue = inifile.ReadDoubleValue(SECTION_SETTINGS, KEY_ROT_DIST_MAX);


                Globals.mainForm.ibdPositonCloseToContainer.Doublevalue = inifile.ReadDoubleValue(SECTION_SETTINGS, KEY_POSITION_CLOSE_TO_PART);
                Globals.mainForm.chkCheckLandedHeight.Checked = inifile.ReadBoolValue(SECTION_SETTINGS, KEY_CHECK_LANDED_HEIGHT);
               
                Globals.mainForm.ibdLandedHeightMin.Doublevalue = inifile.ReadDoubleValue(SECTION_SETTINGS, KEY_LANDED_HEIGHT_MIN);
                Globals.mainForm.ibdLandedHeightMax.Doublevalue = inifile.ReadDoubleValue(SECTION_SETTINGS, KEY_LANDED_HEIGHT_MAX);
                Globals.mainForm.chkRotateReverse.Checked = inifile.ReadBoolValue(SECTION_SETTINGS, KEY_ROTATE_IN_REVERSE);
                Globals.mainForm.ibdMaxTorque.Doublevalue = inifile.ReadDoubleValue(SECTION_SETTINGS, KEY_PERMISABLE_TORQUE);
                Globals.mainForm.ibdTuningLinearP.Doublevalue = inifile.ReadDoubleValue(SECTION_SETTINGS, KEY_TUNING_LINEAR_P);
                Globals.mainForm.ibdTuningLinearI.Doublevalue = inifile.ReadDoubleValue(SECTION_SETTINGS, KEY_TUNING_LINEAR_I);
                Globals.mainForm.ibdTuningLinearD.Doublevalue = inifile.ReadDoubleValue(SECTION_SETTINGS, KEY_TUNING_LINEAR_D);
                Globals.mainForm.ibdTuningRotaryP.Doublevalue = inifile.ReadDoubleValue(SECTION_SETTINGS, KEY_TUNING_ROTARY_P);
                Globals.mainForm.ibdTuningRotaryI.Doublevalue = inifile.ReadDoubleValue(SECTION_SETTINGS, KEY_TUNING_ROTARY_I);
                Globals.mainForm.ibdTuningRotaryD.Doublevalue = inifile.ReadDoubleValue(SECTION_SETTINGS, KEY_TUNING_ROTARY_D);
                Globals.mainForm.ibdSpeed.Doublevalue = inifile.ReadDoubleValue(SECTION_SETTINGS, KEY_SPEED);
                Globals.mainForm.ibiRotarySensitivity.Intvalue = inifile.ReadIntValue(SECTION_SETTINGS, KEY_SENSITIVITY);
                
                Globals.mainForm.txtTeachedSqLinear.Text = inifile.ReadValue(SECTION_SETTINGS, KEY_TEACHED_SQ_LINEAR);
                Globals.mainForm.txtTeachedSqLinearHome.Text = inifile.ReadValue(SECTION_SETTINGS, KEY_TEACHED_SQ_LINEAR_HOME);
                Globals.mainForm.txtTeachedHeight.Text = inifile.ReadValue(SECTION_SETTINGS, KEY_TEACHED_HEIGHT);
                
            }
            catch
            {
                MessageBox.Show("Error reading settings file " + path , "File I/O error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }


        public void Write(string path)
        {
            IniFile inifile = new IniFile(path);
            try
            {
                inifile.WriteValue(SECTION_SETTINGS, KEY_ACTUATOR, Globals.mainForm.cmbActuator.SelectedItem.ToString());
                inifile.WriteValue(SECTION_SETTINGS, KEY_THREAD_DIRECTION, Globals.mainForm.cmbThreadDirection.SelectedItem.ToString());


                inifile.WriteValue(SECTION_SETTINGS, KEY_NUMBER_OF_THREAD, Globals.mainForm.ibdNumofThreads.Stringvalue);
                inifile.WriteValue(SECTION_SETTINGS, KEY_CAP_TYPE, Globals.mainForm.cmbCappingType.SelectedItem.ToString());  
                inifile.WriteValue(SECTION_SETTINGS, KEY_PARK_POSITION, Globals.mainForm.ibdPositionClosetoPickCap.Stringvalue);
                inifile.WriteValue(SECTION_SETTINGS, KEY_LIN_DIST_MIN, Globals.mainForm.ibdLinearDistanceMin.Stringvalue);
                inifile.WriteValue(SECTION_SETTINGS, KEY_ROT_DIST_MAX, Globals.mainForm.ibdRotaryDistanceMax.Stringvalue);
                inifile.WriteValue(SECTION_SETTINGS, KEY_ROT_DIST_MIN, Globals.mainForm.ibdRotaryDistanceMin.Stringvalue);
                inifile.WriteValue(SECTION_SETTINGS, KEY_LIN_DIST_MAX, Globals.mainForm.ibdLinearDistanceMax.Stringvalue);
                inifile.WriteValue(SECTION_SETTINGS, KEY_SPEED_TO_PICK_CAP, Globals.mainForm.ibdSpeedToPickCap.Stringvalue);
                inifile.WriteValue(SECTION_SETTINGS, KEY_RETRACT_DIST, Globals.mainForm.ibdRetractDistance.Stringvalue);


                inifile.WriteValue(SECTION_SETTINGS, KEY_POSITION_CLOSE_TO_PART, Globals.mainForm.ibdPositonCloseToContainer.Stringvalue);
                inifile.WriteValue(SECTION_SETTINGS, KEY_CHECK_LANDED_HEIGHT, Globals.mainForm.chkCheckLandedHeight.Checked.ToString());
                inifile.WriteValue(SECTION_SETTINGS, KEY_LANDED_HEIGHT_MIN, Globals.mainForm.ibdLandedHeightMin.Stringvalue);
                inifile.WriteValue(SECTION_SETTINGS, KEY_LANDED_HEIGHT_MAX, Globals.mainForm.ibdLandedHeightMax.Stringvalue);



                inifile.WriteValue(SECTION_SETTINGS, KEY_ROTATE_IN_REVERSE, Globals.mainForm.chkRotateReverse.Checked.ToString());
                inifile.WriteValue(SECTION_SETTINGS, KEY_PERMISABLE_TORQUE, Globals.mainForm.ibdMaxTorque.Stringvalue);
                inifile.WriteValue(SECTION_SETTINGS, KEY_TUNING_LINEAR_P, Globals.mainForm.ibdTuningLinearP.Stringvalue);
                inifile.WriteValue(SECTION_SETTINGS, KEY_TUNING_LINEAR_I, Globals.mainForm.ibdTuningLinearI.Stringvalue);
                inifile.WriteValue(SECTION_SETTINGS, KEY_TUNING_LINEAR_D, Globals.mainForm.ibdTuningLinearD.Stringvalue);
                inifile.WriteValue(SECTION_SETTINGS, KEY_TUNING_ROTARY_P, Globals.mainForm.ibdTuningRotaryP.Stringvalue);
                inifile.WriteValue(SECTION_SETTINGS, KEY_TUNING_ROTARY_I, Globals.mainForm.ibdTuningRotaryI.Stringvalue);
                inifile.WriteValue(SECTION_SETTINGS, KEY_TUNING_ROTARY_D, Globals.mainForm.ibdTuningRotaryD.Stringvalue);
                inifile.WriteValue(SECTION_SETTINGS, KEY_SPEED, Globals.mainForm.ibdSpeed.Stringvalue);
                inifile.WriteValue(SECTION_SETTINGS, KEY_SENSITIVITY, Globals.mainForm.ibiRotarySensitivity.Stringvalue);

                inifile.WriteValue(SECTION_SETTINGS, KEY_TEACHED_SQ_LINEAR, Globals.mainForm.txtTeachedSqLinear.Text);
                inifile.WriteValue(SECTION_SETTINGS, KEY_TEACHED_SQ_LINEAR_HOME, Globals.mainForm.txtTeachedSqLinearHome.Text);
                inifile.WriteValue(SECTION_SETTINGS, KEY_TEACHED_HEIGHT, Globals.mainForm.txtTeachedHeight.Text);

            }
            catch
            {
                MessageBox.Show("Error writing settings file " + path , "File I/O error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
