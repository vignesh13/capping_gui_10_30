﻿//#define SHOW_LISTBOX1
//#define SHOW_TEXTBOX2

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.IO.Ports;
using System.Diagnostics;


namespace SMAC_Capping_Control_Center
{

    public partial class MainForm : Form
    {

        private const string STR_MM = "mm";
        private const string STR_TPI = "TPI";
        public string ActuatorFoldername = "";

        private bool passwordRequired = true;  // TODO REMOVE IN FINAL VERSION

        public const string programName = "SMAC Capping Control Center 0.18_TEMP";
        private List<string> header = new List<string>();

        private delegate void SetTextDeleg(string text);
        public Serial port = new Serial();

        private string fileName = "";

        public bool NoActuatorfilesFound = false;
        private bool ignoreResponse = false;        // Ignores responses from the controller that start with !
        private bool checkFirst = false;            // True if the next character must be checked for !
        private string lastCommand = "";
        
        public InputBoxDouble ibdPositonCloseToContainer = null;
        public InputBoxDouble ibdLandedHeightMin = null;
        public InputBoxDouble ibdLandedHeightMax = null;
        public InputBoxDouble ibdThreadDepth = null;
        public InputBoxDouble ibdMaxTorque = null;
        public InputBoxDouble ibdTeachedSqLinear = null;

        public InputBoxDouble ibdTuningLinearP = null;
        public InputBoxDouble ibdTuningLinearI = null;
        public InputBoxDouble ibdTuningLinearD = null;

        public InputBoxDouble ibdTuningRotaryP = null;
        public InputBoxDouble ibdTuningRotaryI = null;
        public InputBoxDouble ibdTuningRotaryD = null;

        public InputBoxDouble ibdSpeed = null;
        public InputBoxInt    ibiRotarySensitivity = null;

        public InputBoxDouble ibdPositionClosetoPickCap = null;
        public InputBoxDouble ibdNumofThreads = null;
        public InputBoxDouble ibdSpeedToPickCap = null;
        public InputBoxDouble ibdRetractDistance = null;
        public InputBoxDouble ibdReleaseDwellTime = null;
        public InputBoxDouble ibdLinearDistanceMin = null;
        public InputBoxDouble ibdLinearDistanceMax = null;
        public InputBoxDouble ibdRotaryDistanceMin = null;
        public InputBoxDouble ibdRotaryDistanceMax = null;

        private CappingProgram masterProgram = new CappingProgram();

        public Actuator actuator = new Actuator();

        private ValueChecker checkerFile = new ValueChecker();

        private ValueChecker checkerLoad = new ValueChecker();

        public MessageBoxCaptions mbCaptions = new MessageBoxCaptions();


        public MainForm()
        {
            InitializeComponent();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            try
            {
                foreach (string s in Directory.GetFiles(Environment.CurrentDirectory + "\\Actuators", "*.ini*", SearchOption.AllDirectories))
                {
                    cmbActuator.Items.Add(Path.GetFileNameWithoutExtension(s).Replace('!', ':'));
                }
            }
            catch
            {
                MessageBox.Show("No actuator definitions found." + Environment.NewLine +
                                "Re-install the application and then try again.",
                                "Fatal error",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
                Application.Exit();
            }
            // searches each file in the actuator config directory until chosen one is found
            /*ActuatorFoldername = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFilesX86); //12/13 :: VIGNESH:: Used to create a actuator folder in the setup directory itself and load ini files
            ActuatorFoldername = Path.Combine(ActuatorFoldername, "SMAC");
            ActuatorFoldername = Path.Combine(ActuatorFoldername, "SMAC Capping Control Center");
            ActuatorFoldername = Path.Combine(ActuatorFoldername, "Actuators");
            cmbActuator.Items.Clear();
            if (!Directory.Exists(ActuatorFoldername))
            {
                if (DialogResult.OK == MessageBox.Show("Actuators directory missing!! Do you like to create directory: " + ActuatorFoldername + Environment.NewLine +
                        "Hit OK to create this folder", "Confirm Actuator folder creation", MessageBoxButtons.OKCancel, MessageBoxIcon.Asterisk))
                {
                    try
                    {
                        Directory.CreateDirectory(ActuatorFoldername);
                    }
                    catch (Exception excep)
                    {
                        MessageBox.Show("Can not create folder " + ActuatorFoldername + Environment.NewLine +
                                        "Reason: " + excep.ToString());
                        Environment.Exit(0);
                    }
                }
                else
                {
                    Environment.Exit(0);
                }
            }


            int actuator_count = 0;

            try
            {
                foreach (string s in Directory.GetFiles(ActuatorFoldername, "*.ini*", SearchOption.AllDirectories))
                {
                    cmbActuator.Items.Add(Path.GetFileNameWithoutExtension(s).Replace('!', ':'));
                    actuator_count++;
                }
            }
            catch
            {
                MessageBox.Show("No actuator definitions found." + Environment.NewLine +
                                "Re-install the application and then try again.",
                                "Fatal error",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
                Environment.Exit(0);
            }

            if (actuator_count == 0)
            {
                NoActuatorfilesFound = true;
                MessageBox.Show("No actuator definitions found." + Environment.NewLine +
                                "Contact SMAC or Place actuator definition files in directory: " + ActuatorFoldername,
                                "Fatal error",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
                Environment.Exit(0);
            }*/


#if SHOW_LISTBOX1
            listBox1.Visible = true;
#else
            listBox1.Visible = false;
#endif

#if SHOW_TEXTBOX2
            textBox2.Visible = true;
#else
            textBox2.Visible = false;
#endif

            //
            // the code below is necessary to make the background color of the height label transparent:
            //
            lblLandingHeight.Parent = pictureBoxRun;
            lblLandingHeight.Location = new Point(420, 180);
            lblLandingHeight.BackColor = Color.Transparent;
            lblLandingHeight.Text = "";


            lblLandingHeight2.Parent = pictureBoxSetup;
            lblLandingHeight2.Location = new Point(325, 192);
            lblLandingHeight2.BackColor = Color.Transparent;
            lblLandingHeight2.Text = "";


            lblThreadDepthMeasured.Parent = pictureBoxRun;
            lblThreadDepthMeasured.Location = new Point(420, 260);
            lblThreadDepthMeasured.BackColor = Color.Transparent;
            lblThreadDepthMeasured.Text = "";

            lblThreadDepthMeasured2.Parent = pictureBoxSetup;
            lblThreadDepthMeasured2.Location = new Point(325, 100);
            lblThreadDepthMeasured2.BackColor = Color.Transparent;
            lblThreadDepthMeasured2.Text = "";


            

            lblLinearDistance_Run.Text = "";
            lblLinearDistance_Setup.Text = "";
            lblRotaryRevs_Run.Text = "";
            lblRotaryRevs_Setup.Text = "";

            chkCheckLandedHeight.Checked = true;
            chkLinDistTravel.Checked = true;
            chkRotDistTravel.Checked = true;
            chkRotateReverse.Checked = true;

            Globals.mainForm = this;

            //            txtTerminal.Text = DateTime.Now.ToString();
            //            txtTerminal.AppendText(Environment.NewLine +"Appended");
            //            MessageBox.Show(Environment.CurrentDirectory);
            Text = programName;
            
            
            //            MessageBox.Show("Actuators " + cmbActuator.Items.Count.ToString());
            cmbActuator.SelectedIndex = 0;
            

            cmbThreadDirection.Items.Clear();
            cmbThreadDirection.Items.Add(Globals.STR_THREAD_DIRECTION_RIGHT_HANDED);
            cmbThreadDirection.Items.Add(Globals.STR_THREAD_DIRECTION_LEFT_HANDED);
            cmbThreadDirection.SelectedIndex = 0;
            cmbCappingType.SelectedIndex = 0;
            AutoConnectComport();
#if false
                string s = "";
                s += (mbCaptions.Abort + Environment.NewLine);
                s += (mbCaptions.Cancel + Environment.NewLine);
                s += (mbCaptions.Close + Environment.NewLine);
                s += (mbCaptions.Cont + Environment.NewLine);
                s += (mbCaptions.Help + Environment.NewLine);
                s += (mbCaptions.Ignore + Environment.NewLine);
                s += (mbCaptions.No + Environment.NewLine);
                s += (mbCaptions.Ok + Environment.NewLine);
                s += (mbCaptions.Retry + Environment.NewLine);
                s += (mbCaptions.TryAgain + Environment.NewLine);
                s += (mbCaptions.Yes + Environment.NewLine);
                MessageBox.Show(s);
#endif
           

            ibdMaxTorque = new InputBoxDouble(txtMaxTorque, 0.5, 100, 100, "Max premisable torque");

            ibdTuningLinearP = new InputBoxDouble(txtTuningLinearP, 10, 1000, 100, "Linear tuning P factor");
            ibdTuningLinearI = new InputBoxDouble(txtTuningLinearI, 10, 1000, 100, "Linear tuning I factor");
            ibdTuningLinearD = new InputBoxDouble(txtTuningLinearD, 10, 1000, 100, "Linear tuning D factor");

            ibdTuningRotaryP = new InputBoxDouble(txtTuningRotaryP, 10, 1000, 100, "Rotary tuning P factor");
            ibdTuningRotaryI = new InputBoxDouble(txtTuningRotaryI, 10, 1000, 100, "Rotary tuning I factor");
            ibdTuningRotaryD = new InputBoxDouble(txtTuningRotaryD, 10, 1000, 100, "Rotary tuning D factor");
            ibdPositionClosetoPickCap = new InputBoxDouble(txtPositionCloseToPickCap, 0, 500, 0, "Park Position" ); 

            ibdSpeed = new InputBoxDouble(txtSpeed, 10, 100, 50, "Speed");
            ibiRotarySensitivity = new InputBoxInt(txtRotarySensitivity, 10, 1250, 250, "Sensitivity");
        



            ibdSpeedToPickCap = new InputBoxDouble(txtSpeedtoPickCap, 0, 100, 50, "Speed to Pick Cap");
            ibdNumofThreads = new InputBoxDouble(txtNumofThreads, 1, 100, 1, "Number of Threads");
            
           
            ibdRotaryDistanceMin = new InputBoxDouble(txtMinRotDistTravel, 0, 100, 0, "Min Limit [revs]");
            ibdRotaryDistanceMax = new InputBoxDouble(txtMaxRotDistTravel, 0, 100, 5, "Max Limit [revs]");

            load_values_from_actuator_ini();

            chart1.Series[0].Points.Clear();
            for (int i = -100; i <= 100; i += 5)
            {
                chart1.Series[0].Points.AddXY(i, i);
            }
//            tabControl1.SelectedIndex = 1;

            fileName = Properties.Settings.Default.File;

            if ((fileName != "") && File.Exists(fileName))
            {
                //                MessageBox.Show("Reading Settings from " + fileName);
                LoadSettingsFromFile(fileName);
            }
            else
            {
                fileName = "";
                checkerFile.Load();

            }
            EnableInputFields();
            PopulateTooltips();
            ShowSettupButtons();
        }

        public void load_values_from_actuator_ini()
        {
            ibdPositonCloseToContainer = new InputBoxDouble(txtPositionCloseToPart, 0, (int)(actuator.LinearStroke() - 5.0), (int)(0), "Position Close To Container [mm]");
           

            ibdLandedHeightMin = new InputBoxDouble(txtLandedHeightMin, 0, (int)(actuator.LinearStroke()), (int)(actuator.LinearStroke() / 2 + 1), "Landed Height Minimum [mm]");
            txtLandedHeightMin.Text = ((int)(actuator.LinearStroke() / 2 + 1)).ToString();

            ibdLandedHeightMax = new InputBoxDouble(txtLandedHeightMax, 0, (int)(actuator.LinearStroke()), (int)(actuator.LinearStroke() / 2 + 2), "Landed Height Maximum [mm]");
            txtLandedHeightMax.Text = ((int)(actuator.LinearStroke() / 2 + 2)).ToString();

            ibdPositionClosetoPickCap = new InputBoxDouble(txtPositionCloseToPickCap, 0, (int)(actuator.LinearStroke()) - 5, 0, "Park Position [mm]");
           

            ibdRetractDistance = new InputBoxDouble(txtRetractDistance, 0, (int)(actuator.LinearStroke())-5, (int)(0), "Retract Distance [mm]");
            

            ibdLinearDistanceMin = new InputBoxDouble(txtMinLinTravel, 0, (int)(actuator.LinearStroke()), (int)(0), "Min Limit [mm]");
            

            ibdLinearDistanceMax = new InputBoxDouble(txtMaxLinTravel, 0, (int)(actuator.LinearStroke()), (int)(actuator.LinearStroke() - 5.0), "Max Limit [mm]");
           

            
        }

        private void AutoConnectComport()
        {
            FillPortnames();
            switch (cmbPort.Items.Count)
            {
                case 0:
                    break;
                case 1:
                    cmbPort.SelectedIndex = 0;
                    Opencomport();
                    break;
                default: // More ports: Open the last selected one or let the user select one
                    for (int i = 0; i < cmbPort.Items.Count; i++)
                    {
                        if (Properties.Settings.Default.Port == cmbPort.Items[i].ToString())
                        {
                            cmbPort.SelectedIndex = i;
                            Opencomport();
                            return;
                        }
                    }
                    cmbPort.SelectedIndex = 0;
                    MessageBox.Show("Multiple COM ports found, can not decide which one to use." + Environment.NewLine + Environment.NewLine +
                                    "Please select a COM Port first and then hit the Connect button", "Automatic port selection not possible", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    break;
            }
            SetButtons(port.IsOpen());
        }

        public void DefineToolTip(ToolTip tooltip, Control control, String text)
        {
            tooltip.ShowAlways = true;
            tooltip.UseFading = true;
            tooltip.UseAnimation = true;
            tooltip.ShowAlways = true;
            int autoPopdelay = 50 * text.Length;
            if (autoPopdelay < 5000)
            {
                autoPopdelay = 5000;
            }
            if (autoPopdelay > 30000)
            {
                autoPopdelay = 30000;
            }
            tooltip.AutoPopDelay = autoPopdelay;
            tooltip.InitialDelay = 500;
            tooltip.IsBalloon = false;
            tooltip.SetToolTip(control, text);
        }

        public void PopulateTooltips()
        {
            const string tiptextActuator =
                "Select the right actuator here";
            DefineToolTip(new ToolTip(), cmbActuator, tiptextActuator);
            DefineToolTip(new ToolTip(), lblActuator, tiptextActuator);

            const string tiptextThreadDirection = 
                "Select normal (Right handed) or reverse (Left handed) thread";
            DefineToolTip(new ToolTip(), cmbThreadDirection, tiptextThreadDirection);
            DefineToolTip(new ToolTip(), lblThreadDirection, tiptextThreadDirection);
            
            const string tiptextPort = 
                "Select serial communication port";
            DefineToolTip(new ToolTip(), cmbPort, tiptextPort);
            DefineToolTip(new ToolTip(), lblPort, tiptextPort);

            const string tiptextPositionCloseToPart = 
                "Rapid traverse position close to the part surface.\n" +
                "Please note: This is pre-calculated in the \"Teach\" process, but can be adjusted if desired.";
            DefineToolTip(new ToolTip(), txtPositionCloseToPart, tiptextPositionCloseToPart);
            DefineToolTip(new ToolTip(), lblPositionCloseToPart, tiptextPositionCloseToPart);

            DefineToolTip(new ToolTip(), btnTeach, 
                "Use this once the Actuator, Thread Direction, and Capping Type selections have been made and the program has been saved into the controller.\n" + 
                "This will teach the actuator where the height of the part is at as well as the linear force settings required.\n" +
                "This should be used in conjunction with a \"Good Part\" properly fixture in front of the Actuator.\n" +
                "This will allow the actuator to learn the forces & locations at various points throughout the path of the capping process.\n"+
                "These values will also be used to preset some suggested starting values( Position close to part, Landed Limits,etc) for other fields in the GUI.\n"+  
                "Another \"Save to Controller\" is required after the teach is completed in order to update these settings.");

            const string tiptextPartLocationMin =
                "This is the minimum allowable cap to container starting height.\n" +
                "This limit is pre-calculated in the “Teach” process, but can be adjusted if desired";
            DefineToolTip(new ToolTip(), txtLandedHeightMin, tiptextPartLocationMin);
            DefineToolTip(new ToolTip(), lblLandedHeightMin, tiptextPartLocationMin);

            const string tiptextPartLocationMax =
                "This is the maximum allowable cap to container starting height.\n" +
                "This limit is pre-calculated in the “Teach” process, but can be adjusted if desired";
            DefineToolTip(new ToolTip(), txtLandedHeightMax, tiptextPartLocationMax);
            DefineToolTip(new ToolTip(), lblLandedHeightMax, tiptextPartLocationMax);
            
            const string tiptextMaxTorque = 
                "Enter the maximum percentage of Permissible Torque for the SMAC to use during the thread in/on to the thread.\n" +
                "Note 1: The SMAC Thread Check Center handles this value and calculates based on it so that the outbound torque\n" +
                "is always greater than the inbound torque to aid in the prevention of becoming bound onto a tight part.\n" +
                "Note 2: This setting in combination with the Sensitivity and Speed settings will affect the functionality of the SMAC.";
            DefineToolTip(new ToolTip(), txtMaxTorque, tiptextMaxTorque);
            DefineToolTip(new ToolTip(), lblMaxTorque, tiptextMaxTorque);

            const string tiptextRotarySensitivity =
                "Enter the sensitivity setting.\n" +
                "This should be 5 to 20 counts over what is seen with a good part in the Rotary Position Error vs. Linear Position.\n" +
                "Note: If this value is set to high there is a risk of getting caught/ wedged in or on a thread that is too tight.\n" +
                "If set to low, the SMAC may detect and fail the thread on small debris or slight mis-alignment that causes added friction.";
            DefineToolTip(new ToolTip(), txtRotarySensitivity, tiptextRotarySensitivity);
            DefineToolTip(new ToolTip(), lblRotarySensitivity, tiptextRotarySensitivity);

            {
                const string tiptextLinearPID = 
                    "Use this to adjust the PID settings for the linear axis only if needed (Example: Excessive tooling weight)";
                DefineToolTip(new ToolTip(), gbxTuningLinear, tiptextLinearPID);
                DefineToolTip(new ToolTip(), txtTuningLinearP, tiptextLinearPID);
                DefineToolTip(new ToolTip(), txtTuningLinearI, tiptextLinearPID);
                DefineToolTip(new ToolTip(), txtTuningLinearD, tiptextLinearPID);
                DefineToolTip(new ToolTip(), lblTuningLinearP, tiptextLinearPID);
                DefineToolTip(new ToolTip(), lblTuningLinearI, tiptextLinearPID);
                DefineToolTip(new ToolTip(), lblTuningLinearD, tiptextLinearPID);
                DefineToolTip(new ToolTip(), lblTuningLinearPUnits, tiptextLinearPID);
                DefineToolTip(new ToolTip(), lblTuningLinearIUnits, tiptextLinearPID);
                DefineToolTip(new ToolTip(), lblTuningLinearDUnits, tiptextLinearPID);
            }
            {
                const string tiptextRotaryPID = 
                    "Use this to adjust the PID settings for the rotary axis only if needed (Example: Excessive tooling weight)";
                DefineToolTip(new ToolTip(), gbxTuningRotary, tiptextRotaryPID);
                DefineToolTip(new ToolTip(), txtTuningRotaryP, tiptextRotaryPID);
                DefineToolTip(new ToolTip(), txtTuningRotaryI, tiptextRotaryPID);
                DefineToolTip(new ToolTip(), txtTuningRotaryD, tiptextRotaryPID);
                DefineToolTip(new ToolTip(), lblTuningRotaryP, tiptextRotaryPID);
                DefineToolTip(new ToolTip(), lblTuningRotaryI, tiptextRotaryPID);
                DefineToolTip(new ToolTip(), lblTuningRotaryD, tiptextRotaryPID);
                DefineToolTip(new ToolTip(), lblTuningRotaryPUnits, tiptextRotaryPID);
                DefineToolTip(new ToolTip(), lblTuningRotaryIUnits, tiptextRotaryPID);
                DefineToolTip(new ToolTip(), lblTuningRotaryDUnits, tiptextRotaryPID);
            }

            {
                const string tiptextTeacResult = 
                    "This is information that was found during the “Teach” routine";
                DefineToolTip(new ToolTip(), gbxTeachResult, tiptextTeacResult);
                DefineToolTip(new ToolTip(), txtTeachedSqLinearHome, tiptextTeacResult);
                DefineToolTip(new ToolTip(), lblTeachedSqLinearHome, tiptextTeacResult);
                DefineToolTip(new ToolTip(), txtTeachedSqLinear, tiptextTeacResult);
                DefineToolTip(new ToolTip(), lblTeachedSqLinear, tiptextTeacResult);
                DefineToolTip(new ToolTip(), txtTeachedHeight, tiptextTeacResult);
                DefineToolTip(new ToolTip(), lblTeachedHeight, tiptextTeacResult);
                DefineToolTip(new ToolTip(), lblTeachedHeightUnits, tiptextTeacResult);
            }

            const string tiptextSpeed = 
                "Enter percentage of desired speed.\n" +
                "Lower settings will provide better results.\n" +
                "This setting in combination with the Sensitivity and Permissible Torque settings will affect the functionality of the SMAC.";
            DefineToolTip(new ToolTip(), txtSpeed, tiptextSpeed);
            DefineToolTip(new ToolTip(), lblSpeed, tiptextSpeed);

            DefineToolTip(new ToolTip(), btnActuatorInfo,
                "Selected actuator detailed information.");
            DefineToolTip(new ToolTip(), btnLoadSettingsFromFile, 
                "Use this to load GUI settings from file.");
            DefineToolTip(new ToolTip(), btnSaveSettingsToFile, 
                "Save GUI settings to computer");
            DefineToolTip(new ToolTip(), btnSaveProgramToFile, 
                "Save compiled LAC-25 program to computer");
            DefineToolTip(new ToolTip(), btnSaveToController, 
                "Save compiled LAC-25 program to controller");
            DefineToolTip(new ToolTip(), btnSingleCycle, 
                "Use this to run a single cycle.\n" +
                "The \"Teach\" routine should have been run at least once initially, prior to the use of this button to learn the height of the part.");
            DefineToolTip(new ToolTip(), btnRun, 
                "Use this to run the program that has been downloaded into the controller");
            DefineToolTip(new ToolTip(), btnContactInfo, 
                "Use this to get SMAC contact information");
            DefineToolTip(new ToolTip(), btnHelpDocument, 
                "Use this to open the help document.\nThis document will be opened in a separate window");
            DefineToolTip(new ToolTip(), btnStop, 
                "Use this to stop the program in the controller and to deactivate the actuator");
            DefineToolTip(new ToolTip(), btnUndo, 
                "This is the tooltip for the undo button");

            DefineToolTip(new ToolTip(), chkCheckLandedHeight, 
                "Select this option for verifying landed height.\n" +
                "The limits are pre-calculated in the \"Teach\" process, but can be adjusted if desired. ");
            DefineToolTip(new ToolTip(), chkRotateReverse, 
                "Select this option for locating the lead in of the first thread.\n" +
                "Please note results will vary depending upon lead in and fitment of two mating threads (cap & container threads)");
            DefineToolTip(new ToolTip(), chkShowAll, 
                "Select this to display all of the data being sent out of the SMAC controller.");

//            DefineToolTip(new ToolTip(), txtTerminal, 
//                "This is the tooltip for the terminal window");

            DefineToolTip(new ToolTip(), chart1, 
                "This is a display of the Rotary Position Error vs. the Linear Position during the inbound testing.\n" +
                "Note 1. : For best performance of the SMAC the Rotary Position Error should be less than 100.\n" +
                "Note 2. : Set the Rotary Sensitivity on the Tuning screen slightly above (+5 to 20 counts) what is seen on a good part.");

            DefineToolTip(new ToolTip(), pictureBoxRun, 
                "General information for an internal thread check application");

            DefineToolTip(new ToolTip(), pictureBoxSetup, 
                "General information for an internal thread check application");
            {
                const string tiptextThreadPlugGauge =
                    "Thread plug or ring gauge coupled to SMAC shaft.\n" +
                    "•\tSMAC recommends some type of floating coupler device be applied to allow for slight mis-alignments that are seen from part to part and fixturing variation.\n" +
                    "\tNote that this floating coupler may affect accuracy of thread clearance measurements.\n" +
                    "•\tSMAC recommends cleanout grooves in the plug or ring gauge for better performance.";

                DefineToolTip(new ToolTip(), lblPluginfoRun, tiptextThreadPlugGauge);
                DefineToolTip(new ToolTip(), lblPluginfoSetup, tiptextThreadPlugGauge);
            }
            

            {
                const string tiptextThreadStart =
                    "Distance from the SMAC home position to the Soft-Land© position of the first thread.\n" +
                    "This can be used to set limits for checking counter bore height or part height.";
                DefineToolTip(new ToolTip(), lblThreadStartRun, tiptextThreadStart);
                DefineToolTip(new ToolTip(), lblThreadStartSetup, tiptextThreadStart);
                DefineToolTip(new ToolTip(), lblLandingHeight, tiptextThreadStart);
                DefineToolTip(new ToolTip(), lblLandingHeight2, tiptextThreadStart);
            }
            {
                const string tiptextThreadDepthMeasured =
                    "This is the actual depth that the SMAC traveled into the thread from the start location.";
                DefineToolTip(new ToolTip(), lblRotaryDepthRun, tiptextThreadDepthMeasured);
                DefineToolTip(new ToolTip(), lblRotDepthSetup, tiptextThreadDepthMeasured);
                DefineToolTip(new ToolTip(), lblThreadDepthMeasured, tiptextThreadDepthMeasured);
                DefineToolTip(new ToolTip(), lblThreadDepthMeasured2, tiptextThreadDepthMeasured);
            }

            {
                const string tiptextPositionCloseToPickCap =
                    "This is the rapid travers position to move the acutator fast to a positon close to the part.";
                DefineToolTip(new ToolTip(), lblPositionCloseToPickCap, tiptextPositionCloseToPickCap);
                DefineToolTip(new ToolTip(), txtPositionCloseToPickCap, tiptextPositionCloseToPickCap);
            }

            {
                const string tiptextSpeedToPickCap =
                    "Adjust the speed for the approach to the pick up position as well as the speed for the retract away from the pick up position. ";
                DefineToolTip(new ToolTip(), lblSpeedToPickCap, tiptextSpeedToPickCap);
                DefineToolTip(new ToolTip(), txtSpeedtoPickCap, tiptextSpeedToPickCap);
            }

            {
                const string tiptextRetractDistance =
                    "This is a position that the SMAC will retract to in order to clear the cap infeed tooling, this can also can be used as a \"new\" park position when tooling and set up allows. ";
                DefineToolTip(new ToolTip(), lblRetractDistance, tiptextRetractDistance);
                DefineToolTip(new ToolTip(), txtRetractDistance, tiptextRetractDistance);
            }


            {
                const string tiptextTeachPickupCap = " ";

                DefineToolTip(new ToolTip(), btnTeachPickCap, tiptextTeachPickupCap);
            }
            {
                const string tiptextCheckLinearDistanceTravelled =
                    "This is a QC inspection to check the linear distance traveled was within the Min & Max Linear Limits.\n" +
                    "If the \"Rotate In Reverse To Locate First Thread\" was used/ applied, then the Linear distance will be in relation to that lead in thread height";
                
                DefineToolTip(new ToolTip(), chkLinDistTravel, tiptextCheckLinearDistanceTravelled);
                
            }
            {
                const string tiptextLinearDistanceTravelMin =
                    "Minimum Linear Distance in mm that the cap should have achieved to be considered to be properly seated/ installed.";
                DefineToolTip(new ToolTip(), lblMinLinTravel, tiptextLinearDistanceTravelMin);
                DefineToolTip(new ToolTip(), txtMinLinTravel, tiptextLinearDistanceTravelMin);
            }
            {
                const string tiptextLinearDistanceTravelMax =
                    "Maximum Linear Distance in mm that the cap should have achieved to be considered to be properly seated/ installed.";
                DefineToolTip(new ToolTip(), lblMaxLinTravel, tiptextLinearDistanceTravelMax);
                DefineToolTip(new ToolTip(), txtMaxLinTravel, tiptextLinearDistanceTravelMax);
            }
            {
                const string tiptextCheckRotaryDistanceTravelled =
                    "This is a QC inspection to check the rotary distance traveled was within the Min & Max Rotary Limits." +
                    "If the \"Rotate In Reverse To Locate First Thread\" was used/ applied, then the Rotary distance will be in relation to that lead in thread height.";
                DefineToolTip(new ToolTip(), chkRotDistTravel, tiptextCheckRotaryDistanceTravelled);

            }
            {
                const string tiptextRotaryDistanceTravelMin =
                    "Minimium Rotary Distance in Revoultions that the cap should have achieved to be considered to be properly seated/ installed.";
                DefineToolTip(new ToolTip(), lblMinRotDistTravel, tiptextRotaryDistanceTravelMin);
                DefineToolTip(new ToolTip(), txtMinRotDistTravel, tiptextRotaryDistanceTravelMin);
            }
            {
                const string tiptextRotaryDistanceTravelMax =
                    "Maximium Rotary Distance in Revoultions that the cap should have achieved to be considered to be properly seated/ installed.";
                DefineToolTip(new ToolTip(), lblMaxRotDistTravel, tiptextRotaryDistanceTravelMax);
                DefineToolTip(new ToolTip(), txtMaxRotDistTravel, tiptextRotaryDistanceTravelMax);
            }
            {
                const string tiptextCappingType =
                    "Select the type of capping operation. Options are: \n" +
                    "1) Pick & Install of Cap. Extend to Pick, Verify Pick Height, Grip Cap, Retract To Specified Position, Extend to Container ,Verify Landed Height (optional), Rotate in Rev. to locate first thread (optional),  Thread Forward/Install Cap To Set Torque While Monitoring For No Threads- Misaligned Threads- Cross Threading , Verify Linear Distance Traveled As Well As Rotary Distance Traveled.\n" + 
                    "2) Pre-Placed Cap Instalation.  Extend To Locate Cap, Verify Cap Hieght, Grip Cap, Rotate in Rev. to locate first thread (optional), Thread Forward/Install Cap To Set Torque While Monitoring For No Threads- Misaligned Threads- Cross Threading , Verify Linear Distance Traveled As Well As Rotary Distance Traveled. ";
                DefineToolTip(new ToolTip(), lblCappingType, tiptextCappingType);
                DefineToolTip(new ToolTip(), cmbCappingType, tiptextCappingType);
            }
        }

        private void Title()
        {
            if (fileName == "")
            {
                Text = programName;
            }
            else
            {
                Text = fileName + " - " + programName;
            }
        }

        public void sp_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            if (port.IsOpen())
            {
                BeginInvoke(new SetTextDeleg(si_DataReceived), port.GetAllBytes());
            }
        }

        private void si_DataReceived(string data)
        {
            for (int i = 0; i < data.Length; i++)
            {
#if SHOW_LISTBOX1

                string print = "";
                if ((int)data[i] < 0x20)
                {
                    print = ((int)data[i]).ToString("x2");
                }
                else
                {
                    print = data[i].ToString();
                }
                listBox1.Items.Add(print);
                listBox1.TopIndex = listBox1.Items.Count - 1;
#endif

                if (checkFirst)
                {
                    if (data[i] == '!')
                    {
                        if (chkShowAll.Checked)
                        {
                            ignoreResponse = false;
                        }
                        else
                        {
                            ignoreResponse = true;
                        }
                    }
                    else
                    {
                        ignoreResponse = false;
                    }
                    checkFirst = false;
                }

                switch (data[i])
                {
                    case '\b':      // Backspace
                        //
                        // The backspace handling looks a bit complicated but this method is needed to assure that the cursor
                        // is at the correct position
                        //

                        txtTerminal.AppendText(" ");        // Start with appending a space character to make sure the cursor is at end of line
                        int firstIndex = txtTerminal.GetFirstCharIndexOfCurrentLine();
                        string s = txtTerminal.Text.Substring(firstIndex);
                        txtTerminal.Text = txtTerminal.Text.Remove(firstIndex);
                        switch (s.Length)
                        {
                            case 1:
                                break;
                            default:
                                s = s.Remove(s.Length - 2);
                                break;
                        }
                        txtTerminal.AppendText(s);
                        break;

                    case '\r':      // The CR character terminates a response
                        processControllerResponse();
                        if (!ignoreResponse)
                        {
                            txtTerminal.AppendText(data[i].ToString());
                        }
                        break;

                    case '\n':
                        checkFirst = true;
                        if (!ignoreResponse)
                        {
                            txtTerminal.AppendText(data[i].ToString());
                        }
                        break;

                    default:        // Echo all other characters including CR and LF simply to the output text box
                        lastCommand += data[i].ToString();
                        if (!ignoreResponse)
                        {
                            txtTerminal.AppendText(data[i].ToString());
                        }
                        break;
                }
            }
        }

        private void Plot(int pos, int error)
        {
            chart1.Series[0].Points.AddXY((double)pos * actuator.LinearResoloution() / 1000 , (double) error);    // Plot in mm
        }

        //
        // Controller responses can have the following format:
        //
        // C denotes a character
        // X and Y means a integer number
        //
        // !X,Y     Where X and Y are integer numbers
        //          This is a plot point: X is the linear position in Encoder counts
        //          Y is the rotary error in encoder counts
        // !CX      C is the character that indicates the source, X is the value
        //          The following source characters are defined:
        //          H - Landed height in counts
        //          D - Measured hole depth
        //          P - Result of Push/Pull test in encoder counts
        //          I - Initialisation:
        //
        //          I0 : Clear screeen at startup
        //          I1 : Measurement started: Clear chart and results
        //          I2 : Measurement finished and back at home position
        //
        //          E - Error condition
        //
        // Any other controller response will be ignored
        //

        void processControllerResponse()
        {
            string response = lastCommand.Trim();

            lastCommand = "";

#if SHOW_TEXTBOX2

            textBox2.AppendText(response + "\r\n");
#endif

            int val1 = 0;
            int val2 = 0;
            Color color = Color.Black;



            if (response.Length == 0)           // ignore empty lines
            {
                return;
            }

            if (response[0] != '!')            // This is the command prompt: ignore
            {
                return;
            }

            response = response.Substring(1);     // Delete the !
            response = response.Trim();

            if (response.IndexOf(',') > 0)     // Then this could be a plot command consisting of 2 comma separated integers
            {
                string[] values = response.Split(',');
                if (values.Count() == 2)
                {
                    if (int.TryParse(values[0], out val1) && int.TryParse(values[1], out val2))
                    {
                        Plot(val1, val2);
                        
                        return;
                    }
                }
            }

            if(response[0] == 'L' && response[1] == 'D')
            {
                int.TryParse(response.Substring(2), out val1);
                double linearDistance = millimeters(val1);
                if (chkLinDistTravel.Checked)
                {
                    if ((linearDistance >= ibdLinearDistanceMin.Doublevalue) && (linearDistance <= ibdLinearDistanceMax.Doublevalue))
                    {
                        color = Color.Green;
                    }
                    else
                    {
                        color = Color.Red;
                    }
                }
                lblLinearDistance_Run.ForeColor = lblLinearDistance_Setup.ForeColor = color;
                lblLinearDistance_Run.Text = lblLinearDistance_Setup.Text = linearDistance.ToString("#.###") + " mm";
            }

            else if (response[0] == 'R' && response[1] == 'D')
            {
                int.TryParse(response.Substring(2), out val1);
                double RotaryDistance = revolutions(val1);
                if (chkRotDistTravel.Checked)
                {
                    if ((RotaryDistance >= ibdRotaryDistanceMin.Doublevalue) && (RotaryDistance <= ibdRotaryDistanceMax.Doublevalue))
                    {
                        color = Color.Green;
                    }
                    else
                    {
                        color = Color.Red;
                    }
                }
                lblRotaryRevs_Run.ForeColor = lblRotaryRevs_Setup.ForeColor = color;
                lblRotaryRevs_Run.Text = lblRotaryRevs_Setup.Text = RotaryDistance.ToString("#.###") + " revs";
            }

            else if ((response.Length >= 2) && ("HPEDILQR".IndexOf(response[0]) >= 0) && int.TryParse(response.Substring(1), out val1))
            {
                

                switch (response[0])
                {
                    case 'L':   // Landed height for teach function
                        double landedHeight = millimeters(val1);
                        txtPositionCloseToPart.Text = (landedHeight - 10).ToString();
                        txtLandedHeightMin.Text = (landedHeight - 2).ToString();
                        txtLandedHeightMax.Text = (landedHeight + 2).ToString();
                        ibdPositonCloseToContainer.ReadValue();
                        ibdLandedHeightMin.ReadValue();
                        ibdLandedHeightMax.ReadValue();
                        txtTeachedHeight.Text = Utils.neutralDouble(landedHeight);
                        break;

                    case 'Q':
                        txtTeachedSqLinear.Text = val1.ToString();
                        double TeachedSQLinear = val1;  
                        // This is the last output from the teach function
                        // Show a messagebox to present the teach result.
                        //
                        string message =  "Teach succeded." + Environment.NewLine;
                        message += Environment.NewLine;
                        message += "Measured height = " + txtTeachedHeight.Text + Environment.NewLine;
                        message += "Measured SQ close to part = " + txtTeachedSqLinear.Text + Environment.NewLine;
                        message += "Measured SQ at home position = " + txtTeachedSqLinearHome.Text;
                        MessageBox.Show(message, "Teach result", MessageBoxButtons.OK, MessageBoxIcon.Information);
                       
                        break;

                    case 'R':
                        txtTeachedSqLinearHome.Text = val1.ToString();
                        break;
                    case 'H':
                        double height = millimeters(val1);
                        if (chkCheckLandedHeight.Checked)
                        {
                            if ((height >= ibdLandedHeightMin.Doublevalue) && (height <= ibdLandedHeightMax.Doublevalue))
                            {
                                color = Color.Green;
                            }
                            else
                            {
                                color = Color.Red;
                            }
                        }
                        lblLandingHeight.ForeColor = lblLandingHeight2.ForeColor = color;
                        lblLandingHeight.Text = lblLandingHeight2.Text = height.ToString() + " mm";
                        return;
                    
                    case 'E':
                        return;

                    case 'D':
                        double depth = millimeters(val1);
                        lblThreadDepthMeasured.ForeColor = lblThreadDepthMeasured2.ForeColor = color;
                        lblThreadDepthMeasured.Text = lblThreadDepthMeasured2.Text = depth.ToString() + " mm";;
                        return;
                    case 'I':   // Initialisations:
                        switch (val1)
                        {
                            case 0:
                                txtTerminal.Text = "";
                                chart1.Series[0].Points.Clear();
                                return;
                            case 1:
                                txtTerminal.Text = "";
                                chart1.Series[0].Points.Clear();
                                lblThreadDepthMeasured.Text =
                                lblThreadDepthMeasured2.Text =
                                lblLandingHeight.Text =
                                lblLandingHeight2.Text =
                                lblLinearDistance_Run.Text = lblLinearDistance_Setup.Text = "";                              
                                return;
                            case 2:
                                return;
                            default:
                                break;
                        }
                        break;
                    default:
                        break;
                }
            }
        }
        private void Opencomport()
        {
            FillPortnames();
            if (port.IsOpen())
            {
                port.Close();
            }
            if (cmbPort.SelectedItem != null)
            {
                port.Open(cmbPort.SelectedItem.ToString(), 57600, sp_DataReceived);
                if (!port.IsOpen())
                {
                    cmbPort.SelectedItem = null;
                }
            }
            SetButtons(port.IsOpen());
        }


        private void btnClearcontrolleroutputwindow_Click(object sender, EventArgs e)
        {
            txtTerminal.Clear();
            txtTerminal.Focus();
        }


        private void txtTerminal_KeyPress(object sender, KeyPressEventArgs e)
        {
            //            txtDebug.AppendText("Character " + e.KeyChar + Environment.NewLine);
            if (LoggedIn())
            {
                switch ((int)e.KeyChar)
                {
                    case 1:            // Ctrl-A
                        txtTerminal.SelectAll();
                        break;
                    case 3:             // Ctrl-C
                        Clipboard.SetText(txtTerminal.SelectedText);
                        break;
                    default:
                        port.WriteString(e.KeyChar.ToString());
                        break;
                }
            }
            else
            {
                MessageBox.Show("Typing in terminal window not allowed", "Access denied", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            e.Handled = true;
        }

        //
        // FillPortnames will compare the contents of the port combo box with the actual serial ports
        // that are present in the system.
        //
        // First a check is done if all actual ports are in the combobox. Ports that are not present are added to the combobox.
        // Then a check is done if all combobox ports are present in the system. Missing ports are removed from the combobox.

        private bool FillPortnames()
        {
            List<string> portnames = new List<string>(SerialPort.GetPortNames());
            List<string> cmbItems = new List<string>();
            for (int i = 0; i < cmbPort.Items.Count; i++)
            {
                cmbItems.Add(cmbPort.Items[i].ToString());
            }

            foreach (string s in portnames)
            {
                if (!cmbItems.Contains(s))
                {
                    cmbPort.Items.Add(s);
                    cmbItems.Add(s);
                }
            }

            foreach (string s in cmbItems)
            {
                if (!portnames.Contains(s))
                {
                    cmbPort.Items.Remove(s);
                }
            }

            if (cmbPort.Items.Count == 0)
            {
                MessageBox.Show("There is no serial port detected on your system." + Environment.NewLine + Environment.NewLine +
                    "It will not be possible to connect to a controlller.", "No serial port found", MessageBoxButtons.OK, MessageBoxIcon.Error);
                port.Close();
                SetButtons(false);
            }


            return (cmbPort.Items.Count > 0);
        }


        // check if the user has entered reasonable values that are conform
        // the actuator specification.
        // The index position is 2-4 mm from the full retract position
        // The stroke is defined as full stroke of teh actuator.
        // This function will show a warning message
        // return value is true if the input is ok
        // false if the input is not ok and the user has selected cancel
        // 

        private bool CheckUserInput()
        {
            double maxLinear = actuator.LinearStroke() - 4.0;

            string message = "";
            //if ((ibdPositonCloseToContainer.Doublevalue + ibdThreadDepth.Doublevalue) > maxLinear)
            //{
            //    message = "The sum of position close to part (" + ibdPositonCloseToContainer.Stringvalue + " mm) and thread depth (" + ibdThreadDepth.Stringvalue + " mm)" + Environment.NewLine +
            //              "must be within the stroke range of the actuator (" + Utils.neutralDouble(maxLinear) + " mm)";
            //}
            if ((ibdPositonCloseToContainer.Doublevalue) > maxLinear)
            {
                message = "The Position close to Container (" + ibdPositonCloseToContainer.Stringvalue + " mm)"  + Environment.NewLine +
                          "must be within the stroke range of the actuator (" + Utils.neutralDouble(maxLinear) + " mm)";
            }
            else if (chkCheckLandedHeight.Checked && (ibdLandedHeightMin.Doublevalue < ibdPositonCloseToContainer.Doublevalue))
            {
                message = "The Min value for landed height check (" + ibdLandedHeightMin.Stringvalue + " mm) must be higher than the position close to part (" + ibdPositonCloseToContainer.Stringvalue + " mm)";
            }

            else if (chkCheckLandedHeight.Checked && (ibdLandedHeightMax.Doublevalue < ibdPositonCloseToContainer.Doublevalue))
            {
                message = "The Max value for landed height check (" + ibdLandedHeightMax.Stringvalue + " mm) must be higher than the position close to part (" + ibdPositonCloseToContainer.Stringvalue + " mm)";
            }

            else if (chkCheckLandedHeight.Checked && (ibdLandedHeightMax.Doublevalue < ibdLandedHeightMin.Doublevalue))
            {
                message = "The Max value for landed height check (" + ibdLandedHeightMax.Stringvalue + " mm) must be higher than The Min value for landed height check (" + ibdLandedHeightMin.Stringvalue + " mm)";
            }

            //else if (chkCheckLandedHeight.Checked && ((ibdLandedHeightMax.Doublevalue + ibdThreadDepth.Doublevalue) > maxLinear))
            //{
            //    message = "The sum of Max value for landed height check (" + ibdLandedHeightMax.Stringvalue + " mm) and thread depth (" + ibdThreadDepth.Stringvalue + " mm)" + Environment.NewLine +
            //              "must be within the stroke range of the actuator (" + Utils.neutralDouble(maxLinear) + " mm)";
            //}

            else if (chkCheckLandedHeight.Checked && ((ibdLandedHeightMax.Doublevalue) > maxLinear))
            {
                message = "The Max value for landed height check (" + ibdLandedHeightMax.Stringvalue + " mm)" + Environment.NewLine +
                          "must be within the stroke range of the actuator (" + Utils.neutralDouble(maxLinear) + " mm)";
            }

            //else if (chkCheckThreadClearance.Checked && rbnTop.Checked && ibdThreadDepth.Doublevalue < (4.0 * GetPitch()))
            //{
            //    message = "Checking the thread clearance \"At top\" is not possible with a thread depth that is less than 4 times the pitch" +Environment.NewLine +
            //              "Select the \"At bottom\" option or increase the thread depth to at least " + (4.0 * GetPitch()).ToString() + " mm.";

            //}

            if (message != "")
            {
                return DialogResult.OK == MessageBox.Show(message + Environment.NewLine + Environment.NewLine +
                                          "Ignoring this message may result in a non functioning program in the controller" + Environment.NewLine  + Environment.NewLine +
                                          "Select OK to continue and to ignore this warning." + Environment.NewLine +
                                          "Select Cancel if you want to correct your input first", "User input out of limits", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);
            }
            return true;
        }


        private void btnActuatorinfo_Click(object sender, EventArgs e)
        {
            string strTemp = "";
            foreach (string s in actuator.Info())
            {
                strTemp += (s + Environment.NewLine);
            }

            MessageBox.Show(strTemp, "Actuator info",MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

     

        private void Compile()
        {

            // First create the header
            //
            header.Clear();
            header.Add(";");
            header.Add("; Program automatic created by: " + programName);
            header.Add("; Creation time : " + DateTime.Now.ToString());
            header.Add(";");
            header.Add("; Actuator info:");

            foreach (string s in actuator.Info())
            {
                if (s != "")
                {
                    header.Add("; " + s);
                }
            }
            header.Add(";");

            header.Add("; Thread direction : " + cmbThreadDirection.SelectedItem.ToString());
            header.Add("; Position close to part = " + ibdPositonCloseToContainer.Stringvalue + " mm.");
            if (chkCheckLandedHeight.Checked)
            {
                header.Add("; Check landed height min = " + ibdLandedHeightMin.Stringvalue + " mm.");
                header.Add("; Check landed height max = " + ibdLandedHeightMax.Stringvalue + " mm.");
            }
            else
            {
                header.Add("; Landed height not checked");
            }

            
            if (chkRotateReverse.Checked)
            {
                header.Add("; Rotate in reverse to find first thread is enabled");
            }
            else
            {
                header.Add("; Do not rotate in reverse to find first thread");
            }

            
            header.Add("; Max permissible torque = " + ibdMaxTorque.Stringvalue + " %");

            header.Add(";");
            header.Add("; Linear PID settings : P=" + ibdTuningLinearP.Stringvalue + "%, " +
                                               "I=" + ibdTuningLinearI.Stringvalue + "%, " +
                                               "D=" + ibdTuningLinearD.Stringvalue + "%.");

            header.Add("; Rotary PID settings : P=" + ibdTuningRotaryP.Stringvalue + "%, " +
                                               "I=" + ibdTuningRotaryI.Stringvalue + "%, " +
                                               "D=" + ibdTuningRotaryD.Stringvalue + "%.");

            header.Add("; Speed = " + ibdSpeed.Stringvalue + "%");
            header.Add("; Rotary sensitivity = " + ibiRotarySensitivity.Stringvalue + " counts");

            string strTemp = "";
            foreach (string s in header)
            {
                strTemp += (s + Environment.NewLine);
            }
//            MessageBox.Show(strTemp, "Header");

            actuator.Load(cmbActuator.SelectedItem.ToString());
            masterProgram.variables = actuator.variables;
            masterProgram.ResetTranslation();

            if(masterProgram.variables.GetValue("DEMO")==1)
            {
                masterProgram.DemoModeEnabled();
            }
            

            masterProgram.SetLinearPID(ibdTuningLinearP.Doublevalue, ibdTuningLinearI.Doublevalue, ibdTuningLinearD.Doublevalue);
            masterProgram.SetRotaryPID(ibdTuningRotaryP.Doublevalue, ibdTuningRotaryI.Doublevalue, ibdTuningRotaryD.Doublevalue);
            masterProgram.SetSpeed(ibdSpeed.Doublevalue / 100.0);
            masterProgram.SetFastMoveParameters();

            masterProgram.SetRotarySensitivity(ibiRotarySensitivity.Intvalue);
            masterProgram.SetRotaryResolution((int)actuator.RotaryResoloution());
            masterProgram.SetNumofThreadsCheckValue(ibdNumofThreads.Doublevalue, chkRotateReverse.Checked);
            masterProgram.SetParkPosition(LinearEncoderCounts(ibdPositionClosetoPickCap.Doublevalue));

            masterProgram.SetRotaryDirection(cmbThreadDirection.SelectedItem.ToString());
            masterProgram.SetPositionCloseToContainer(LinearEncoderCounts(ibdPositonCloseToContainer.Doublevalue));
            masterProgram.SetLandedHeightMin(LinearEncoderCounts(ibdLandedHeightMin.Doublevalue));
            masterProgram.SetLandedHeightMax(LinearEncoderCounts(ibdLandedHeightMax.Doublevalue));
            masterProgram.SetSafeMinimumRetractDistance(LinearEncoderCounts(ibdRetractDistance.Doublevalue));
            masterProgram.SetSafeParkPosition(LinearEncoderCounts(ibdPositionClosetoPickCap.Doublevalue) + LinearEncoderCounts(2.0));
            
           

            masterProgram.SetMinLinearDistanceTravelled(LinearEncoderCounts(ibdLinearDistanceMin.Doublevalue));
            masterProgram.SetMaxLinearDistanceTravelled(LinearEncoderCounts(ibdLinearDistanceMax.Doublevalue));
            masterProgram.SetMinRotaryDistanceTravelled((int)(ibdRotaryDistanceMin.Doublevalue*actuator.RotaryResoloution()));
            masterProgram.SetMaxRotaryDistanceTravelled((int)(ibdRotaryDistanceMax.Doublevalue*actuator.RotaryResoloution()));
            masterProgram.SetLinearConstants(LinearEncoderCounts(1.0));
            masterProgram.SetPermisableTorque((int)((double) actuator.RotaryMaxSQ() * ibdMaxTorque.Doublevalue / 100.0 + 0.5));

            masterProgram.setLinearForce(txtTeachedSqLinear.Text);
            masterProgram.setLinearForceHome(txtTeachedSqLinearHome.Text);
            



            masterProgram.EnableHeightCheck(chkCheckLandedHeight.Checked);
            masterProgram.EnableRotateReverseFind(chkRotateReverse.Checked);
            masterProgram.EnableLinearDistanceTravelCheck(chkLinDistTravel.Checked);
            masterProgram.EnableRotaryDistanceTravelCheck(chkRotDistTravel.Checked);
            
            masterProgram.SetCapInPlace();
            
            masterProgram.Create();
        }

        private void btnSaveProgramToFile_Click(object sender, EventArgs e)
        {
            if (CheckUserInput() == false)
            {
                return;
            }

            Compile();
            SaveFileDialog filedialog = new SaveFileDialog();
            filedialog.Filter = "Text files (*.txt)|*.txt";
            filedialog.DefaultExt = "txt";
            filedialog.FileName = Path.GetFileNameWithoutExtension(fileName) + "." + filedialog.DefaultExt;
            string chosenFile = "";

            if (filedialog.ShowDialog() == DialogResult.OK)
            {
                chosenFile = filedialog.FileName;
                System.IO.StreamWriter outputfile = new System.IO.StreamWriter(chosenFile);

                foreach (string s in header)
                {
                    outputfile.WriteLine(s);
                }
                for (int i = 0; i < masterProgram.Count(); i++)
                {
                    outputfile.WriteLine(masterProgram.GetLine(i));
                }
                outputfile.Close();
            }
        }

        private void btnSaveInController_Click(object sender, EventArgs e)
        {
            if (CheckUserInput() == false)
            {
                return;
            }

            Compile();

           

            if (port.AbortAndWaitForPrompt() == false)
            {
            }
            else
            {
                btnRun.Enabled = false;
                btnSingleCycle.Enabled = false;
                btnStop.Enabled = false;
                port.DisableHandler();
                port.Mode9600(true);
                if(port.AbortAndWaitForPrompt())
                {
                    port.WriteString("RM\r");
                    if (port.WaitForPrompt(1000))  //02/22 ::VIGNESH::To use LAC-25 TURBO2 controller, wait time of 3 sec is added before loading the master program. "RM" is sent separately before sending master program.
                    {
                        for (int i = 0; i < masterProgram.Count(); i++)
                        {

                            if (masterProgram.GetLine(i)[0] != ';')
                            {

                                port.WriteStringAndWaitForPrompt(masterProgram.GetLine(i) + "\r");
                                

                            }
                        }
                    }

                }                        
                
                port.Mode9600(false);
                port.EnableHandler();
            }
            
            btnRun.Enabled = true;
            btnSingleCycle.Enabled = true;
            btnStop.Enabled = true;

        }


        private void txtPositionCloseToPart_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                txtPositionCloseToPart_Leave(null, null);
                e.Handled = true;
                SendKeys.Send("\t");            // Tab to the next field
            }
        }

        private void txtPositionCloseToPart_Leave(object sender, EventArgs e)
        {
            ibdPositonCloseToContainer.ReadValue();     // Read and check the value
        }


        private void txtPartLocationMin_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                txtPartLocationMin_Leave(null, null);
                e.Handled = true;
                SendKeys.Send("\t");            // Tab to the next field
            }
        }

        private void txtPartLocationMin_Leave(object sender, EventArgs e)
        {
            ibdLandedHeightMin.ReadValue();     // Read and check the value
        }

        private void txtPartLocationMax_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                txtPartLocationMax_Leave(null, null);
                e.Handled = true;
                SendKeys.Send("\t");            // Tab to the next field
            }
        }

        private void txtPartLocationMax_Leave(object sender, EventArgs e)
        {
            ibdLandedHeightMax.ReadValue();     // Read and check the value
        }

        

        private void txtMaxTorque_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                txtMaxTorque_Leave(null, null);
                e.Handled = true;
                SendKeys.Send("\t");            // Tab to the next field
            }
        }

        private void txtMaxTorque_Leave(object sender, EventArgs e)
        {
            ibdMaxTorque.ReadValue();     // Read and check the value
        }

        private void EnableInputFields()
        {
            txtLandedHeightMin.Enabled = chkCheckLandedHeight.Checked;
            txtLandedHeightMax.Enabled = chkCheckLandedHeight.Checked;
        }


        private void chkCheckLandedHeight_CheckedChanged(object sender, EventArgs e)
        {
            EnableInputFields();
        }

        private void chkRotateReverse_CheckedChanged(object sender, EventArgs e)
        {
            EnableInputFields();
        }


        private void txtTuningLinearP_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                txtTuningLinearP_Leave(null, null);
                e.Handled = true;
                SendKeys.Send("\t");            // Tab to the next field
            }
        }

        private void txtTuningLinearP_Leave(object sender, EventArgs e)
        {
            ibdTuningLinearP.ReadValue();     // Read and check the value
        }

        private void txtTuningLinearI_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                txtTuningLinearI_Leave(null, null);
                e.Handled = true;
                SendKeys.Send("\t");            // Tab to the next field
            }
        }

        private void txtTuningLinearI_Leave(object sender, EventArgs e)
        {
            ibdTuningLinearI.ReadValue();     // Read and check the value
        }

        private void txtTuningLinearD_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                txtTuningLinearD_Leave(null, null);
                e.Handled = true;
                SendKeys.Send("\t");            // Tab to the next field
            }
        }

        private void txtTuningLinearD_Leave(object sender, EventArgs e)
        {
            ibdTuningLinearD.ReadValue();     // Read and check the value
        }

        private void txtTuningRotaryP_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                txtTuningRotaryP_Leave(null, null);
                e.Handled = true;
                SendKeys.Send("\t");            // Tab to the next field
            }
        }

        private void txtTuningRotaryP_Leave(object sender, EventArgs e)
        {
            ibdTuningRotaryP.ReadValue();     // Read and check the value
        }

        private void txtTuningRotaryI_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                txtTuningRotaryI_Leave(null, null);
                e.Handled = true;
                SendKeys.Send("\t");            // Tab to the next field
            }
        }

        private void txtTuningRotaryI_Leave(object sender, EventArgs e)
        {
            ibdTuningRotaryI.ReadValue();     // Read and check the value
        }

        private void txtTuningRotaryD_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                txtTuningRotaryD_Leave(null, null);
                e.Handled = true;
                SendKeys.Send("\t");            // Tab to the next field
            }
        }

        private void txtTuningRotaryD_Leave(object sender, EventArgs e)
        {
            ibdTuningRotaryD.ReadValue();     // Read and check the value
        }

        private void txtSpeed_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                txtSpeed_Leave(null, null);
                e.Handled = true;
                SendKeys.Send("\t");            // Tab to the next field
            }
        }

        private void txtSpeed_Leave(object sender, EventArgs e)
        {
            ibdSpeed.ReadValue();     // Read and check the value
        }


        private void txtRotarySensitivity_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                txtRotarySensitivity_Leave(null, null);
                e.Handled = true;
                SendKeys.Send("\t");            // Tab to the next field
            }
        }

        private void txtRotarySensitivity_Leave(object sender, EventArgs e)
        {
            ibiRotarySensitivity.ReadValue();     // Read and check the value
        }

        private void cmbActuator_SelectedIndexChanged(object sender, EventArgs e)
        {
            actuator.Load(cmbActuator.SelectedItem.ToString());
            load_values_from_actuator_ini();

        }

        private void SetButtons(bool bConnected)
        {
            btnSaveToController.Enabled = bConnected;
            btnTeach.Enabled = bConnected;
            btnSingleCycle.Enabled = bConnected;
            btnRun.Enabled = bConnected;
            btnStop.Enabled = bConnected;
            button2.Enabled = bConnected;
        }

        private void NotImplemented()
        {
            MessageBox.Show("Not implemented yet");
        }

        private void LoadSettingsFromFile(string filename)
        {
            MySettings settings = new MySettings();
            settings.Read(filename);
            fileName = filename;
            Title();
            checkerFile.Load();
        }


        //
        // ConfirmChanges() returns true in the following conditions:
        //
        // - There are no changes
        // - There are changes and the operator selects OK in the warning meassage to discard the changes
        // - There are changes and the operator has selected to save the changes and actually has saved them
        //

        private bool ConfirmChanges(string message)
        {
            if (!NoActuatorfilesFound)
            {
                if (checkerFile.Changed())
                {
                    if (MessageBox.Show("You made changes that are not saved" + Environment.NewLine + Environment.NewLine +
                                        "Select OK to " + message + " and discard the changes" + Environment.NewLine +
                                        "Select Cancel to save your changes first", "Warning", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning)
                                        == DialogResult.OK)
                    {
                        return true;
                    }
                    else
                    {
                        return SaveSettingsToFile();
                    }
                }
                else
                {
                    return true;
                }
            }
            else
            {
                return true;
            }
        }

        private void btnLoadSettingsFromFile_Click(object sender, EventArgs e)
        {
            bool bOk = ConfirmChanges("load new settings from a file");
            if (bOk)
            {
                OpenFileDialog filedialog = new OpenFileDialog();
                filedialog.Filter = "SMAC Capping Control Center files (*.sccc)|*.sccc";
                filedialog.DefaultExt = "sccc";
                filedialog.FileName = Path.GetFileName(fileName);
                if (filedialog.ShowDialog() == DialogResult.OK)
                {
                    fileName = filedialog.FileName;     // Set the new fillename here
                    LoadSettingsFromFile(fileName);
                    Title();
                }
            }
        }

        private bool SaveSettingsToFile()
        {
            SaveFileDialog filedialog = new SaveFileDialog();
            filedialog.FileName = Path.GetFileName(fileName);
            filedialog.Filter = "SMAC Capping Control Center files (*.sccc)|*.sccc";
            filedialog.DefaultExt = "sccc";
            if (filedialog.ShowDialog() == DialogResult.OK)
            {
                fileName = filedialog.FileName;
                MySettings settings = new MySettings();
                settings.Write(fileName);
                checkerFile.Load();
                Title();
                return true;
            }
            else
            {
                return false;
            }
        }

        private void btnSaveSettingsToFile_Click(object sender, EventArgs e)
        {
            SaveSettingsToFile();
        }

        private void btnTeach_Click(object sender, EventArgs e)
        {
            port.DisableHandler();
            if (port.AbortAndWaitForPrompt())
            {
                if (MessageBox.Show("Make sure that part is in place and then hit OK", "", MessageBoxButtons.OKCancel, MessageBoxIcon.Information) == DialogResult.OK)
                {
                    btnUndo_Click(null, null);
                    port.WriteString("MS240\r");
                }
            }
            port.EnableHandler();
        }

        private void btnSingleCycle_Click(object sender, EventArgs e)
        {
            port.DisableHandler();
            if (port.AbortAndWaitForPrompt())
            {
                port.WriteString("MS24\r");
            }
            port.EnableHandler();
        }

        private void btnRun_Click(object sender, EventArgs e)
        {
            port.DisableHandler();
            if (port.AbortAndWaitForPrompt())
            {
                port.WriteString("MS0\r");
            }
            port.EnableHandler();
        }

        private void btnStop_Click(object sender, EventArgs e)
        {
            port.DisableHandler();
            if (port.AbortAndWaitForPrompt())
            {
                port.WriteString("0AB,MF\r");
            }
            port.EnableHandler();
        }


        private void btnContactInfo_Click(object sender, EventArgs e)
        {
            DlgContactInfo contactInfo = new DlgContactInfo();

            contactInfo.ShowDialog();
            
        }

        private void button4_Click(object sender, EventArgs e)
        {
//            System.Diagnostics.Process.Start(@"c:\users\herman\Testfile.pdf");
            try
            {
                System.Diagnostics.Process.Start("Help document.pdf");
            }
            catch
            {
                MessageBox.Show("Can not open the help document", "File open error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            NotImplemented();
        }

        private int LinearEncoderCounts(double value)
        {
            return (int)(0.5 + value / actuator.LinearResoloution() * 1000.0);
        }

        private double millimeters(int counts)
        {
            return (double)counts * actuator.LinearResoloution() / 1000.0;
        }

        private double revolutions(int counts)
        {
            return ((double)counts / actuator.RotaryResoloution());
        }

        private double micrometers(int counts)
        {
            return (double)counts * actuator.LinearResoloution();
        }

        private void cmbPort_MouseDown(object sender, MouseEventArgs e)
        {
            FillPortnames();
        }


        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!NoActuatorfilesFound)
            {
                e.Cancel = !ConfirmChanges("quit the application");
                if (cmbPort.SelectedItem != null)
                {
                    Properties.Settings.Default.Port = cmbPort.SelectedItem.ToString();
                }
                Properties.Settings.Default.File = fileName;
                Properties.Settings.Default.Save();
            }
        }

        private void cmbPort_SelectedIndexChanged(object sender, EventArgs e)
        {
            Opencomport();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            SaveFileDialog filedialog = new SaveFileDialog();
            filedialog.Filter = "Text files (*.txt)|*.txt";
            filedialog.DefaultExt = "txt";
            if (filedialog.ShowDialog() == DialogResult.OK)
            {
                List<string> Listing = new List<string>();
                CappingProgram program = new CappingProgram();
                Variables variables = new Variables(Constants.Defaults());
                Listing.Add(programName);
                Listing.Add("+----------------------------------------------------+");
                Listing.Add("|               The master program                   |");
                Listing.Add("+----------------------------------------------------+");
                for (int i = 0; i < program.CountRaw(); i++)
                {
                    Listing.Add(program.GetRawString(i));
                }
                Listing.Add("+--------------------------------------------------------------------+");
                Listing.Add("|              Variables and their default values                    |");
                Listing.Add("+--------------------------------------------------------------------+");

                for (int i = 0; i < variables.Count(); i++)
                {
                    Listing.Add(variables.GetName(i) + "=" + variables.GetValue(i).ToString());
                }
                File.WriteAllLines(filedialog.FileName, Listing.ToArray());
            }
        }

        private void chkShowAll_CheckedChanged(object sender, EventArgs e)
        {
            txtTerminal.Focus();
        }

        public bool LoggedIn()
        {
            return ((tabControl1.SelectedIndex == 1) || !passwordRequired);
        }

        private void ShowSettupButtons()
        {
            bool show = (tabControl1.SelectedIndex != 0);
            lblPort.Visible = show;
            cmbPort.Visible = show;
            btnLoadSettingsFromFile.Visible = show;
            btnSaveSettingsToFile.Visible = show;
            btnSaveProgramToFile.Visible = show;
            btnSaveToController.Visible = show;
            btnSingleCycle.Visible = show;
            chkShowAll.Visible = show;
            button2.Visible = show;
        }

        private void tabControl1_SelectedIndexChanged(object sender, EventArgs e)
        {
            tabControl1.SelectedIndexChanged -= tabControl1_SelectedIndexChanged;
            if (tabControl1.SelectedIndex == 1 || tabControl1.SelectedIndex == 2)
            {
                int newindex = tabControl1.SelectedIndex;
                if (passwordRequired)
                {
                    tabControl1.SelectedIndex = 0;  // Do not show the Setup tab yet
                    DlgPassword password = new DlgPassword("SMAC5807");
                    switch (password.ShowDialog())
                    {
                        case DialogResult.Cancel:
                            newindex = 0;
                            break;
                        case DialogResult.OK:
                            break;
                        case DialogResult.Yes:
                            passwordRequired = false;
                            break;
                    }
                }
                tabControl1.SelectedIndex = newindex;
            }
            ShowSettupButtons();
            tabControl1.SelectedIndexChanged += tabControl1_SelectedIndexChanged;
        }

        private void btnUndo_Click(object sender, EventArgs e)
        {
            txtTeachedSqLinear.Text = "";
            txtTeachedSqLinearHome.Text = "";
            txtTeachedHeight.Text = "";
        }

        
        

        private void lblPluginfoSetup_Click(object sender, EventArgs e)
        {

        }

        private void cmbCappingType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(cmbCappingType.SelectedIndex == 0)
            {
                //txtPositionCloseToPickCap.Enabled = false;
                txtSpeedtoPickCap.Enabled = false;
                txtRetractDistance.Enabled = false;
                txtRetractDistance.Text = "0";
                btnTeachPickCap.Enabled = false;
            }
            else if (cmbCappingType.SelectedIndex == 1)
            {
                txtPositionCloseToPickCap.Enabled = true;
                txtSpeedtoPickCap.Enabled = true;
                txtRetractDistance.Enabled = true;
                btnTeachPickCap.Enabled = true;
            }
        }

        private void txtMaxLinTravel_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                txtMaxLinTravel_Leave(null, null);
                e.Handled = true;
                SendKeys.Send("\t");            // Tab to the next field
            }
        }

        private void txtMaxLinTravel_Leave(object sender, EventArgs e)
        {
            ibdLinearDistanceMax.ReadValue();
        }

        private void txtMinLinTravel_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                txtMinLinTravel_Leave(null, null);
                e.Handled = true;
                SendKeys.Send("\t");            // Tab to the next field
            }
        }

        private void txtMinLinTravel_Leave(object sender, EventArgs e)
        {
            ibdLinearDistanceMin.ReadValue();
        }

        private void txtMaxRotDistTravel_Leave(object sender, EventArgs e)
        {
            ibdRotaryDistanceMax.ReadValue();
        }

        private void txtMaxRotDistTravel_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                txtMaxRotDistTravel_Leave(null, null);
                e.Handled = true;
                SendKeys.Send("\t");            // Tab to the next field
            }
        }

        private void txtMinRotDistTravel_Leave(object sender, EventArgs e)
        {
            ibdRotaryDistanceMin.ReadValue();
        }

        private void txtMinRotDistTravel_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                txtMinRotDistTravel_Leave(null, null);
                e.Handled = true;
                SendKeys.Send("\t");            // Tab to the next field
            }
        }

        private void txtReleaseDwellTime_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                txtReleaseDwellTime_Leave(null, null);
                e.Handled = true;
                SendKeys.Send("\t");            // Tab to the next field
            }
        }

        private void txtReleaseDwellTime_Leave(object sender, EventArgs e)
        {
            ibdReleaseDwellTime.ReadValue();
        }

        private void txtPositionCloseToPickCap_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                txtPositionCloseToPickCap_Leave(null, null);
                e.Handled = true;
                SendKeys.Send("\t");            // Tab to the next field
            }
        }

        private void txtPositionCloseToPickCap_Leave(object sender, EventArgs e)
        {
            ibdPositionClosetoPickCap.ReadValue();
        }

        private void txtSpeedtoPickCap_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                txtSpeedtoPickCap_Leave(null, null);
                e.Handled = true;
                SendKeys.Send("\t");            // Tab to the next field
            }
        }

        private void txtSpeedtoPickCap_Leave(object sender, EventArgs e)
        {
            ibdSpeedToPickCap.ReadValue();
        }

        private void txtRetractDistance_Leave(object sender, EventArgs e)
        {
            ibdRetractDistance.ReadValue();
        }

        private void txtRetractDistance_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                txtRetractDistance_Leave(null, null);
                e.Handled = true;
                SendKeys.Send("\t");            // Tab to the next field
            }
        }

        private void chkLinDistTravel_CheckedChanged(object sender, EventArgs e)
        {
            if(chkLinDistTravel.Checked)
            {
                txtMaxLinTravel.Enabled = true;
                txtMinLinTravel.Enabled = true;
            }
            else
            {
                txtMaxLinTravel.Enabled = false;
                txtMinLinTravel.Enabled = false;
            }
        }

        private void chkRotDistTravel_CheckedChanged(object sender, EventArgs e)
        {
            if (chkLinDistTravel.Checked)
            {
                txtMaxRotDistTravel.Enabled = true;
                txtMinRotDistTravel.Enabled = true;
            }
            else
            {
                txtMaxRotDistTravel.Enabled = false;
                txtMinRotDistTravel.Enabled = false;
            }
        }

        private void txtNumofThreads_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                txtNumofThreads_Leave(null, null);
                e.Handled = true;
                SendKeys.Send("\t");            // Tab to the next field
            }
        }

        private void txtNumofThreads_Leave(object sender, EventArgs e)
        {
            ibdNumofThreads.ReadValue();
        }

        private void btnTeachPickCap_Click(object sender, EventArgs e)
        {
            btnTeach_Click(null, null);
        }

        private void txtNumofThreads_TextChanged(object sender, EventArgs e)
        {
           
        }

        private void lblLinearDistance_Run_Click(object sender, EventArgs e)
        {

        }
    }
}
