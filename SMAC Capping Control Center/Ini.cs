﻿using System;
using System.Runtime.InteropServices;
using System.Text;
using System.Collections.Generic;
using System.Windows.Forms;
using System.IO;

// Code from: // http://www.codeproject.com/Articles/1966/An-INI-file-handling-class-using-C
// 
// Using the class
// Steps to use the Ini class:
//
// 1. In your project namespace definition add  
//    using INI;
// 2. Create a IniFile like this
//    IniFile ini = new IniFile("C:\\test.ini");
// 3. Use IniWriteValue to write a new value to a specific key in a section or use IniReadValue to read a value FROM a key in a specific Section.
//    That's all. It's very easy in C# to include API functions in your class(es).



namespace SMAC_Capping_Control_Center
{
    /// <summary>
    /// Create a New INI file to store or load data
    /// </summary>
    public class IniFile
    {
        public string path;

        [DllImport("kernel32")]
        private static extern long WritePrivateProfileString(string section,
            string key, string val, string filePath);

//        DWORD WINAPI GetPrivateProfileString(
//              _In_   LPCTSTR lpAppName,
//              _In_   LPCTSTR lpKeyName,
//              _In_   LPCTSTR lpDefault,
//              _Out_  LPTSTR lpReturnedString,
//              _In_   DWORD nSize,
//              _In_   LPCTSTR lpFileName);

        [DllImport("kernel32")]
        private static extern int GetPrivateProfileString(string section,
                 string key, string def, StringBuilder retVal,
            int size, string filePath);

//        DWORD WINAPI GetPrivateProfileSection(
//              _In_   LPCTSTR lpAppName,
//              _Out_  LPTSTR lpReturnedString,
//              _In_   DWORD nSize,
//              _In_   LPCTSTR lpFileName);
        [DllImport("kernel32")]
        private static extern int GetPrivateProfileSection(
                string section,
                StringBuilder retVal,
                int size,
                string filePath);

        /// <summary>
        /// INIFile Constructor.
        /// </summary>
        /// <PARAM name="INIPath"></PARAM>
        public IniFile(string IniPath)
        {
            path = IniPath;
        }
        /// <summary>
        /// Write Data to the INI File
        /// </summary>
        /// <PARAM name="Section"></PARAM>
        /// Section name
        /// <PARAM name="Key"></PARAM>
        /// Key Name
        /// <PARAM name="Value"></PARAM>
        /// Value Name
        public void WriteValue(string Section, string Key, string Value)
        {
            WritePrivateProfileString(Section, Key, Value, this.path);
        }

        public void WriteDoubleValue(string Section, string Key, double Value)
        {
            WritePrivateProfileString(Section, Key, Utils.neutralDouble(Value), this.path);
        }

        public void WriteIntValue(string Section, string Key, int Value)
        {
            WritePrivateProfileString(Section, Key, Value.ToString(), this.path);
        }



        /// <summary>
        /// Read Data Value From the Ini File
        /// </summary>
        /// <PARAM name="Section"></PARAM>
        /// <PARAM name="Key"></PARAM>
        /// <PARAM name="Path"></PARAM>
        /// <returns></returns>
        public string ReadValue(string Section, string Key)
        {
            StringBuilder temp = new StringBuilder(255);
            int i = GetPrivateProfileString(Section, Key, "", temp,
                                            255, this.path);
            string retval = temp.ToString();
            int comment = retval.IndexOf(';');
            if (comment >= 0)       // Strip off a comment
            {
                retval = retval.Substring(0, comment);
            }
            return retval.Trim();
        }

        public string[] ReadSectionKeynames(string Section)
        {
            List<string> retval = new List<string>();
            bool bFoundSection = false;
            foreach (string s in File.ReadAllLines(path))
            {
                string temp = s.Trim();
                if (!bFoundSection)
                {
                    bFoundSection = temp.IndexOf("[" + Section + "]") == 0;
                }
                else
                {
                    if (temp.IndexOf('[') == 0)
                    {
                        break;      // Hit the start of the next section
                    }

                    if ((temp.Length > 0) && (temp[0] != ';') && (temp.IndexOf('=') > 0))
                    {
                        retval.Add(temp.Substring(0, temp.IndexOf('=')).Trim());
                    }
                }
            }
            return retval.ToArray();
        }

        public double ReadDoubleValue(string Section, string Key)
        {
            string strTemp = ReadValue(Section, Key);
            double retval = 0;
            double dummy = 0.5;
            string localSeparator = ".";
            if (dummy.ToString().Contains(","))
            {
                localSeparator = ",";
            }

            if (!double.TryParse(strTemp.Replace(".", localSeparator), out retval))
            {
                MessageBox.Show("File : " + path + Environment.NewLine +
                                "Section : " + Section + Environment.NewLine +
                                "Key : " + Key + Environment.NewLine +
                                "Has in incorrect value : " + strTemp);
            }
            return retval;
        }

        public int ReadIntValue(string Section, string Key)
        {
            string strTemp = ReadValue(Section, Key);
            int retval = 0;

            if (!int.TryParse(strTemp, out retval))
            {
                MessageBox.Show("File : " + path + Environment.NewLine +
                                "Section : " + Section + Environment.NewLine +
                                "Key : " + Key + Environment.NewLine +
                                "Has in incorrect value : " + strTemp);
            }
            return retval;
        }

        public bool ReadBoolValue(string Section, string Key)
        {
            string strTemp = ReadValue(Section, Key);
            bool retval = false;

            if (!bool.TryParse(strTemp, out retval))
            {
                MessageBox.Show("File : " + path + Environment.NewLine +
                                "Section : " + Section + Environment.NewLine +
                                "Key : " + Key + Environment.NewLine +
                                "Has in incorrect value : " + strTemp);
            }
            return retval;
        }
    }
}